package net.thumbtack.school.ModelsTask9;


import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

public class Group {
    private List<Trainee> trainees;
    private List<Subject> subjects;
    private String name;
    private String room;
    private int id;

    public Group(int id, String name, String room, List<Trainee> trainees, List<Subject> subjects) {
        this.id = id;
        this.name = name;
        this.room = room;
        this.trainees = trainees;
        this.subjects = subjects;
    }

    public Group(int id, String name, String room) {
        this(id, name, room, new CopyOnWriteArrayList<>(), new CopyOnWriteArrayList<>());
    }

    public Group(String name, String room) {
        this(0, name, room, new CopyOnWriteArrayList<>(), new CopyOnWriteArrayList<>());
    }

    public synchronized String getName() {
        return name;
    }

    public synchronized void setName(String name) {
        this.name = name;
    }

    public synchronized String getRoom() {
        return room;
    }

    public synchronized void setRoom(String room) {
        this.room = room;
    }

    public synchronized List<Subject> getSubjects() {
        return subjects;
    }

    public synchronized void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

    public synchronized List<Trainee> getTrainees() {
        return trainees;
    }

    public synchronized void setTrainees(List<Trainee> trainees) {
        this.trainees = trainees;
    }

    public synchronized int getId() {
        return id;
    }

    public synchronized void setId(int id) {
        this.id = id;
    }

    public synchronized void addTrainee(Trainee trainee) {
        trainees.add(trainee);
    }

    public synchronized void addSubject(Subject subject) {
        subjects.add(subject);
    }

    public synchronized void removeTrainee(Trainee trainee) {
        trainees.remove(trainee);
    }

    public synchronized void removeSubject(Subject subject) {
        subjects.remove(subject);
    }

    @Override
    public synchronized boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Group)) return false;
        Group group = (Group) o;
        return getName().equals(group.getName()) &&
                getRoom().equals(group.getRoom()) &&
                getId() == group.getId() &&
                Objects.equals(subjects, group.subjects) &&
                Objects.equals(trainees, group.trainees);
    }

    @Override
    public synchronized int hashCode() {
        return Objects.hash(getName(), getRoom(), getId(), subjects, trainees);
    }
}
