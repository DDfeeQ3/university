package net.thumbtack.school.ModelsTask9;

import java.util.Objects;

public class Trainee {
    private int id;
    private int rating;
    private String firstName;
    private String lastName;

    public Trainee(int id, String firstName, String lastName, int rating) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.id = id;
        this.rating = rating;
    }

    public Trainee(String firstName, String lastName, int rating) {
        this(0, firstName, lastName, rating);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Trainee)) return false;
        Trainee trainee = (Trainee) o;
        return getId() == trainee.getId() && getRating() == trainee.getRating() && getFirstName().equals(trainee.getFirstName()) && getLastName().equals(trainee.getLastName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getRating(), getFirstName(), getLastName());
    }
}
