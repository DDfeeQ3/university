package net.thumbtack.school.hospital.serviceTask18;

import net.thumbtack.school.hospital.dao.DoctorDao;
import net.thumbtack.school.hospital.dao.PatientDao;
import net.thumbtack.school.hospital.daoimpl.DoctorDaoImpl;
import net.thumbtack.school.hospital.daoimpl.PatientDaoImpl;
import net.thumbtack.school.hospital.dto.request.*;
import net.thumbtack.school.hospital.dto.response.*;
import net.thumbtack.school.hospital.exceptions.ErrorCode;
import net.thumbtack.school.hospital.exceptions.ServerException;
import net.thumbtack.school.hospital.model.Appointment;
import net.thumbtack.school.hospital.model.Doctor;
import net.thumbtack.school.hospital.model.MethodType;
import net.thumbtack.school.hospital.model.Patient;

import java.util.LinkedList;
import java.util.List;

public class DoctorService {
    private DoctorDao doctorDao = new DoctorDaoImpl();
    private PatientDao patientDao = new PatientDaoImpl();

    public BaseResponse registerDoctor(RegisterDoctorDtoRequest registerDoctorDtoRequest) {
        try {
            validateRegistrationDoctor(registerDoctorDtoRequest);
            Doctor doctor = new Doctor(registerDoctorDtoRequest.getFirstName(),
                    registerDoctorDtoRequest.getLastName(),
                    registerDoctorDtoRequest.getLogin(), registerDoctorDtoRequest.getPassword(), registerDoctorDtoRequest.getSpeciality());
            String token = doctorDao.insert(doctor);
            return new RegisterDoctorDtoResponse(token);
        } catch (ServerException e) {
            return new ErrorResponse(e);
        }
    }

    public BaseResponse login(LoginUserDtoRequest loginUserDtoRequest) {
        try {
            validateLogin(loginUserDtoRequest);

            Doctor doctor = doctorDao.getByLoginAndPassword(loginUserDtoRequest.getLogin(), loginUserDtoRequest.getPassword());

            return new LoginUserDtoResponse(doctorDao.login(doctor));
        } catch (ServerException e) {
            return new ErrorResponse(e);
        }
    }

    public BaseResponse logout(LogoutUserDtoRequest logoutUserDtoRequest) {
        try {
            checkUserByToken(logoutUserDtoRequest.getToken());
            doctorDao.logout(logoutUserDtoRequest.getToken());

            return new BaseResponse();
        } catch (ServerException e) {
            return new ErrorResponse(e);
        }
    }

    public BaseResponse getDoctorByToken(GetDoctorByTokenDtoRequest getDoctorByTokenDtoRequest) {
        try {
            Doctor doctor = doctorDao.getByToken(getDoctorByTokenDtoRequest.getToken());

            return new DoctorDtoResponse(doctor.getId(), doctor.getFirstName(), doctor.getLastName(),
                    doctor.getSpeciality(), doctor.getLogin(), doctor.getPassword());
        } catch (ServerException e) {
            return new ErrorResponse(e);
        }
    }

    public BaseResponse getPatientByID(GetPatientByIDtoRequest getPatientByIDtoRequest) {
        try {
            Patient patient = patientDao.getPatientByID(getPatientByIDtoRequest.getPatientID());

            return new PatientDtoResponse(patient.getFirstName(), patient.getLastName(), patient.getId());
        } catch (ServerException e) {
            return new ErrorResponse(e);
        }
    }

    public BaseResponse registerPatient(RegisterPatientDtoRequest registerPatientDtoRequest) {
        try {
            validateRegistrationPatient(registerPatientDtoRequest);

            Doctor doctor = doctorDao.getByLoginForPatient(registerPatientDtoRequest.getLoginDoctor());
            if (registerPatientDtoRequest.getDisease().equals("")) {
                throw new ServerException(ErrorCode.WRONG_DISEASE_FORMAT_ERROR);
            }

            String disease = registerPatientDtoRequest.getDisease();

            Patient patient = new Patient(doctor, registerPatientDtoRequest.getFirstName(),
                    registerPatientDtoRequest.getLastName(), disease,
                    registerPatientDtoRequest.getLogin(),
                    registerPatientDtoRequest.getPassword());

            String token = patientDao.insert(doctor, patient);
            return new RegisterPatientDtoResponse(token, patient.getLogin());
        } catch (ServerException e) {
            return new ErrorResponse(e);
        }
    }

    public BaseResponse getPatientListByDoctor(GetPatientListByDoctorDtoRequest getListPatientByDoctorDtoRequest) {
        try {
            Doctor doctor = doctorDao.getByToken(getListPatientByDoctorDtoRequest.getToken());

            List<Patient> patientList = doctorDao.getPatientListByDoctor(doctor);

            return new PatientListDtoResponse(getPatientListResponse(patientList).getPatientList());
        } catch (ServerException e) {
            return new ErrorResponse(e);
        }
    }

    public BaseResponse getPatientListByDoctorByDisease(GetPatientListByDoctorByDiseaseDtoRequest getPatientListByDoctorByDiseaseDtoRequest) {
        try {
            Doctor doctor = doctorDao.getByToken(getPatientListByDoctorByDiseaseDtoRequest.getToken());
            List<Patient> patientList = doctorDao.getPatientListByDoctorByDisease(doctor, getPatientListByDoctorByDiseaseDtoRequest.getDisease());

            return new PatientListDtoResponse(getPatientListResponse(patientList).getPatientList());
        } catch (ServerException e) {
            return new ErrorResponse(e);
        }
    }

    public BaseResponse getPatientListByDoctorByAppointments(GetPatientListByDoctorByAppointmentDtoRequest getPatientListByDoctorByAppointmentDtoRequest) {
        try {
            Doctor doctor = doctorDao.getByToken(getPatientListByDoctorByAppointmentDtoRequest.getToken());

            AppointmentDtoRequest app = getPatientListByDoctorByAppointmentDtoRequest.getAppointment();
            Appointment appointment;
            if (app.getMethodType() == MethodTypeRequest.MEDICINE) {
                appointment = new Appointment(MethodType.MEDICINE, app.getTreatmentMethod(), app.getFrequency());
            } else if (app.getMethodType() == MethodTypeRequest.PROCEDURE) {
                appointment = new Appointment(MethodType.PROCEDURE, app.getTreatmentMethod(), app.getFrequency());
            } else {
                throw new ServerException(ErrorCode.WRONG_PATIENT_APPOINTMENTS_ERROR);
            }

            List<Patient> patientList = doctorDao.getPatientListByDoctorByAppointment(doctor, appointment);

            return new PatientListDtoResponse(getPatientListResponse(patientList).getPatientList());
        } catch (ServerException e) {
            return new ErrorResponse(e);
        }
    }

    public BaseResponse getPatientListByDisease(GetPatientListByDiseaseDtoRequest getPatientListByDiseaseDtoRequest) {
        try {
            String disease = getPatientListByDiseaseDtoRequest.getDisease();
            List<Patient> patientList = doctorDao.getPatientListByDisease(disease);

            return new PatientListDtoResponse(getPatientListResponse(patientList).getPatientList());
        } catch (ServerException e) {
            return new ErrorResponse(e);
        }
    }

    public BaseResponse getPatientListByAppointment(GetPatientListByAppointmentDtoRequest getPatientListByAppointmentsDtoRequest) {
        try {
            AppointmentDtoRequest app = getPatientListByAppointmentsDtoRequest.getAppointment();
            Appointment appointment;
            if (app.getMethodType() == MethodTypeRequest.MEDICINE) {
                appointment = new Appointment(MethodType.MEDICINE, app.getTreatmentMethod(), app.getFrequency());
            } else if (app.getMethodType() == MethodTypeRequest.PROCEDURE) {
                appointment = new Appointment(MethodType.PROCEDURE, app.getTreatmentMethod(), app.getFrequency());
            } else {
                throw new ServerException(ErrorCode.WRONG_PATIENT_APPOINTMENTS_ERROR);
            }

            List<Patient> patientList = doctorDao.getPatientListByAppointment(appointment);

            return new PatientListDtoResponse(getPatientListResponse(patientList).getPatientList());
        } catch (ServerException e) {
            return new ErrorResponse(e);
        }
    }

    public BaseResponse addAppointment(AddAppointmentDtoRequest addAppointmentsDtoRequest) {
        try {
            Appointment appointmentFotAdd;
            if (addAppointmentsDtoRequest.getMethodType() == MethodTypeRequest.MEDICINE) {
                appointmentFotAdd = new Appointment(MethodType.MEDICINE, addAppointmentsDtoRequest.getMethodName(),
                        addAppointmentsDtoRequest.getFrequency());
            } else if (addAppointmentsDtoRequest.getMethodType() == MethodTypeRequest.PROCEDURE) {
                appointmentFotAdd = new Appointment(MethodType.PROCEDURE, addAppointmentsDtoRequest.getMethodName(),
                        addAppointmentsDtoRequest.getFrequency());
            } else {
                throw new ServerException(ErrorCode.WRONG_PATIENT_APPOINTMENTS_ERROR);
            }

            int appId = doctorDao.addAppointment(addAppointmentsDtoRequest.getPatientID(), appointmentFotAdd);
            String patientToken = patientDao.getPatientTokenById(addAppointmentsDtoRequest.getPatientID());
            return new AddAppointmentDtoResponse(patientToken, appId);
        } catch (ServerException e) {
            return new ErrorResponse(e);
        }
    }

    public BaseResponse deletePatientByDoctor(DeletePatientByDoctorDtoRequest deletePatientByDoctorDtoRequest) {
        try {
            Patient patient = patientDao.getPatientByID(deletePatientByDoctorDtoRequest.getPatientID());

            doctorDao.deletePatient(patient);
            if (doctorDao.getByToken(deletePatientByDoctorDtoRequest.getToken()) != null) {
                throw new ServerException(ErrorCode.DELETION_ERROR);
            }
            return new BaseResponse();
        } catch (ServerException e) {
            return new ErrorResponse(e);
        }
    }

    private PatientListDtoResponse getPatientListResponse(List<Patient> patientList) {
        List<PatientDtoResponse> patientDtoResponses = new LinkedList<>();

        for (Patient patient : patientList) {
            patientDtoResponses.add(new PatientDtoResponse(patient.getFirstName(), patient.getLastName(), patient.getId()));
        }
        return new PatientListDtoResponse(patientDtoResponses);
    }

    public BaseResponse delete(DeleteUserDtoRequest deleteAccountUserRequest) {
        try {
            checkUserByToken(deleteAccountUserRequest.getToken());

            doctorDao.delete(doctorDao.getByToken(deleteAccountUserRequest.getToken()));

            return new BaseResponse();
        } catch (ServerException e) {
            return new ErrorResponse(e);
        }
    }

    private static void validateRegistrationDoctor(RegisterDoctorDtoRequest request) throws ServerException {
        if ((request.getFirstName().equals(""))) {
            throw new ServerException(ErrorCode.WRONG_FIRST_NAME_ERROR);
        } else if (request.getLastName().equals("")) {
            throw new ServerException(ErrorCode.WRONG_LAST_NAME_ERROR);
        } else if (request.getLogin().equals("")) {
            throw new ServerException(ErrorCode.WRONG_LOGIN_FORMAT_ERROR);
        } else if (request.getPassword().equals("") || request.getPassword().length() < 6) {
            throw new ServerException(ErrorCode.WRONG_PASSWORD_FORMAT_ERROR);
        } else if (request.getSpeciality().equals("")) {
            throw new ServerException(ErrorCode.WRONG_DOCTOR_SPECIALITY_ERROR);
        }
    }

    private static void validateRegistrationPatient(RegisterPatientDtoRequest request) throws ServerException {
        if ((request.getFirstName().equals(""))) {
            throw new ServerException(ErrorCode.WRONG_FIRST_NAME_ERROR);
        } else if (request.getLastName().equals("")) {
            throw new ServerException(ErrorCode.WRONG_LAST_NAME_ERROR);
        } else if (request.getLogin().equals("")) {
            throw new ServerException(ErrorCode.WRONG_LOGIN_FORMAT_ERROR);
        } else if (request.getPassword().equals("") || request.getPassword().length() < 6) {
            throw new ServerException(ErrorCode.WRONG_PASSWORD_FORMAT_ERROR);
        } else if (request.getDisease().equals("")) {
            throw new ServerException(ErrorCode.WRONG_DISEASE_FORMAT_ERROR);
        }
    }

    private boolean validateLogin(LoginUserDtoRequest request) throws ServerException {
        if (doctorDao.getByLoginAndPassword(request.getLogin(), request.getPassword()) == null) {
            throw new ServerException(ErrorCode.INVALID_LOGIN_OR_PASSWORD_ERROR);
        } else return true;
    }

    private void checkUserByToken(String token) throws ServerException {
        doctorDao.getByToken(token);
    }
}
