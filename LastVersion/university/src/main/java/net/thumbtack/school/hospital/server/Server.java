package net.thumbtack.school.hospital.server;

import net.thumbtack.school.hospital.exceptions.ServerException;
import net.thumbtack.school.hospital.mybatis.utils.HospitalUtils;
import net.thumbtack.school.hospital.service.ClearDatabaseService;
import net.thumbtack.school.hospital.service.DatabaseService;
import net.thumbtack.school.hospital.service.DoctorService;
import net.thumbtack.school.hospital.service.PatientService;

public class Server {
    private final DoctorService doctorService = new DoctorService();
    private final PatientService patientService = new PatientService();
    private final DatabaseService databaseService = new DatabaseService();
    private static boolean setUpIsDone = false;

    public Server() {
        if (!setUpIsDone) {
            boolean initSqlSessionFactory = HospitalUtils.initSqlSessionFactory();
            if (!initSqlSessionFactory) {
                throw new RuntimeException("Can't create connection, stop");
            }
            setUpIsDone = true;
        }
        new ClearDatabaseService().clearDatabase();
    }

    public String loginDoctor(String json) {
        return doctorService.login(json);
    }

    public String logoutDoctor(String json) {
        return doctorService.logout(json);
    }

    public String loginPatient(String json) {
        return patientService.login(json);
    }

    public String logoutPatient(String json) {
        return patientService.logout(json);
    }

    public String registerDoctor(String json) {
        return doctorService.registerDoctor(json);
    }

    public String addAppointments(String json) {
        return doctorService.addAppointments(json);
    }

    public String registerPatient(String json) {
        return doctorService.registerPatient(json);
    }

    public String deleteDoctor(String json) {
        return doctorService.delete(json);
    }

    public String deletePatient(String json) {
        return patientService.delete(json);
    }

    public String deletePatientByDoctor(String json) {
        return doctorService.deletePatientByDoctor(json);
    }

    public String getDoctorByToken(String json) {
        return doctorService.getDoctorByToken(json);
    }

    public String getPatientByToken(String json) {
        return patientService.getPatientByToken(json);
    }

    public String getDoctorInfoByPatient(String json) {
        return patientService.getDoctorInfoByPatient(json);
    }

    public String GetPatientByIDDtoRequest(String json) {
        return doctorService.getPatientByID(json);
    }

    public String getPatientListByDoctor(String json) {
        return doctorService.getPatientListByDoctor(json);
    }

    public String getPatientListByDoctorByDisease(String json) {
        return doctorService.getPatientListByDoctorByDisease(json);
    }

    public String getPatientListByDoctorByAppointments(String json) {
        return doctorService.getPatientListByDoctorByAppointments(json);
    }

    public String getPatientListByDisease(String json) {
        return doctorService.getPatientListByDisease(json);
    }

    public String getPatientListByAppointments(String json) {
        return doctorService.getPatientListByAppointments(json);
    }

    public String changePasswordPatient(String json) {
        return patientService.changePasswordPatient(json);
    }

    public String getDoctorByPatient(String json) {
        return patientService.getDoctorByPatient(json);
    }

    public String getAppointmentsByPatient(String json) {
        return patientService.getAppointmentsByPatient(json);
    }

    public void clearDatabase() {
        databaseService.clearDatabase();
    }
}

