package net.thumbtack.school.hospital.dto.request;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class RegisterDoctorDtoRequest {
    private String firstName;
    private String lastName;
    private String speciality;
    private String login;
    private String password;

    public RegisterDoctorDtoRequest(String firstName, String lastName, String speciality, String login, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.speciality = speciality;
        this.login = login;
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getSpeciality() {
        return speciality;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
