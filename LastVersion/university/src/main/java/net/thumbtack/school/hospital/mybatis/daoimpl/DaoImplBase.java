package net.thumbtack.school.hospital.mybatis.daoimpl;

import net.thumbtack.school.hospital.mybatis.mappers.AppointmentMapper;
import net.thumbtack.school.hospital.mybatis.mappers.DoctorMapper;
import net.thumbtack.school.hospital.mybatis.mappers.PatientMapper;
import net.thumbtack.school.hospital.mybatis.utils.HospitalUtils;
import org.apache.ibatis.session.SqlSession;

public class DaoImplBase {

    protected SqlSession getSession() {
        return HospitalUtils.getSqlSessionFactory().openSession();
    }

    protected DoctorMapper getDoctorMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(DoctorMapper.class);
    }

    protected PatientMapper getPatientMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(PatientMapper.class);
    }

    protected AppointmentMapper getAppointmentMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(AppointmentMapper.class);
    }

}