package net.thumbtack.school.hospital.service;


import net.thumbtack.school.hospital.dao.DatabaseDao;
import net.thumbtack.school.hospital.daoimpl.DatabaseDaoImpl;
import net.thumbtack.school.hospital.dto.response.BaseResponse;
import net.thumbtack.school.hospital.exceptions.ServerException;

public class ClearDatabaseService {
    private DatabaseDao databaseDao = new DatabaseDaoImpl();

    public BaseResponse clearDatabase() {
        databaseDao.clearDatabase();
        return new BaseResponse();
    }
}
