package net.thumbtack.school.hospital.server;

import net.thumbtack.school.hospital.dto.request.*;
import net.thumbtack.school.hospital.dto.response.BaseResponse;
import net.thumbtack.school.hospital.exceptions.ServerException;
import net.thumbtack.school.hospital.mybatis.mappers.HospitalExceptionMapper;
import net.thumbtack.school.hospital.mybatis.utils.HospitalUtils;
import net.thumbtack.school.hospital.resources.HospitalResourceConfig;
import net.thumbtack.school.hospital.service.ClearDatabaseService;
import net.thumbtack.school.hospital.serviceTask18.DatabaseService;
import net.thumbtack.school.hospital.serviceTask18.DoctorService;
import net.thumbtack.school.hospital.serviceTask18.PatientService;
import net.thumbtack.school.hospital.utils.Settings;
import org.glassfish.jersey.jetty.JettyHttpContainerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.UriBuilder;
import java.net.*;

public class ServerTask18 {
    private final DoctorService doctorService = new DoctorService();
    private final PatientService patientService = new PatientService();
    private static final DatabaseService clearDatabaseService = new DatabaseService();

    private static final Logger LOGGER = LoggerFactory.getLogger(Server.class);
    public static org.eclipse.jetty.server.Server jettyServer;
    private static boolean setUpIsDone = false;

    public ServerTask18() throws ServerException {
        if (!setUpIsDone) {
            boolean initSqlSessionFactory = HospitalUtils.initSqlSessionFactory();
            if (!initSqlSessionFactory) {
                throw new RuntimeException("Can't create connection, stop");
            }
            setUpIsDone = true;
        }
        clearDatabase();
    }

    private static void attachShutDownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(ServerTask18::stopServer));
    }

    public static void stopServer() {
        LOGGER.info("Stopping server");
        try {
            jettyServer.stop();
            jettyServer.destroy();
        } catch (Exception e) {
            LOGGER.error("Error stop service", e);
            System.exit(1);
        }
        LOGGER.info("Server stopped");
    }

    public static void createServer() {
        URI baseUri = UriBuilder.fromUri("http://localhost/").port(Settings.getRestHttpPort()).build();
        HospitalResourceConfig config = new HospitalResourceConfig();
        config.register(HospitalExceptionMapper.class);
        jettyServer = JettyHttpContainerFactory.createServer(baseUri, config);
        LOGGER.info("Server started at port " + Settings.getRestHttpPort());
        if (!setUpIsDone) {
            boolean initSqlSessionFactory = HospitalUtils.initSqlSessionFactory();
            if (!initSqlSessionFactory) {
                throw new RuntimeException("Can't create connection, stop");
            }
            setUpIsDone = true;
        }
        clearDatabase();
    }

/*    public static void main(String[] args) throws ServerException {
        attachShutDownHook();
        createServer();
    }*/

    public BaseResponse loginDoctor(LoginUserDtoRequest loginUserDtoRequest) {
        return doctorService.login(loginUserDtoRequest);
    }

    public BaseResponse logoutDoctor(LogoutUserDtoRequest logoutUserDtoRequest) {
        return doctorService.logout(logoutUserDtoRequest);
    }

    public BaseResponse loginPatient(LoginUserDtoRequest loginUserDtoRequest) {
        return patientService.login(loginUserDtoRequest);
    }

    public BaseResponse logoutPatient(LogoutUserDtoRequest logoutUserDtoRequest) {
        return patientService.logout(logoutUserDtoRequest);
    }

    public BaseResponse registerDoctor(RegisterDoctorDtoRequest registerDoctorDtoRequest) {
        return doctorService.registerDoctor(registerDoctorDtoRequest);
    }

    public BaseResponse addAppointment(AddAppointmentDtoRequest addAppointmentsDtoRequest) {
        return doctorService.addAppointment(addAppointmentsDtoRequest);
    }

    public BaseResponse registerPatient(RegisterPatientDtoRequest registerPatientDtoRequest) {
        return doctorService.registerPatient(registerPatientDtoRequest);
    }

    public BaseResponse deleteDoctor(DeleteUserDtoRequest deleteAccountUserRequest) {
        return doctorService.delete(deleteAccountUserRequest);
    }

    public BaseResponse deletePatient(DeleteUserDtoRequest deleteAccountUserRequest) {
        return patientService.delete(deleteAccountUserRequest);
    }

    public BaseResponse deletePatientByDoctor(DeletePatientByDoctorDtoRequest deletePatientByDoctorDtoRequest) {
        return doctorService.deletePatientByDoctor(deletePatientByDoctorDtoRequest);
    }

    public BaseResponse getDoctorByToken(GetDoctorByTokenDtoRequest getDoctorByTokenDtoRequest) {
        return doctorService.getDoctorByToken(getDoctorByTokenDtoRequest);
    }

    public BaseResponse getPatientByToken(GetPatientByTokenDtoRequest getPatientByTokenDtoRequest) {
        return patientService.getPatientByToken(getPatientByTokenDtoRequest);
    }

    public BaseResponse GetPatientByIDDtoRequest(GetPatientByIDtoRequest getPatientByIDtoRequest) {
        return doctorService.getPatientByID(getPatientByIDtoRequest);
    }

    public BaseResponse getPatientListByDoctor(GetPatientListByDoctorDtoRequest getListPatientByDoctorDtoRequest) {
        return doctorService.getPatientListByDoctor(getListPatientByDoctorDtoRequest);
    }

    public BaseResponse getPatientListByDoctorByDisease(GetPatientListByDoctorByDiseaseDtoRequest getPatientListByDoctorByDiseaseDtoRequest) {
        return doctorService.getPatientListByDoctorByDisease(getPatientListByDoctorByDiseaseDtoRequest);
    }

    public BaseResponse getPatientListByDoctorByAppointments(GetPatientListByDoctorByAppointmentDtoRequest getPatientListByDoctorByAppointmentDtoRequest) {
        return doctorService.getPatientListByDoctorByAppointments(getPatientListByDoctorByAppointmentDtoRequest);
    }

    public BaseResponse getPatientListByDisease(GetPatientListByDiseaseDtoRequest getPatientListByDiseaseDtoRequest) {
        return doctorService.getPatientListByDisease(getPatientListByDiseaseDtoRequest);
    }

    public BaseResponse getPatientListByAppointments(GetPatientListByAppointmentDtoRequest getPatientListByAppointmentsDtoRequest) {
        return doctorService.getPatientListByAppointment(getPatientListByAppointmentsDtoRequest);
    }


    public BaseResponse changePasswordPatient(ChangePatientPasswordDtoRequest changePasswordDtoRequest) {
        return patientService.changePasswordPatient(changePasswordDtoRequest);
    }

    public BaseResponse getDoctorByPatient(GetDoctorByPatientDtoRequest getDoctorByPatientDtoRequest) {
        return patientService.getDoctorByPatient(getDoctorByPatientDtoRequest);
    }

    public BaseResponse getAppointmentsByPatient(GetAppointmentsByPatientDtoRequest getAppointmentsByPatientDtoRequest) {
        return patientService.getAppointmentsByPatient(getAppointmentsByPatientDtoRequest);
    }

    public static BaseResponse clearDatabase() {
        return clearDatabaseService.clearDataBase();
    }
}

