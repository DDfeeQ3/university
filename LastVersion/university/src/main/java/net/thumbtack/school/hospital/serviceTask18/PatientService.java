package net.thumbtack.school.hospital.serviceTask18;

import net.thumbtack.school.hospital.dao.PatientDao;
import net.thumbtack.school.hospital.daoimpl.PatientDaoImpl;
import net.thumbtack.school.hospital.dto.request.*;
import net.thumbtack.school.hospital.dto.response.*;
import net.thumbtack.school.hospital.exceptions.ErrorCode;
import net.thumbtack.school.hospital.exceptions.ServerException;
import net.thumbtack.school.hospital.model.Appointment;
import net.thumbtack.school.hospital.model.Doctor;
import net.thumbtack.school.hospital.model.MethodType;
import net.thumbtack.school.hospital.model.Patient;

import java.util.LinkedList;
import java.util.List;

public class PatientService {
    private PatientDao patientDao = new PatientDaoImpl();

    public BaseResponse login(LoginUserDtoRequest loginUserDtoRequest) {
        try {
            validateLogin(loginUserDtoRequest);

            String token = patientDao.login(loginUserDtoRequest.getLogin(), loginUserDtoRequest.getPassword());

            return new LoginUserDtoResponse(token);
        } catch (ServerException e) {
            return new ErrorResponse(e);
        }
    }

    public BaseResponse logout(LogoutUserDtoRequest logoutUserDtoRequest) {
        try {
            checkUserByToken(logoutUserDtoRequest.getToken());
            patientDao.logout(logoutUserDtoRequest.getToken());

            return new BaseResponse();
        } catch (ServerException e) {
            return new ErrorResponse(e);
        }
    }

    public BaseResponse getDoctorByPatient(GetDoctorByPatientDtoRequest getDoctorByPatientDtoRequest) {
        try {
            Patient patient = patientDao.getPatientByID(getDoctorByPatientDtoRequest.getPatientID());
            Doctor doctor = patientDao.getDoctorByPatient(patient);

            return new DoctorDtoResponse(doctor.getFirstName(), doctor.getLastName(), doctor.getSpeciality());
        } catch (ServerException e) {
            return new ErrorResponse(e);
        }
    }

    public BaseResponse changePasswordPatient(ChangePatientPasswordDtoRequest changePasswordDtoRequest) {
        try {
            if (changePasswordDtoRequest.getNewPassword() == null || changePasswordDtoRequest.getNewPassword().equals("") ||
                    changePasswordDtoRequest.getNewPassword().length() < 6) {
                throw new ServerException(ErrorCode.WRONG_PASSWORD_FORMAT_ERROR);
            }
            Patient patient = patientDao.getByLoginAndPassword(changePasswordDtoRequest.getLogin(), changePasswordDtoRequest.getOldPassword());

            patientDao.changePatientPassword(patient, changePasswordDtoRequest.getNewPassword());

            return new BaseResponse();
        } catch (ServerException e) {
            return new ErrorResponse(e);
        }
    }

    public BaseResponse getAppointmentsByPatient(GetAppointmentsByPatientDtoRequest getAppointmentsByPatientDtoRequest) {
        try {
            Patient patient = patientDao.getPatientByID(getAppointmentsByPatientDtoRequest.getPatientID());
            List<AppointmentDtoResponse> appListResponse = new LinkedList<>();

            if (patient.getAppointments() != null) {
                AppointmentDtoResponse appointmentResponse;
                for (Appointment app : patientDao.getAppointmentsByPatient(patient)) {
                    if (app.getMethodType() == MethodType.MEDICINE) {
                        appointmentResponse = new AppointmentDtoResponse(MethodTypeResponse.MEDICINE, app.getTreatmentMethod(),
                                app.getFrequency());
                    } else {
                        appointmentResponse = new AppointmentDtoResponse(MethodTypeResponse.PROCEDURE, app.getTreatmentMethod(),
                                app.getFrequency());
                    }
                    appListResponse.add(appointmentResponse);
                }
            }
            return new AppointmentListDtoResponse(appListResponse);
        } catch (ServerException e) {
            return new ErrorResponse(e);
        }
    }

    public BaseResponse delete(DeleteUserDtoRequest deleteAccountUserRequest) {
        try {
            checkUserByToken(deleteAccountUserRequest.getToken());

            patientDao.delete(patientDao.getByToken(deleteAccountUserRequest.getToken()));

            return new BaseResponse();
        } catch (ServerException e) {
            return new ErrorResponse(e);
        }
    }

    public BaseResponse getPatientByToken(GetPatientByTokenDtoRequest getPatientByTokenDtoRequest) {
        try {
            Patient patient = patientDao.getByToken(getPatientByTokenDtoRequest.getToken());

            return new PatientDtoResponse(patient.getFirstName(), patient.getLastName(),
                    patient.getId(), patient.getDisease(), patient.getLogin(), patient.getPassword());
        } catch (ServerException e) {
            return new ErrorResponse(e);
        }
    }

    private boolean validateLogin(LoginUserDtoRequest request) throws ServerException {
        if (patientDao.getByLoginAndPassword(request.getLogin(), request.getPassword()) == null) {
            throw new ServerException(ErrorCode.INVALID_LOGIN_OR_PASSWORD_ERROR);
        } else return true;
    }

    private void checkUserByToken(String token) throws ServerException {
        patientDao.getByToken(token);
    }
}
