package net.thumbtack.school.threads;

import java.text.SimpleDateFormat;
import java.util.Date;

class Formatter {
    private static ThreadLocal<SimpleDateFormat> t = ThreadLocal.withInitial(() -> new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));

    public void format(Date date) {
        date = new Date();
        System.out.println(t.get().format(date));
    }
}