package net.thumbtack.school.hospital.mybatis.daoimpl;

import net.thumbtack.school.hospital.model.Appointment;
import net.thumbtack.school.hospital.model.Doctor;
import net.thumbtack.school.hospital.model.Patient;
import net.thumbtack.school.hospital.mybatis.dao.PatientDao;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Comparator;
import java.util.List;

public class PatientDaoImpl extends DaoImplBase implements PatientDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(PatientDaoImpl.class);


    @Override
    public Patient insert(Doctor doctor, Patient patient) throws RuntimeException {
        LOGGER.info("DAO insert Patient {}", patient);
        try (SqlSession sqlSession = getSession()) {
            try {
                getPatientMapper(sqlSession).createAccount(patient);
                getPatientMapper(sqlSession).insert(patient, doctor.getId());
                getDoctorMapper(sqlSession).changeDoctor(patient, doctor);
                for (Appointment appointment : patient.getAppointments()) {
                    getAppointmentMapper(sqlSession).insert(patient.getId(), appointment);
                }
                for (Appointment appointment : patient.getAppointments()) {
                    appointment.setPatientId(patient.getId());
                }
            } catch (RuntimeException e) {
                LOGGER.info("Can't insert Patient {}, {}", patient, e);
                sqlSession.rollback();
                throw e;
            }
            sqlSession.commit();
        }
        return patient;
    }

    @Override
    public Patient getById(int id) {
        LOGGER.info("DAO get Patient by ID {}", id);
        try (SqlSession sqlSession = getSession()) {
            try {
                Patient patient = getPatientMapper(sqlSession).getById(id);
                patient.setDoctor(getDoctorMapper(sqlSession).getByPatientId(patient.getId()));
                patient.setAppointments(getAppointmentMapper(sqlSession).getAppointmentsByPatientId(patient.getId()));
                return patient;
            } catch (RuntimeException e) {
                LOGGER.info("Can't get Patient by ID {}, {}", id, e);
                throw e;
            }
        }
    }

    @Override
    public Patient changePassword(Patient patient, String password) throws RuntimeException {
        LOGGER.info("DAO change password Patient {}, {}", patient, password);
        try (SqlSession sqlSession = getSession()) {
            try {
                getPatientMapper(sqlSession).changePassword(patient, password);
                patient.setPassword(password);
            } catch (RuntimeException e) {
                LOGGER.info("Can't update Patient {}, {}, {}", patient, password, e);
                sqlSession.rollback();
                throw e;
            }
            sqlSession.commit();
        }
        return patient;
    }

    @Override
    public List<Appointment> getAllAppointmentsByPatient(Patient patient) throws RuntimeException {
        LOGGER.info("DAO get all Appointment by Patient {}", patient);
        try (SqlSession sqlSession = getSession()) {
            try {
                List<Appointment> appointmentList = getAppointmentMapper(sqlSession).getAppointmentsByPatientId(patient.getId());
                appointmentList.sort(Comparator.comparingInt(Appointment::getId));
                return appointmentList;
            } catch (RuntimeException e) {
                LOGGER.info("Can't get all Appointment by Patient {}, {}", patient, e);
                throw e;
            }
        }
    }

    @Override
    public Doctor getDoctorByPatient(Patient patient) throws RuntimeException {
            LOGGER.info("DAO get Doctor by Patient {}", patient);
            try (SqlSession sqlSession = getSession()) {
                try {
                    return getDoctorMapper(sqlSession).getByPatientId(patient.getId());
                } catch (RuntimeException e) {
                    LOGGER.info("Can't get Doctor by Patient Id {}", patient, e);
                    throw e;
                }
            }
    }

    @Override
    public void delete(Patient patient) throws RuntimeException {
        LOGGER.info("DAO delete Patient {}", patient);
        try (SqlSession sqlSession = getSession()) {
            try {
                getPatientMapper(sqlSession).delete(patient);
            } catch (RuntimeException e) {
                LOGGER.info("Can't delete Patient {}, {}", patient, e);
                sqlSession.rollback();
                throw e;
            }
            sqlSession.commit();
        }
    }
}
