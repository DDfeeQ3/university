package net.thumbtack.school.hospital.service;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.internal.Primitives;
import net.thumbtack.school.hospital.dao.PatientDao;
import net.thumbtack.school.hospital.daoimpl.PatientDaoImpl;
import net.thumbtack.school.hospital.dto.request.*;
import net.thumbtack.school.hospital.dto.response.*;
import net.thumbtack.school.hospital.exceptions.ErrorCode;
import net.thumbtack.school.hospital.exceptions.ServerException;
import net.thumbtack.school.hospital.model.*;

import java.util.LinkedList;
import java.util.List;

public class PatientService {
    private Gson gson = new Gson();
    private PatientDao patientDao = new PatientDaoImpl();

    public <T> T getClassFromJson(String json, Class<T> classOfT) throws ServerException {
        try {
            T t = gson.fromJson(json, classOfT);
            return Primitives.wrap(classOfT).cast(t);
        } catch (JsonSyntaxException e) {
            throw new ServerException(ErrorCode.WRONG_JSON_ERROR);
        }
    }

    public String login(String json) {
        try {
            LoginUserDtoRequest loginUserDtoRequest = getClassFromJson(json, LoginUserDtoRequest.class);
            validateLogin(loginUserDtoRequest);

            String token = patientDao.login(loginUserDtoRequest.getLogin(), loginUserDtoRequest.getPassword());

            return gson.toJson(new LoginUserDtoResponse(token));//сделать класс
        } catch (ServerException e) {
            return gson.toJson(new ErrorResponse(e));
        }
    }

    public String logout(String json) {
        try {
            LogoutUserDtoRequest logoutUserDtoRequest = getClassFromJson(json, LogoutUserDtoRequest.class);
            checkUserByToken(logoutUserDtoRequest.getToken());
            patientDao.logout(logoutUserDtoRequest.getToken());

            return gson.toJson(null);
        } catch (ServerException e) {
            return gson.toJson(new ErrorResponse(e));
        }
    }

    public String getDoctorByPatient(String json) {
        try {
            GetDoctorByPatientDtoRequest getDoctorByPatientDtoRequest = getClassFromJson(json, GetDoctorByPatientDtoRequest.class);
            Patient patient = patientDao.getPatientByID(getDoctorByPatientDtoRequest.getPatientID());
            Doctor doctor = patientDao.getDoctorByPatient(patient);

            DoctorDtoResponse doctorDtoResponse = new DoctorDtoResponse(doctor.getFirstName(), doctor.getLastName(), doctor.getSpeciality());
            return gson.toJson(doctorDtoResponse);
        } catch (ServerException e) {
            return gson.toJson(new ErrorResponse(e));
        }
    }

    public String getDoctorInfoByPatient(String json) {
        try {
            GetInfoAboutDoctorByPatientDtoRequest getInfoAboutDoctorByPatientDtoRequest = getClassFromJson(json,
                    GetInfoAboutDoctorByPatientDtoRequest.class);
            Patient patient = patientDao.getByToken(getInfoAboutDoctorByPatientDtoRequest.getToken());

            Doctor doctor = patient.getDoctor();

            DoctorDtoResponse getInfoAboutDoctorDtoResponse = new DoctorDtoResponse(doctor.getFirstName(),
                    doctor.getLastName(), doctor.getSpeciality());
            return gson.toJson(getInfoAboutDoctorDtoResponse);
        } catch (ServerException e) {
            return gson.toJson(new ErrorResponse(e));
        }
    }

    public String changePasswordPatient(String json) {
        try {
            ChangePatientPasswordDtoRequest changePasswordDtoRequest = getClassFromJson(json, ChangePatientPasswordDtoRequest.class);
            if (changePasswordDtoRequest.getNewPassword() == null || changePasswordDtoRequest.getNewPassword().equals("") ||
                    changePasswordDtoRequest.getNewPassword().length() < 6) {
                throw new ServerException(ErrorCode.WRONG_PASSWORD_FORMAT_ERROR);
            }
            Patient patient = patientDao.getByLoginAndPassword(changePasswordDtoRequest.getLogin(), changePasswordDtoRequest.getOldPassword());

            patientDao.changePatientPassword(patient, changePasswordDtoRequest.getNewPassword());

            return gson.toJson(null);
        } catch (ServerException e) {
            return gson.toJson(new ErrorResponse(e));
        }
    }

    public String getAppointmentsByPatient(String json) {
        try {
            GetAppointmentsByPatientDtoRequest getAppointmentsByPatientDtoRequest = getClassFromJson(json, GetAppointmentsByPatientDtoRequest.class);
            Patient patient = patientDao.getPatientByID(getAppointmentsByPatientDtoRequest.getPatientID());
            List<AppointmentDtoResponse> appListResponse = new LinkedList<>();

            if (patient.getAppointments() != null) {
                AppointmentDtoResponse appointmentResponse;
                for (Appointment app : patientDao.getAppointmentsByPatient(patient)) {
                    if (app.getMethodType() == MethodType.MEDICINE) {
                        appointmentResponse = new AppointmentDtoResponse(MethodTypeResponse.MEDICINE, app.getTreatmentMethod(),
                                app.getFrequency());
                    } else {
                        appointmentResponse = new AppointmentDtoResponse(MethodTypeResponse.PROCEDURE, app.getTreatmentMethod(),
                                app.getFrequency());
                    }
                    appListResponse.add(appointmentResponse);
                }
            }
            AppointmentListDtoResponse appointmentListDtoResponse = new AppointmentListDtoResponse(appListResponse);
            return gson.toJson(appointmentListDtoResponse);
        } catch (ServerException e) {
            return gson.toJson(new ErrorResponse(e));
        }
    }

    public String delete(String json) {
        try {
            DeleteUserDtoRequest deleteAccountUserRequest = getClassFromJson(json, DeleteUserDtoRequest.class);
            checkUserByToken(deleteAccountUserRequest.getToken());

            patientDao.delete(patientDao.getByToken(deleteAccountUserRequest.getToken()));

            return gson.toJson(null);
        } catch (ServerException e) {
            return gson.toJson(new ErrorResponse(e));
        }
    }

    public String getPatientByToken(String json) {
        try {
            GetPatientByTokenDtoRequest getPatientByTokenDtoRequest = getClassFromJson(json,
                    GetPatientByTokenDtoRequest.class);
            Patient patient = patientDao.getByToken(getPatientByTokenDtoRequest.getToken());

            PatientDtoResponse patientDtoResponse = new PatientDtoResponse(patient.getFirstName(), patient.getLastName(),
                    patient.getId(), patient.getDisease(), patient.getLogin(), patient.getPassword());
            return gson.toJson(patientDtoResponse);
        } catch (ServerException e) {
            return gson.toJson(new ErrorResponse(e));
        }
    }

    private boolean validateLogin(LoginUserDtoRequest request) throws ServerException {
        if (patientDao.getByLoginAndPassword(request.getLogin(), request.getPassword()) == null) {
            throw new ServerException(ErrorCode.INVALID_LOGIN_OR_PASSWORD_ERROR);
        } else return true;
    }

    private void checkUserByToken(String token) throws ServerException {
        patientDao.getByToken(token);
    }
}
