package net.thumbtack.school.threads;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

class Message {
    private String emailAddress;
    private String sender;
    private String subject;
    private String body;

    public Message(String emailAddress, String sender, String subject, String body) {
        this.emailAddress = emailAddress;
        this.sender = sender;
        this.subject = subject;
        this.body = body;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}

class Transport {
    private final File file = new File("messaged.txt");

    public void send(Message message) {
        FileWriter fr = null;
        try {
            fr = new FileWriter(file, true);
            fr.append("От: ").append(message.getSender());
            fr.append("\nКому: ").append(message.getEmailAddress());
            fr.append("\nПредмет: ").append(message.getSubject());
            fr.append("\nСодержание: ").append(message.getBody()).append("\n\n");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
