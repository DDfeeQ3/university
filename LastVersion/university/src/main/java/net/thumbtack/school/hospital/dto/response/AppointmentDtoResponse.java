package net.thumbtack.school.hospital.dto.response;

import lombok.NoArgsConstructor;

import java.util.Objects;

@NoArgsConstructor
public class AppointmentDtoResponse extends BaseResponse {
    private String treatmentMethod;
    private int frequency;
    private MethodTypeResponse methodType;

    public AppointmentDtoResponse(MethodTypeResponse methodType, String treatmentMethod, int frequency) {
        this.treatmentMethod = treatmentMethod;
        this.frequency = frequency;
        this.methodType = methodType;
    }

    public MethodTypeResponse getMethodType() {
        return methodType;
    }

    public String getTreatmentMethod() {
        return treatmentMethod;
    }

    public int getFrequency() {
        return frequency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AppointmentDtoResponse that = (AppointmentDtoResponse) o;
        return frequency == that.frequency && treatmentMethod.equals(that.treatmentMethod) && methodType == that.methodType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(treatmentMethod, frequency, methodType);
    }
}
