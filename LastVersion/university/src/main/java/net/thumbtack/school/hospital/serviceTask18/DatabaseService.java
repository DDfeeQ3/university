package net.thumbtack.school.hospital.serviceTask18;

import net.thumbtack.school.hospital.dao.DoctorDao;
import net.thumbtack.school.hospital.daoimpl.DoctorDaoImpl;
import net.thumbtack.school.hospital.dto.response.BaseResponse;
import net.thumbtack.school.hospital.exceptions.ServerException;

public class DatabaseService {
    private DoctorDao doctorDao = new DoctorDaoImpl();

    public BaseResponse clearDataBase() {
        try {
            doctorDao.deleteAll();
            return new BaseResponse();
        } catch (ServerException e) {
            return new BaseResponse();
        }
    }
}
