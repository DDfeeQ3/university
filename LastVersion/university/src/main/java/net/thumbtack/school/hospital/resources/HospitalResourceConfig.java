package net.thumbtack.school.hospital.resources;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;

public class HospitalResourceConfig extends ResourceConfig {

    public HospitalResourceConfig() {
        packages("net.thumbtack.school.hospital.resources",
                "net.thumbtack.school.hospital.mybatis.mappers");
        property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);
    }
}
