package net.thumbtack.school.threads;


class ChildThread extends Thread {
    public ChildThread() {
        super("MyThread");
    }

    public void run() {
        try {
            for (int i = 5; i > 0; i--) {
                System.out.println("Child Thread: " + i);
                Thread.sleep(500);
            }
        } catch (InterruptedException e) {
            System.out.println("Child interrupted.");
        }
    }
}

class MyThread extends Thread {
    private final String name;

    public MyThread(String threadName) {
        name = threadName;
    }

    public void run() {
        try {
            for (int i = 5; i > 0; i--) {
                System.out.println(name + ": " + i);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            System.out.println(name + "Interrupted");
        }
    }
}
