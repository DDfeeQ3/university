package net.thumbtack.school.hospital.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Settings {
    private static int restHttpPort = 8080;
    private String typeDatabase;

    public Settings() {
        this.typeDatabase = getType();
    }


    private String getType() {
        String rez = "";
        Properties props = new Properties();
        try {
            props.load(new FileInputStream(new File("src/main/resources/hospital.properties")));
            rez = props.getProperty("typeDatabase");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rez;
    }

    public static int getRestHttpPort(){
        return restHttpPort;
    }

    public String getTypeDatabase() {
        return typeDatabase;
    }
}
