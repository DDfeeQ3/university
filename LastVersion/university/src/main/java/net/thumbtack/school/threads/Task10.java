package net.thumbtack.school.threads;

import java.util.List;
import java.util.concurrent.locks.Lock;

class AddIntThreadTask10 extends Thread {
    private final Lock lock;
    private final List<Integer> list;

    public AddIntThreadTask10(List<Integer> list, Lock lock) {
        this.lock = lock;
        this.list = list;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            try {
                lock.lock();
                list.add((int) (Math.random() * 10000));
                Thread.sleep(1);
            } catch (InterruptedException exc) {
                System.out.println(exc);
            } finally {
                lock.unlock();
            }
        }
    }
}

class DeleteIntThreadTask10 extends Thread {
    private final Lock lock;
    private final List<Integer> list;

    public DeleteIntThreadTask10(List<Integer> list, Lock lock) {
        this.list = list;
        this.lock = lock;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            try {
                lock.lock();
                if (list.size() != 0) {
                    list.remove((int) (Math.random() * list.size()));
                }
                Thread.sleep(1);
            } catch (InterruptedException exc) {
                System.out.println(exc);
            } finally {
                lock.unlock();
            }
        }
    }

}