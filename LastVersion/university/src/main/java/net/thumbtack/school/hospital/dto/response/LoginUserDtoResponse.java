package net.thumbtack.school.hospital.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class LoginUserDtoResponse extends BaseResponse {
    private String token;

    public LoginUserDtoResponse(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
