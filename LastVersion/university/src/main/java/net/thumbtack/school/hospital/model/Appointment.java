package net.thumbtack.school.hospital.model;

import java.util.Objects;


public class Appointment {
    private int id;
    private int frequency;
    private int patientId;
    private String treatmentMethod;
    private MethodType methodType;

    public Appointment(MethodType methodType, String treatmentMethod, int frequency) {
        setMethodType(methodType);
        setFrequency(frequency);
        setTreatmentMethod(treatmentMethod);
    }

    public Appointment(int id, String treatmentMethod, MethodType methodType, int frequency) {
        this(methodType, treatmentMethod, frequency);
        setId(id);
    }

    public Appointment(int id, String treatmentMethod, MethodType methodType, int frequency, Patient patient) {
        this(id, treatmentMethod, methodType, frequency);
        setPatientId(patient.getId());
    }

    public Appointment(int id, String treatmentMethod, MethodType methodType, int frequency, int patientId) {
        this(id, treatmentMethod, methodType, frequency);
        setPatientId(patientId);
    }

    public String getTreatmentMethod() {
        return treatmentMethod;
    }

    public void setTreatmentMethod(String methodName) {
        this.treatmentMethod = methodName;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public MethodType getMethodType() {
        return methodType;
    }

    public void setMethodType(MethodType methodType) {
        this.methodType = methodType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Appointment)) return false;
        Appointment that = (Appointment) o;
        return getId() == that.getId() && getFrequency() == that.getFrequency() && getPatientId() == that.getPatientId() && Objects.equals(getTreatmentMethod(), that.getTreatmentMethod()) && getMethodType() == that.getMethodType();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFrequency(), getPatientId(), getTreatmentMethod(), getMethodType());
    }
}
