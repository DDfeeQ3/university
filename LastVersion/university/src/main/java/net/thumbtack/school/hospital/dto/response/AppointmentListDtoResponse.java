package net.thumbtack.school.hospital.dto.response;

import lombok.NoArgsConstructor;
import java.util.List;
import java.util.Objects;

@NoArgsConstructor
public class AppointmentListDtoResponse extends BaseResponse {
    private List<AppointmentDtoResponse> appointments;

    public AppointmentListDtoResponse(List<AppointmentDtoResponse> appointments) {
        this.appointments = appointments;
    }

    public List<AppointmentDtoResponse> getAppointments() {
        return appointments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AppointmentListDtoResponse that = (AppointmentListDtoResponse) o;
        return appointments.equals(that.appointments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(appointments);
    }
}
