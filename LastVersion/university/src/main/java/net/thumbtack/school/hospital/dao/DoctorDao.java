package net.thumbtack.school.hospital.dao;

import net.thumbtack.school.hospital.exceptions.ServerException;
import net.thumbtack.school.hospital.model.*;

import java.util.List;


public interface DoctorDao {
    String insert(Doctor doctor) throws ServerException;

    int addAppointment(int patientID, Appointment appointment) throws ServerException;

    String login(Doctor doctor) throws ServerException;

    void logout(String token) throws ServerException;

    Doctor getByLoginAndPassword(String login, String password) throws ServerException;

    Doctor getByToken(String token) throws ServerException;

    Doctor getByLoginForPatient(String login) throws ServerException;

    Doctor getById(int id) throws ServerException;

    List<Doctor> getAllDoctors() throws ServerException;

    List<Patient> getPatientListByDoctor(Doctor doctor) throws ServerException;

    List<Patient> getPatientListByDoctorByDisease(Doctor doctor, String disease) throws ServerException;

    List<Patient> getPatientListByDoctorByAppointment(Doctor doctor, Appointment appointment) throws ServerException;

    List<Patient> getPatientListByDisease(String disease) throws ServerException;

    List<Patient> getPatientListByAppointment(Appointment appointment) throws ServerException;

    void deletePatient(Patient patient) throws ServerException;

    void delete(Doctor doctor) throws ServerException;

    void deleteAll() throws ServerException;
}
