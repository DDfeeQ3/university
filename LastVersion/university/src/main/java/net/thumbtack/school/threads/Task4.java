package net.thumbtack.school.threads;

import java.util.List;

class AddIntThreadTask4 extends Thread {

    private final List<Integer> list;

    public AddIntThreadTask4(List<Integer> list) {
        this.list = list;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            try {
                Thread.sleep(1);
                synchronized (list) {
                    list.add((int) (Math.random() * 10000));
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class DeleteIntThreadTask4 extends Thread {

    private final List<Integer> list;

    public DeleteIntThreadTask4(List<Integer> list) {
        this.list = list;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            try {
                Thread.sleep(1);
                synchronized (list) {
                    if (list.size() != 0) {
                        list.remove((int) (Math.random() * list.size()));
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
