package net.thumbtack.school.hospital.dto.request;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class GetPatientByIDtoRequest {
    private int patientID;

    public GetPatientByIDtoRequest(int patientID) {
        setPatientID(patientID);
    }

    public int getPatientID() {
        return patientID;
    }

    public void setPatientID(int patientID) {
        this.patientID = patientID;
    }
}
