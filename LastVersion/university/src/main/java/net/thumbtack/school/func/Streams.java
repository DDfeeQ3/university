package net.thumbtack.school.func;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.IntUnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Streams {
    public static IntStream transform(IntStream stream, IntUnaryOperator op) {
        return stream.flatMap(x -> IntStream.of(op.applyAsInt(x)));
    }

    public static IntStream transform2(IntStream stream, IntUnaryOperator op) {
        return stream.parallel().flatMap(x -> IntStream.of(op.applyAsInt(x)));
    }

    public static int sum(List<Integer> list) {
        return list.stream().reduce(0, Integer::sum);
    }

    public static int product(List<Integer> list) {
        return list.stream().reduce(1, (x, y) -> x * y);
    }

    public static void main(String[] args) {

        System.out.println("Задание 13:");
        IntStream intStream = IntStream.of(1, 2, 3);
        IntUnaryOperator op = a -> 2 * a;
        transform(intStream, op).forEach(System.out::println);

        System.out.println();
        System.out.println("Задание 14:");
        IntStream intStream2 = IntStream.of(1, 2, 3);
        IntUnaryOperator op2 = a -> 2 * a;
        transform2(intStream2, op2).forEach(System.out::println);

        System.out.println();
        System.out.println("Задание 15:");
        List<Person> personList = new LinkedList<>();
        personList.add(new Person("Иван", 31));
        personList.add(new Person("Иван", 32));
        personList.add(new Person("Иван", 33));
        personList.add(new Person("Иван", 34));
        personList.add(new Person("Матвей", 19));
        personList.add(new Person("Матвей", 45));
        personList.add(new Person("Дима", 24));
        personList.add(new Person("Дима", 30));
        personList.add(new Person("Катя", 33));
        personList.stream().filter(person -> person.getAge() > 30).map(Person::getName).distinct().sorted(Comparator.comparingInt(String::length)).forEach(System.out::println);

        System.out.println();
        System.out.println("Задание 16:");
        personList.stream().filter(person -> person.getAge() > 30)
                .map(Person::getName)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .sorted(Map.Entry.<String, Long>comparingByValue().reversed())
                .map(Map.Entry::getKey)
                .forEach(System.out::println);

        System.out.println();
        System.out.println("Задание 17:");
        List<Integer> integers = new LinkedList<>();
        integers.add(5);
        integers.add(2);
        integers.add(3);
        integers.add(4);
        System.out.println("Сумма = " + sum(integers));
        System.out.println("Произведение = " + product(integers));
    }
}
