package net.thumbtack.school.threads;

import java.util.concurrent.Semaphore;

class PingPong {
    private final Semaphore smfPing = new Semaphore(1);
    private final Semaphore smfPong = new Semaphore(0);

    public void printPing() {
        try {
            smfPing.acquire();
            System.out.println("ping");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            smfPong.release();
        }
    }

    public void printPong() {
        try {
            smfPong.acquire();
            System.out.println("pong");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            smfPing.release();
        }
    }
}
