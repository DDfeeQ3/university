package net.thumbtack.school.hospital.daoimpl;

import net.thumbtack.school.hospital.dao.DoctorDao;
import net.thumbtack.school.hospital.database.Database;
import net.thumbtack.school.hospital.exceptions.ErrorCode;
import net.thumbtack.school.hospital.exceptions.ServerException;
import net.thumbtack.school.hospital.model.*;
import net.thumbtack.school.hospital.mybatis.daoimpl.DaoImplBase;
import net.thumbtack.school.hospital.utils.Settings;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class DoctorDaoImpl extends DaoImplBase implements DoctorDao {
    private final Database database = Database.getInstance();
    private static final String typeDatabase = new Settings().getTypeDatabase();
    private static final Logger LOGGER = LoggerFactory.getLogger(DoctorDaoImpl.class);

    @Override
    public String insert(Doctor doctor) throws ServerException {
        if (typeDatabase.equals("ramDatabase")) {
            return database.registerDoctor(doctor);
        } else if (typeDatabase.equals("mySql")) {
            LOGGER.debug("DAO insert Doctor {}", doctor);
            String token = (UUID.randomUUID()).toString();
            try (SqlSession sqlSession = getSession()) {
                try {
                    if (getByLogin(doctor.getLogin()) != null) {
                        throw new ServerException(ErrorCode.LOGIN_ALREADY_EXISTS_ERROR);
                    }
                    getDoctorMapper(sqlSession).createAccount(doctor);
                    getDoctorMapper(sqlSession).insert(doctor);
                    getDoctorMapper(sqlSession).loginDoctor(doctor, token);
                } catch (RuntimeException e) {
                    LOGGER.info("Can't insert Doctor {}, {}", doctor, e);
                    sqlSession.rollback();
                    throw e;
                }
                sqlSession.commit();
            }
            return token;
        } else {
            throw new ServerException(ErrorCode.UNKNOWN_DATABASE_TYPE_ERROR);
        }
    }

    @Override
    public Doctor getByToken(String token) throws ServerException {
        if (typeDatabase.equals("ramDatabase")) {
            User user = database.getUserByToken(token);
            if (user == null) {
                throw new ServerException(ErrorCode.NON_LOGGED_USER_ERROR);
            }
            if (!user.getClass().getName().equals(Doctor.class.getName())) {
                throw new ServerException(ErrorCode.NOT_DOCTOR_ERROR);
            }
            return (Doctor) user;
        } else if (typeDatabase.equals("mySql")) {
            LOGGER.debug("DAO get Doctor by token {}", token);
            try (SqlSession sqlSession = getSession()) {
                try {
                    Doctor doctor = getDoctorMapper(sqlSession).getByToken(token);
                    if (doctor == null) {
                        throw new ServerException(ErrorCode.NON_LOGGED_USER_ERROR);
                    }
                    doctor.setPatientList(getDoctorMapper(sqlSession).getPatientListByDoctorId(doctor.getId()));
                    return doctor;
                } catch (RuntimeException e) {
                    LOGGER.info("Can't get Doctor by token {}, {}", token, e);
                    throw e;
                }
            }
        } else {
            throw new ServerException(ErrorCode.UNKNOWN_DATABASE_TYPE_ERROR);
        }
    }

    @Override
    public Doctor getByLoginForPatient(String login) throws ServerException {
        if (typeDatabase.equals("ramDatabase")) {
            User user = database.getUserByLogin(login);
            if (user == null) {
                throw new ServerException(ErrorCode.NON_LOGGED_USER_ERROR);
            }
            if (!user.getClass().getName().equals(Doctor.class.getName())) {
                throw new ServerException(ErrorCode.NOT_DOCTOR_ERROR);
            }
            return (Doctor) user;
        } else if (typeDatabase.equals("mySql")) {
            LOGGER.debug("DAO get Doctor by login {}", login);
            try (SqlSession sqlSession = getSession()) {
                try {
                    Doctor doctor = getDoctorMapper(sqlSession).getByLogin(login);
                    if (doctor == null) {
                        throw new ServerException(ErrorCode.NON_LOGGED_USER_ERROR);
                    }
                    doctor.setPatientList(getDoctorMapper(sqlSession).getPatientListByDoctorId(doctor.getId()));
                    return doctor;
                } catch (RuntimeException e) {
                    LOGGER.info("Can't get Doctor by login {}, {}", login, e);
                    throw e;
                }
            }
        } else {
            throw new ServerException(ErrorCode.UNKNOWN_DATABASE_TYPE_ERROR);
        }
    }

    @Override
    public Doctor getById(int id) throws ServerException {
        if (typeDatabase.equals("ramDatabase")) {
            User user = database.getUserByID(id);
            if (user == null) {
                throw new ServerException(ErrorCode.NON_LOGGED_USER_ERROR);
            }
            if (!user.getClass().getName().equals(Doctor.class.getName())) {
                throw new ServerException(ErrorCode.NOT_DOCTOR_ERROR);
            }
            return (Doctor) user;
        } else if (typeDatabase.equals("mySql")) {
            LOGGER.debug("DAO get Doctor by ID {}", id);
            try (SqlSession sqlSession = getSession()) {
                try {
                    Doctor doctor = getDoctorMapper(sqlSession).getById(id);
                    doctor.setPatientList(getDoctorMapper(sqlSession).getPatientListByDoctorId(doctor.getId()));
                    return doctor;
                } catch (RuntimeException e) {
                    LOGGER.info("Can't get Doctor by ID {}, {}", id, e);
                    throw e;
                }
            }
        } else {
            throw new ServerException(ErrorCode.UNKNOWN_DATABASE_TYPE_ERROR);
        }
    }

    @Override
    public List<Doctor> getAllDoctors() throws ServerException {
        if (typeDatabase.equals("ramDatabase")) {
            return database.getAllDoctors();
        } else if (typeDatabase.equals("mySql")) {
            LOGGER.debug("DAO get all Doctors");
            try (SqlSession sqlSession = getSession()) {
                try {
                    List<Doctor> doctorList = getDoctorMapper(sqlSession).getAllDoctors();
                    doctorList.sort(Comparator.comparingInt(Doctor::getId));
                    return doctorList;
                } catch (RuntimeException e) {
                    LOGGER.info("Can't get all Doctors {}", e);
                    throw e;
                }
            }
        } else {
            throw new ServerException(ErrorCode.UNKNOWN_DATABASE_TYPE_ERROR);
        }
    }

    @Override
    public int addAppointment(int patientID, Appointment appointment) throws ServerException {
        int appId = -1;
        if (typeDatabase.equals("ramDatabase")) {
            if (!database.getUserByID(patientID).getClass().equals(Patient.class)) {
                throw new ServerException(ErrorCode.PATIENT_NOT_FOUND_ERROR);
            }
            Patient patient = (Patient) database.getUserByID(patientID);
            if (patient.getAppointments() == null) {
                List<Appointment> app = new LinkedList<>();
                app.add(appointment);
                patient.setAppointments(app);
                appId = 1;
            } else {
                appId = patient.getAppointments().size();
                patient.getAppointments().add(appointment);
            }
            return appId;
        } else if (typeDatabase.equals("mySql")) {
            LOGGER.info("DAO add  Appointment {}", appointment);
            try (SqlSession sqlSession = getSession()) {
                try {
                    getAppointmentMapper(sqlSession).insert(patientID, appointment);
                    appId = appointment.getId();
                    appointment.setPatientId(patientID);
                } catch (RuntimeException e) {
                    LOGGER.info("Can't add  Appointment {}, {}", appointment, e);
                    sqlSession.rollback();
                    throw e;
                }
                sqlSession.commit();
                return appId;
            }
        } else {
            throw new ServerException(ErrorCode.UNKNOWN_DATABASE_TYPE_ERROR);
        }
    }

    @Override
    public String login(Doctor doctor) throws ServerException {
        if (typeDatabase.equals("ramDatabase")) {
            return database.loginUser(doctor);
        } else if (typeDatabase.equals("mySql")) {
            LOGGER.debug("DAO login Doctor {}", doctor);
            String token = (UUID.randomUUID()).toString();
            try (SqlSession sqlSession = getSession()) {
                try {
                    if (getDoctorMapper(sqlSession).getTokenById(doctor.getId()) != null) {
                        throw new ServerException(ErrorCode.USER_ALREADY_AUTHORIZED_ERROR);
                    }
                    getDoctorMapper(sqlSession).loginDoctor(doctor, token);
                    sqlSession.commit();
                    return token;
                } catch (RuntimeException e) {
                    LOGGER.info("Can't login Doctor {}, {}", doctor, e);
                    sqlSession.rollback();
                    throw e;
                }
            }
        } else {
            throw new ServerException(ErrorCode.UNKNOWN_DATABASE_TYPE_ERROR);
        }
    }

    @Override
    public void logout(String token) throws ServerException {
        if (typeDatabase.equals("ramDatabase")) {
            database.logoutUser(token);
        } else if (typeDatabase.equals("mySql")) {
            LOGGER.debug("DAO logout Doctor {}", token);
            try (SqlSession sqlSession = getSession()) {
                try {
                    if (getByToken(token) == null) {
                        throw new ServerException(ErrorCode.NON_LOGGED_USER_ERROR);
                    }
                    getDoctorMapper(sqlSession).logoutDoctor(token);
                } catch (RuntimeException e) {
                    LOGGER.info("Can't logout Doctor {}, {}", token, e);
                    sqlSession.rollback();
                    throw e;
                }
                sqlSession.commit();
            }
        } else {
            throw new ServerException(ErrorCode.UNKNOWN_DATABASE_TYPE_ERROR);
        }
    }

    @Override
    public Doctor getByLoginAndPassword(String login, String password) throws ServerException {
        if (typeDatabase.equals("ramDatabase")) {
            User user = database.getByLoginAndPassword(login, password);
            if (!user.getClass().getName().equals(Doctor.class.getName())) {
                throw new ServerException(ErrorCode.NOT_DOCTOR_ERROR);
            }
            return (Doctor) user;
        } else if (typeDatabase.equals("mySql")) {
            LOGGER.debug("DAO get Doctor by login and password {}, {}", login, password);
            try (SqlSession sqlSession = getSession()) {
                try {
                    return getDoctorMapper(sqlSession).getByLoginAndPassword(login, password);
                } catch (RuntimeException e) {
                    LOGGER.info("Can't get Doctor by login and password {}, {}, {}", login, password, e);
                    throw e;
                }
            }
        } else {
            throw new ServerException(ErrorCode.UNKNOWN_DATABASE_TYPE_ERROR);
        }
    }

    @Override
    public List<Patient> getPatientListByDoctor(Doctor doctor) throws ServerException {
        if (typeDatabase.equals("ramDatabase")) {
            return database.getPatientListByDoctor(doctor);
        } else if (typeDatabase.equals("mySql")) {
            LOGGER.info("DAO get Patient List by doctor {}", doctor);
            try (SqlSession sqlSession = getSession()) {
                try {
                    List<Patient> patientList = getDoctorMapper(sqlSession).getPatientListByDoctorId(doctor.getId());
                    patientSort(patientList);
                    return patientList;
                } catch (RuntimeException e) {
                    LOGGER.info("Can't get Patient List by doctor {}, {}", doctor, e);
                    throw e;
                }
            }
        } else {
            throw new ServerException(ErrorCode.UNKNOWN_DATABASE_TYPE_ERROR);
        }
    }

    @Override
    public List<Patient> getPatientListByDoctorByDisease(Doctor doctor, String disease) throws ServerException {
        if (typeDatabase.equals("ramDatabase")) {
            return database.getPatientListByDoctorByDisease(doctor, disease);
        } else if (typeDatabase.equals("mySql")) {
            LOGGER.info("DAO get Patient List by doctor and by disease {}, {}", doctor, disease);
            try (SqlSession sqlSession = getSession()) {
                try {
                    List<Patient> patientList = getDoctorMapper(sqlSession).getPatientListByDoctorIdAndByDisease(doctor.getId(), disease);
                    patientSort(patientList);
                    return patientList;
                } catch (RuntimeException e) {
                    LOGGER.info("Can't get Patient List by doctor and by disease {}, {}, {}", doctor, disease, e);
                    throw e;
                }
            }
        } else {
            throw new ServerException(ErrorCode.UNKNOWN_DATABASE_TYPE_ERROR);
        }
    }

    @Override
    public List<Patient> getPatientListByDoctorByAppointment(Doctor doctor, Appointment appointment) throws ServerException {
        if (typeDatabase.equals("ramDatabase")) {
            return database.getPatientListByDoctorByAppointment(doctor, appointment);
        } else if (typeDatabase.equals("mySql")) {
            LOGGER.info("DAO get Patient List by doctor and by appointment {}, {}", doctor, appointment);
            try (SqlSession sqlSession = getSession()) {
                try {
                    List<Patient> patientList = getDoctorMapper(sqlSession).getPatientListByDoctorIdAndByAppointment(doctor.getId(), appointment);
                    patientSort(patientList);
                    return patientList;
                } catch (RuntimeException e) {
                    LOGGER.info("Can't get Patient List by doctor and by appointment {}, {}, {}", doctor, appointment, e);
                    throw e;
                }
            }
        } else {
            throw new ServerException(ErrorCode.UNKNOWN_DATABASE_TYPE_ERROR);
        }
    }

    @Override
    public List<Patient> getPatientListByDisease(String disease) throws ServerException {
        if (typeDatabase.equals("ramDatabase")) {
            return database.getPatientListByDisease(disease);
        } else if (typeDatabase.equals("mySql")) {
            LOGGER.info("DAO get Patient List by disease {}", disease);
            try (SqlSession sqlSession = getSession()) {
                try {
                    List<Patient> patientList = getDoctorMapper(sqlSession).getPatientListByDisease(disease);
                    patientSort(patientList);
                    return patientList;
                } catch (RuntimeException e) {
                    LOGGER.info("Can't get Patient List by disease {}, {}", disease, e);
                    throw e;
                }
            }
        } else {
            throw new ServerException(ErrorCode.UNKNOWN_DATABASE_TYPE_ERROR);
        }
    }

    @Override
    public List<Patient> getPatientListByAppointment(Appointment appointment) throws ServerException {
        if (typeDatabase.equals("ramDatabase")) {
            return database.getPatientListByAppointment(appointment);
        } else if (typeDatabase.equals("mySql")) {
            LOGGER.info("DAO get Patient List by appointment {}", appointment);
            try (SqlSession sqlSession = getSession()) {
                try {
                    List<Patient> patientList = getDoctorMapper(sqlSession).getPatientListByAppointment(appointment);
                    patientSort(patientList);
                    return patientList;
                } catch (RuntimeException e) {
                    LOGGER.info("Can't get Patient List by appointment {}, {}", appointment, e);
                    throw e;
                }
            }
        } else {
            throw new ServerException(ErrorCode.UNKNOWN_DATABASE_TYPE_ERROR);
        }
    }

    @Override
    public void deletePatient(Patient patient) throws ServerException {
        if (typeDatabase.equals("ramDatabase")) {
            database.deletePatient(patient);
        } else if (typeDatabase.equals("mySql")) {
            LOGGER.info("DAO delete Patient {}", patient);
            try (SqlSession sqlSession = getSession()) {
                try {
                    getPatientMapper(sqlSession).delete(patient);
                } catch (RuntimeException e) {
                    LOGGER.info("Can't delete Patient {}, {}", patient, e);
                    sqlSession.rollback();
                    throw e;
                }
                sqlSession.commit();
            }
        } else {
            throw new ServerException(ErrorCode.UNKNOWN_DATABASE_TYPE_ERROR);
        }
    }

    @Override
    public void delete(Doctor doctor) throws ServerException {
        if (typeDatabase.equals("ramDatabase")) {
            database.deleteDoctor(doctor);
        } else if (typeDatabase.equals("mySql")) {
            LOGGER.info("DAO delete Doctor {}", doctor);
            try (SqlSession sqlSession = getSession()) {
                try {
                    List<Doctor> doctorListFromDB = getDoctorMapper(sqlSession).getBySpecialityForChange(doctor);
                    List<Patient> patientListFromDB = getPatientListByDoctor(doctor);
                    if (doctorListFromDB != null && doctorListFromDB.size() != 0) {
                        int rand = (int) (Math.random() * doctorListFromDB.size());
                        if (rand == doctorListFromDB.size()) {
                            rand--;
                        }
                        for (Patient patient : patientListFromDB) {
                            getDoctorMapper(sqlSession).changeDoctor(patient, doctorListFromDB.get(rand));
                        }
                    } else {
                        doctorListFromDB = getDoctorMapper(sqlSession).getAllDoctorsForChange(doctor.getId());
                        if (doctorListFromDB != null) {
                            int rand = (int) (Math.random() * doctorListFromDB.size());
                            if (rand == doctorListFromDB.size()) {
                                rand--;
                            }
                            for (Patient patient : patientListFromDB) {
                                getDoctorMapper(sqlSession).changeDoctor(patient, doctorListFromDB.get(rand));
                            }
                        }
                    }
                    getDoctorMapper(sqlSession).delete(doctor);
                } catch (RuntimeException e) {
                    LOGGER.info("Can't delete Doctor {}, {}", doctor, e);
                    sqlSession.rollback();
                    throw e;
                }
                sqlSession.commit();
            }
        } else {
            throw new ServerException(ErrorCode.UNKNOWN_DATABASE_TYPE_ERROR);
        }
    }

    @Override
    public void deleteAll() throws ServerException {
        if (typeDatabase.equals("ramDatabase")) {
            database.toClearDataBase();
        } else if (typeDatabase.equals("mySql")) {
            LOGGER.info("DAO delete all");
            try (SqlSession sqlSession = getSession()) {
                try {
                    getDoctorMapper(sqlSession).deleteAll();
                    getDoctorMapper(sqlSession).alterTableAccount();
                    getDoctorMapper(sqlSession).alterTableAppointment();
                } catch (RuntimeException e) {
                    LOGGER.info("Can't delete all {}", e);
                    sqlSession.rollback();
                    throw e;
                }
                sqlSession.commit();
            }
        } else {
            throw new ServerException(ErrorCode.UNKNOWN_DATABASE_TYPE_ERROR);
        }
    }

    private Doctor getByLogin(String login) throws ServerException {
        if (typeDatabase.equals("ramDatabase")) {
            User user = database.getByLogin(login);
            if (!user.getClass().getName().equals(Doctor.class.getName())) {
                throw new ServerException(ErrorCode.NOT_DOCTOR_ERROR);
            }
            return (Doctor) user;
        } else if (typeDatabase.equals("mySql")) {
            LOGGER.debug("DAO get Doctor by login {}", login);
            try (SqlSession sqlSession = getSession()) {
                try {
                    return getDoctorMapper(sqlSession).getByLogin(login);
                } catch (RuntimeException e) {
                    LOGGER.info("Can't get Doctor by login {}, {}", login, e);
                    throw e;
                }
            }
        } else {
            throw new ServerException(ErrorCode.UNKNOWN_DATABASE_TYPE_ERROR);
        }
    }

    private void patientSort(List<Patient> patientList) {
        patientList.sort(Comparator.comparingInt(Patient::getId));
    }
}
