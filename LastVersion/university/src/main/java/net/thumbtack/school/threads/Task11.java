package net.thumbtack.school.threads;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class ConditionPingPong {
    private final Lock lock = new ReentrantLock();
    private final Condition pingWait = lock.newCondition();
    private final Condition pongWait = lock.newCondition();
    private boolean turn;

    public void printPing() throws InterruptedException {
        lock.lock();
        try {
            while (turn)
                pingWait.await();
            System.out.println("ping");
            turn = true;
            pongWait.signal();
        } finally {
            lock.unlock();
        }
    }

    public void printPong() throws InterruptedException {
        lock.lock();
        try {
            while (!turn)
                pongWait.await();
            System.out.println("pong");
            turn = false;
            pingWait.signal();
        } finally {
            lock.unlock();
        }
    }
}

class PingThread extends Thread {
    private final ConditionPingPong conditionPingPong;
    private int limitation = 5;

    public PingThread(ConditionPingPong conditionPingPong) {
        this.conditionPingPong = conditionPingPong;
    }

    // Тут поставлено ограничение на пинг, чтобы нормально коммитилось
    @Override
    public void run() {
        while (limitation > 0) {
            try {
                conditionPingPong.printPing();
                limitation--;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

// Для бесконечного пинг
/*    @Override
    public void run() {
        while (true) {
            try {
                conditionPingPong.printPing();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }*/
}

class PongThread extends Thread {
    private final ConditionPingPong conditionPingPong;
    private int limitation = 5;

    public PongThread(ConditionPingPong conditionPingPong) {
        this.conditionPingPong = conditionPingPong;
    }

    // Тут поставлено ограничение на понг, чтобы нормально коммитилось
    @Override
    public void run() {
        while (limitation > 0) {
            try {
                conditionPingPong.printPong();
                limitation--;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

// Для бесконечного понг
/*    @Override
    public void run() {
        while (true) {
            try {
                conditionPingPong.printPong();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }*/
}
