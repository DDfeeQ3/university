package net.thumbtack.school.hospital.service;

import net.thumbtack.school.hospital.dao.DatabaseDao;
import net.thumbtack.school.hospital.daoimpl.DatabaseDaoImpl;

public class DatabaseService {
    DatabaseDao databaseDao = new DatabaseDaoImpl();

    public void clearDatabase() {
        databaseDao.clearDatabase();
    }
}
