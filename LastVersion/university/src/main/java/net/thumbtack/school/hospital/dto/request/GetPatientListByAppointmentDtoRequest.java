package net.thumbtack.school.hospital.dto.request;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class GetPatientListByAppointmentDtoRequest {
    private AppointmentDtoRequest appointment;

    public GetPatientListByAppointmentDtoRequest(AppointmentDtoRequest appointment){
        this.appointment = appointment;
    }

    public AppointmentDtoRequest getAppointment() {
        return appointment;
    }
}
