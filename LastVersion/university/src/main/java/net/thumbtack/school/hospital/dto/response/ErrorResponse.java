package net.thumbtack.school.hospital.dto.response;

import lombok.NoArgsConstructor;
import net.thumbtack.school.hospital.exceptions.ErrorCode;
import net.thumbtack.school.hospital.exceptions.ServerException;

@NoArgsConstructor
public class ErrorResponse extends BaseResponse {
    private ErrorCode errorCode;

    public ErrorResponse(ServerException e) {
        this.errorCode = e.getErrorCode();
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }
}
