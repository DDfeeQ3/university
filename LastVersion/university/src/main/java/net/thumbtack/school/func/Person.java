package net.thumbtack.school.func;

import java.util.Objects;

public class Person {
    private String name;
    private Person mother;
    private Person father;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Person(String name) {
        this.name = name;
    }

    public Person(String name, Person father, Person mother) {
        this.name = name;
        this.father = father;
        this.mother = mother;
    }

    public Person getMothersMotherFather() {
        try {
            return this.getMother().getMother().getFather();
        } catch (NullPointerException e) {
            return null;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Person getMother() {
        return mother;
    }

    public void setMother(Person mother) {
        this.mother = mother;
    }

    public Person getFather() {
        return father;
    }

    public void setFather(Person father) {
        this.father = father;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        Person person = (Person) o;
        return getAge() == person.getAge() && Objects.equals(getName(), person.getName()) && Objects.equals(getMother(), person.getMother()) && Objects.equals(getFather(), person.getFather());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getMother(), getFather(), getAge());
    }
}
