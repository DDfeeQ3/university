package net.thumbtack.school.hospital.database.ConcurrentCollections;

import org.apache.commons.collections4.bidimap.DualHashBidiMap;

import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class MyDualHashBidiMap<K, V> {
    private final DualHashBidiMap<K, V> map = new DualHashBidiMap<>();
    private final ReadWriteLock lock = new ReentrantReadWriteLock();

    public void put(K key, V value) {
        lock.writeLock().lock();
        try {
            map.put(key, value);
        } finally {
            lock.writeLock().unlock();
        }
    }

    public V putIfAbsent(K key, V value) {
        lock.writeLock().lock();
        try {
            return map.putIfAbsent(key, value);
        } finally {
            lock.writeLock().unlock();
        }
    }

    public K getK(V value) {
        lock.readLock().lock();
        try {
            return map.getKey(value);
        } finally {
            lock.readLock().unlock();
        }
    }

    public V getV(K key) {
        lock.readLock().lock();
        try {
            return map.get(key);
        } finally {
            lock.readLock().unlock();
        }
    }

    public V remove(K key) {
        lock.writeLock().lock();
        try {
            return map.remove(key);
        } finally {
            lock.writeLock().unlock();
        }
    }

    public int size() {
        lock.readLock().lock();
        try {
            return map.size();
        } finally {
            lock.readLock().unlock();
        }
    }

    public Set<V> values() {
        lock.readLock().lock();
        try {
            return map.values();
        } finally {
            lock.readLock().unlock();
        }
    }

    public void clear() {
        lock.readLock().lock();
        try {
            map.clear();
        } finally {
            lock.readLock().unlock();
        }
    }

    public boolean containsByKey(K key) {
        return map.containsKey(key);
    }

    public boolean containsByValue(V value) {
        return map.containsValue(value);
    }
}
