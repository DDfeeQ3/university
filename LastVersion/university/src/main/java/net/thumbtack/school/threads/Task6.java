package net.thumbtack.school.threads;

import java.util.List;

class AddIntThreadTask6 extends Thread {

    private final List<Integer> list;

    public AddIntThreadTask6(List<Integer> list) {
        this.list = list;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            try {
                Thread.sleep(1);
                list.add((int) (Math.random() * 10000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}


class DeleteIntThreadTask6 extends Thread {

    private final List<Integer> list;

    public DeleteIntThreadTask6(List<Integer> list) {
        this.list = list;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            try {
                Thread.sleep(1);
                if (list.size() != 0) {
                    list.remove((int) (Math.random() * list.size()));
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}