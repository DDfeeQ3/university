package net.thumbtack.school.hospital.dto.request;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ChangePatientPasswordDtoRequest {
    private String login;
    private String newPassword;
    private String oldPassword;

    public ChangePatientPasswordDtoRequest(String login, String newPassword, String oldPassword) {
        setLogin(login);
        setNewPassword(newPassword);
        setOldPassword(oldPassword);
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getLogin() {
        return login;
    }

    private void setLogin(String login) {
        this.login = login;
    }

    public String getNewPassword() {
        return newPassword;
    }

    private void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
