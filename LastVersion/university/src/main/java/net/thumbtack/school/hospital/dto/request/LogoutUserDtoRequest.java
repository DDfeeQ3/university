package net.thumbtack.school.hospital.dto.request;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class LogoutUserDtoRequest {
    private String token;

    public LogoutUserDtoRequest(String token) {
        setToken(token);
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
