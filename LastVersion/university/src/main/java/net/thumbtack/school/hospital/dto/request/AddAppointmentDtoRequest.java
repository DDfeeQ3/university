package net.thumbtack.school.hospital.dto.request;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class AddAppointmentDtoRequest {
    private int patientID;
    private String methodName;
    private int frequency;
    private MethodTypeRequest methodTypeRequest;

    public AddAppointmentDtoRequest(MethodTypeRequest methodTypeRequest, String methodName, int frequency, int patientID) {
        setFrequency(frequency);
        setMethodName(methodName);
        setMethodType(methodTypeRequest);
        setPatientID(patientID);
    }

    public int getPatientID() {
        return patientID;
    }

    public void setPatientID(int patientID) {
        this.patientID = patientID;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public MethodTypeRequest getMethodType() {
        return methodTypeRequest;
    }

    public void setMethodType(MethodTypeRequest methodTypeRequest) {
        this.methodTypeRequest = methodTypeRequest;
    }
}
