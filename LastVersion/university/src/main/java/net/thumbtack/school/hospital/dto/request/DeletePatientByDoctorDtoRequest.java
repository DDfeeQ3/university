package net.thumbtack.school.hospital.dto.request;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class DeletePatientByDoctorDtoRequest {
    private String token;
    private int patientID;

    public DeletePatientByDoctorDtoRequest(String token, int patientID) {
        setPatientID(patientID);
        setToken(token);
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getPatientID() {
        return patientID;
    }

    public void setPatientID(int patientID) {
        this.patientID = patientID;
    }
}
