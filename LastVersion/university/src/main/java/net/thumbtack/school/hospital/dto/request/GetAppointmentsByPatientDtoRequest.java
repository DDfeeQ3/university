package net.thumbtack.school.hospital.dto.request;

import lombok.NoArgsConstructor;

import java.util.Objects;

@NoArgsConstructor
public class GetAppointmentsByPatientDtoRequest {
    private int patientID;

    public GetAppointmentsByPatientDtoRequest(int patientID) {
        setPatientID(patientID);
    }

    public int getPatientID() {
        return patientID;
    }

    public void setPatientID(int patientID) {
        this.patientID = patientID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GetAppointmentsByPatientDtoRequest that = (GetAppointmentsByPatientDtoRequest) o;
        return patientID == that.patientID;
    }

    @Override
    public int hashCode() {
        return Objects.hash(patientID);
    }
}
