package net.thumbtack.school.hospital.dao;

import net.thumbtack.school.hospital.exceptions.ServerException;
import net.thumbtack.school.hospital.model.Appointment;
import net.thumbtack.school.hospital.model.Doctor;
import net.thumbtack.school.hospital.model.Patient;
import net.thumbtack.school.hospital.model.User;

import java.util.List;


public interface PatientDao {

    String insert(Doctor doctor, Patient patient) throws ServerException;

    String login(String login, String password) throws ServerException;

    void logout(String token) throws ServerException;

    Patient getByLoginAndPassword(String login, String password) throws ServerException;

    Patient getByToken(String token) throws ServerException;

    Patient getPatientByID(int patientID) throws ServerException;

    void changePatientPassword(Patient patient, String newPassword) throws ServerException;

    List<Appointment> getAppointmentsByPatient(Patient patient) throws ServerException;

    Doctor getDoctorByPatient(Patient patient) throws ServerException;

    void delete(Patient patient) throws ServerException;

    String getPatientTokenById(int id) throws ServerException;
}
