package net.thumbtack.school.hospital.model;


import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class Doctor extends User {
    private List<Patient> patientList;
    private String speciality;

    public Doctor(String firstName, String lastName, String login, String password, String speciality) {
        super(firstName, lastName, login, password);
        setSpeciality(speciality);
        patientList = new LinkedList<>();
    }

    public Doctor(int id, String firstName, String lastName, String login, String password, String speciality) {
        this(firstName, lastName, login, password, speciality);
        setId(id);
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public void setPatient(Patient patient) {
        patientList.add(patient);
    }

    public void setPatientList(List<Patient> patientList) {
        this.patientList = patientList;
    }
    public List<Patient> getPatientList() {
        return patientList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Doctor)) return false;
        if (!super.equals(o)) return false;
        Doctor doctor = (Doctor) o;
        return Objects.equals(getPatientList(), doctor.getPatientList()) && Objects.equals(getSpeciality(), doctor.getSpeciality());
    }
}
