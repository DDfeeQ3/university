package net.thumbtack.school.threads;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadLocalRandom;

class Data {
    private final int[] arr;

    public Data(int... arr) {
        this.arr = arr;
    }

    public int[] get() {
        return arr;
    }
}

class WriterStream extends Thread {
    private final BlockingQueue<Data> queue;

    public WriterStream(BlockingQueue<Data> queue) {
        this.queue = queue;
    }

    public void run() {
        try {
            System.out.println("Producer Started");
            int n = ThreadLocalRandom.current().nextInt(10);
            for (int i = 0; i < n; i++) {
                System.out.println("Producer put : " + i);
                queue.put(new Data(i, new Random().nextInt()));
                Thread.sleep(400);
            }
            System.out.println("Producer finished");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

class ReaderStream extends Thread {
    private final BlockingQueue<Data> queue;

    public ReaderStream(BlockingQueue<Data> queue) {
        this.queue = queue;
    }

    public void run() {
        System.out.println("Consumer Started");
        while (true) {
            try {
                Data data = queue.take();
                if (data.get() == null) {
                    System.out.println("Consumer finished");
                    queue.put(new Data((int[]) null));
                    break;
                }
                System.out.println("Consumer get : " + Arrays.toString(data.get()));
                Thread.sleep(200);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        }
    }
}
