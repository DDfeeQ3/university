package net.thumbtack.school.func;

import java.util.Objects;
import java.util.Optional;

public class PersonOpt {
    private Optional<String> name;
    private Optional<PersonOpt> father;
    private Optional<PersonOpt> mother;

    public PersonOpt(String name) {
        this.name = Optional.ofNullable(name);
        father = Optional.empty();
        mother = Optional.empty();
    }

    public PersonOpt(String name, PersonOpt father, PersonOpt mother) {
        this.name = Optional.ofNullable(name);
        this.father = Optional.ofNullable(father);
        this.mother = Optional.ofNullable(mother);
    }

    public Optional<PersonOpt> getFather() {
        return father;
    }

    public void setFather(PersonOpt father) {
        this.father = Optional.ofNullable(father);
    }

    public Optional<PersonOpt> getMother() {
        return mother;
    }

    public void setMother(PersonOpt mother) {
        this.mother = Optional.ofNullable(mother);
    }

    public Optional<String> getName() {
        return name;
    }

    public void setName(String name) {
        this.name = Optional.ofNullable(name);
    }

    public Optional<PersonOpt> getMothersMotherFather(Optional<PersonOpt> person) {
        return person.flatMap(PersonOpt::getMother).flatMap(PersonOpt::getMother).flatMap(PersonOpt::getFather);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PersonOpt)) return false;
        PersonOpt personOpt = (PersonOpt) o;
        return Objects.equals(getName(), personOpt.getName()) &&
                Objects.equals(getFather(), personOpt.getFather()) &&
                Objects.equals(getMother(), personOpt.getMother());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getFather(), getMother());
    }
}
