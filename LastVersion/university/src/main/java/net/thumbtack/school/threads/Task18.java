package net.thumbtack.school.threads;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadLocalRandom;

class CreateTasks2Stream extends Thread {
    private final BlockingQueue<Executable> queue;
    private final int count;
    private final List<CreateTasks2Stream> developers = new ArrayList<>();
    private final BlockingQueue<Notice> notices;

    public CreateTasks2Stream(BlockingQueue<Executable> queue, int count, BlockingQueue<Notice> notices) {
        this.queue = queue;
        this.count = count;
        this.notices = notices;
    }

    public void run() {
        try {
            for (int i = 0; i < count; i++)
                if (ThreadLocalRandom.current().nextInt(10) > 5) {
                    System.out.println("put task " + i);
                    queue.put(new MultistageTask(Arrays.asList(
                            () -> System.out.println(1),
                            () -> System.out.println(2),
                            () -> System.out.println(3))));
                    notices.put(Notice.DEVELOPER_GENERATED_TASK);
                    System.out.println("developer generated task");
                } else {
                    int shift = (count - i + 1) / 2;
                    CreateTasks2Stream dev = new CreateTasks2Stream(queue, shift, notices);
                    developers.add(dev);
                    dev.start();
                    i += shift - 1;
                    notices.put(Notice.DEVELOPER_CREATED_DEVELOPER);
                    System.out.println("create new developer");
                }
            notices.put(Notice.DEVELOPER_FINISHED);
            System.out.println("developer finished");
            for (CreateTasks2Stream dev : developers) {
                dev.join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
