package net.thumbtack.school.hospital.model;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class Patient extends User {
    private String disease;
    private Doctor doctor;
    private List<Appointment> appointments;

    public Patient(String firstName, String lastName, String login, String password, String disease) {
        super(firstName, lastName, login, password);
        setDisease(disease);
        appointments = new LinkedList<>();
    }

    public Patient(int id, String firstName, String lastName, String login, String password, String disease) {
        this(firstName, lastName, login, password, disease);
        setId(id);
    }

    public Patient(Doctor doctor, String firstName, String lastName, String disease,
                   String login, String password) {
        this(firstName, lastName, login, password, disease);
        this.doctor = doctor;
    }

    public String getDisease() {
        return disease;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }

    public List<Appointment> getAppointments() {
        return appointments;
    }

    public void setAppointments(List<Appointment> appointments) {
        this.appointments = appointments;
    }

    public void addAppointments(List<Appointment> appointments) {
        this.appointments.addAll(appointments);
    }

    public void addAppointment(Appointment appointment) {
        appointments.add(appointment);
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Patient)) return false;
        if (!super.equals(o)) return false;
        Patient patient = (Patient) o;
        return Objects.equals(getDisease(), patient.getDisease()) && Objects.equals(getDoctor(), patient.getDoctor()) && Objects.equals(getAppointments(), patient.getAppointments());
    }
}