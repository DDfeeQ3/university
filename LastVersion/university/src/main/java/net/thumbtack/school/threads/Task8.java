package net.thumbtack.school.threads;

import java.util.concurrent.locks.ReentrantReadWriteLock;

class ReaderWriter {
    private final StringBuffer sb = new StringBuffer();
    private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

    public void write(char ch) {
        lock.writeLock().lock();
        try {
            sb.append(ch);
            System.out.println("Write: " + ch);
        } finally {
            lock.writeLock().unlock();
        }
    }

    public void read() {
        lock.readLock().lock();
        try {
            if (sb.length() != 0) {
                System.out.println(sb.charAt(sb.length() - 1));
            }
        } finally {
            lock.readLock().unlock();
        }
    }
}
