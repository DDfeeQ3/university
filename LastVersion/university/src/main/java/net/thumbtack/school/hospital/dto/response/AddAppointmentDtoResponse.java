package net.thumbtack.school.hospital.dto.response;

import lombok.NoArgsConstructor;

import java.util.Objects;

@NoArgsConstructor
public class AddAppointmentDtoResponse extends BaseResponse {
    private int idAppointment;
    private String tokenPatient;

    public AddAppointmentDtoResponse(String tokenPatient, int idAppointment) {
        this.idAppointment = idAppointment;
        this.tokenPatient = tokenPatient;
    }

    public int getIdAppointment() {
        return idAppointment;
    }

    public String getTokenPatient() {
        return tokenPatient;
    }

    public void setIdAppointment(int idAppointment) {
        this.idAppointment = idAppointment;
    }

    public void setTokenPatient(String tokenPatient) {
        this.tokenPatient = tokenPatient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AddAppointmentDtoResponse)) return false;
        AddAppointmentDtoResponse that = (AddAppointmentDtoResponse) o;
        return getIdAppointment() == that.getIdAppointment() && Objects.equals(getTokenPatient(), that.getTokenPatient());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdAppointment(), getTokenPatient());
    }
}
