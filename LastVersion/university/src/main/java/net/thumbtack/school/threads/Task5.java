package net.thumbtack.school.threads;

import java.util.List;

enum OperationType {
    ADD,
    DELETE
}

class ArrayWrapper {
    private final List<Integer> list;

    public ArrayWrapper(List<Integer> list) {
        this.list = list;
    }

    public synchronized void addInt() {
        list.add((int) (Math.random() * 10000));
    }

    public synchronized void deleteInt() {
        if (list.size() != 0) {
            list.remove((int) (Math.random() * list.size()));
        }
    }
}

class ArrayThreadTask5 extends Thread {

    private final ArrayWrapper arrayWrapper;
    private final OperationType operationType;

    public ArrayThreadTask5(ArrayWrapper arrayWrapper, OperationType operationType) {
        this.arrayWrapper = arrayWrapper;
        this.operationType = operationType;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            try {
                Thread.sleep(1);
                if (operationType.equals(OperationType.ADD)) {
                    arrayWrapper.addInt();
                } else if (operationType.equals(OperationType.DELETE)) {
                    arrayWrapper.deleteInt();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
