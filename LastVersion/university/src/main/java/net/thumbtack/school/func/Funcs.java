package net.thumbtack.school.func;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.function.*;

@FunctionalInterface
interface MyFunction<T, K> {
    public K apply(T arg);
}

public class Funcs {

    public Function<String, List<String>> functionSplit = x -> List.of(x.split(" "));

    public Function<List<?>, Integer> functionCount = List::size;

    public Function<String, Person> functionPerson = Person::new;

    public MyFunction<List<?>, Integer> myFunctionCount = List::size;

    public List<String> split(String str) {
        return functionSplit.apply(str);
    }

    public int count(List<?> strs) {
        return functionCount.apply(strs);
    }

    public int splitAndCount(String str) {
        Function<String, Integer> splitAndCount = functionSplit.andThen(functionCount);
        return splitAndCount.apply(str);
    }

    public Person create(String name) {
        return functionPerson.apply(name);
    }

    public Date getCurrentDate() {
        Supplier<Date> getCurrentDate = Date::new;
        return getCurrentDate.get();
    }

    public boolean isEven(int num) {
        Predicate<Integer> isEvenFunction = x -> x % 3 == 0;
        return isEvenFunction.test(num);
    }

    public boolean areEquals(int numA, int numB) {
        BiFunction<Integer, Integer, Boolean> areEquals = Objects::equals;
        return areEquals.apply(numA, numB);
    }

    public int myFunction(List<String> list) {
        return myFunctionCount.apply(list);
    }
}
