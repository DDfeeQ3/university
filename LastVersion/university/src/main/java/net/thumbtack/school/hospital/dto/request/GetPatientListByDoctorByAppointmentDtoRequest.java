package net.thumbtack.school.hospital.dto.request;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class GetPatientListByDoctorByAppointmentDtoRequest {
    private AppointmentDtoRequest appointment;
    private String token;

    public GetPatientListByDoctorByAppointmentDtoRequest(AppointmentDtoRequest appointment, String token){
        this.token = token;
        this.appointment = appointment;
    }

    public String getToken() {
        return token;
    }

    public AppointmentDtoRequest getAppointment() {
        return appointment;
    }
}
