package net.thumbtack.school.hospital.service;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.internal.Primitives;
import net.thumbtack.school.hospital.dao.DoctorDao;
import net.thumbtack.school.hospital.dao.PatientDao;
import net.thumbtack.school.hospital.daoimpl.DoctorDaoImpl;
import net.thumbtack.school.hospital.daoimpl.PatientDaoImpl;
import net.thumbtack.school.hospital.dto.request.*;
import net.thumbtack.school.hospital.dto.response.*;
import net.thumbtack.school.hospital.exceptions.ErrorCode;
import net.thumbtack.school.hospital.exceptions.ServerException;
import net.thumbtack.school.hospital.model.*;

import java.util.*;

public class DoctorService {
    private Gson gson = new Gson();
    private DoctorDao doctorDao = new DoctorDaoImpl();
    private PatientDao patientDao = new PatientDaoImpl();

    public <T> T getClassFromJson(String json, Class<T> classOfT) throws ServerException {
        try {
            T t = gson.fromJson(json, classOfT);
            return Primitives.wrap(classOfT).cast(t);
        } catch (JsonSyntaxException e) {
            throw new ServerException(ErrorCode.WRONG_JSON_ERROR);
        }
    }

    static <T> String getJsonFromClass(T response){
        Gson gson = new Gson();
        return gson.toJson(response);
    }

    public String registerDoctor(String json) {
        try {
            RegisterDoctorDtoRequest registerDoctorDtoRequest = getClassFromJson(json, RegisterDoctorDtoRequest.class);
            validateRegistrationDoctor(registerDoctorDtoRequest);
            Doctor doctor = new Doctor(registerDoctorDtoRequest.getFirstName(),
                    registerDoctorDtoRequest.getLastName(),
                    registerDoctorDtoRequest.getLogin(), registerDoctorDtoRequest.getPassword(), registerDoctorDtoRequest.getSpeciality());
            String token = doctorDao.insert(doctor);
            RegisterDoctorDtoResponse registerDoctorDtoResponse = new RegisterDoctorDtoResponse(token);
            return gson.toJson(registerDoctorDtoResponse);
        } catch (ServerException e) {
            return gson.toJson(new ErrorResponse(e));
        }
    }

    public String login(String json) {
        try {
            LoginUserDtoRequest loginUserDtoRequest = getClassFromJson(json, LoginUserDtoRequest.class);
            validateLogin(loginUserDtoRequest);

            Doctor doctor = doctorDao.getByLoginAndPassword(loginUserDtoRequest.getLogin(), loginUserDtoRequest.getPassword());

            return gson.toJson(new LoginUserDtoResponse(doctorDao.login(doctor)));//сделать класс
        } catch (ServerException e) {
            return gson.toJson(new ErrorResponse(e));
        }
    }

    public String logout(String json) {
        try {
            LogoutUserDtoRequest logoutUserDtoRequest = getClassFromJson(json, LogoutUserDtoRequest.class);
            checkUserByToken(logoutUserDtoRequest.getToken());
            doctorDao.logout(logoutUserDtoRequest.getToken());

            return gson.toJson(null);
        } catch (ServerException e) {
            return gson.toJson(new ErrorResponse(e));
        }
    }

    public String getDoctorByToken(String json) {
        try {
            GetDoctorByTokenDtoRequest getDoctorByTokenDtoRequest = getClassFromJson(json,
                    GetDoctorByTokenDtoRequest.class);
            Doctor doctor = doctorDao.getByToken(getDoctorByTokenDtoRequest.getToken());

            DoctorDtoResponse doctorDtoResponse = new DoctorDtoResponse(doctor.getId(), doctor.getFirstName(), doctor.getLastName(),
                    doctor.getSpeciality(), doctor.getLogin(), doctor.getPassword());
            return gson.toJson(doctorDtoResponse);
        } catch (ServerException e) {
            return gson.toJson(new ErrorResponse(e));
        }
    }

    public String getPatientByID(String json) {
        try {
            GetPatientByIDtoRequest getPatientByIDtoRequest = getClassFromJson(json,
                    GetPatientByIDtoRequest.class);
            Patient patient = patientDao.getPatientByID(getPatientByIDtoRequest.getPatientID());

            PatientDtoResponse patientDtoResponse = new PatientDtoResponse(patient.getFirstName(), patient.getLastName(), patient.getId());

            return gson.toJson(patientDtoResponse);
        } catch (ServerException e) {
            return gson.toJson(new ErrorResponse(e));
        }
    }

    public String registerPatient(String json) {
        try {
            RegisterPatientDtoRequest registerPatientDtoRequest = getClassFromJson(json, RegisterPatientDtoRequest.class);
            validateRegistrationPatient(registerPatientDtoRequest);

            Doctor doctor = doctorDao.getByLoginForPatient(registerPatientDtoRequest.getLoginDoctor());
            if (registerPatientDtoRequest.getDisease().equals("")) {
                throw new ServerException(ErrorCode.WRONG_DISEASE_FORMAT_ERROR);
            }

            String disease = registerPatientDtoRequest.getDisease();

            Patient patient = new Patient(doctor, registerPatientDtoRequest.getFirstName(),
                    registerPatientDtoRequest.getLastName(), disease,
                    registerPatientDtoRequest.getLogin(),
                    registerPatientDtoRequest.getPassword());

            String token = patientDao.insert(doctor, patient);
            RegisterPatientDtoResponse registerPatientDtoResponse = new RegisterPatientDtoResponse(token, patient.getLogin());
            return gson.toJson(registerPatientDtoResponse);
        } catch (ServerException e) {
            return gson.toJson(new ErrorResponse(e));
        }
    }

    public String getPatientListByDoctor(String json) {
        try {
            GetPatientListByDoctorDtoRequest getListPatientByDoctorDtoRequest = getClassFromJson(json, GetPatientListByDoctorDtoRequest.class);
            Doctor doctor = doctorDao.getByToken(getListPatientByDoctorDtoRequest.getToken());

            List<Patient> patientList = doctorDao.getPatientListByDoctor(doctor);

            return getJsonFromClass(new PatientListDtoResponse(getPatientListResponse(patientList)));
        } catch (ServerException e) {
            return gson.toJson(new ErrorResponse(e));
        }
    }

    public String getPatientListByDoctorByDisease(String json) {
        try {
            GetPatientListByDoctorByDiseaseDtoRequest getPatientListByDoctorByDiseaseDtoRequest = getClassFromJson(json, GetPatientListByDoctorByDiseaseDtoRequest.class);
            Doctor doctor = doctorDao.getByToken(getPatientListByDoctorByDiseaseDtoRequest.getToken());
            List<Patient> patientList = doctorDao.getPatientListByDoctorByDisease(doctor, getPatientListByDoctorByDiseaseDtoRequest.getDisease());

            return getJsonFromClass(new PatientListDtoResponse(getPatientListResponse(patientList)));
        } catch (ServerException e) {
            return gson.toJson(new ErrorResponse(e));
        }
    }

    public String getPatientListByDoctorByAppointments(String json) {
        try {
            GetPatientListByDoctorByAppointmentDtoRequest getPatientListByDoctorByAppointmentDtoRequest = getClassFromJson(json, GetPatientListByDoctorByAppointmentDtoRequest.class);
            Doctor doctor = doctorDao.getByToken(getPatientListByDoctorByAppointmentDtoRequest.getToken());

            AppointmentDtoRequest app = getPatientListByDoctorByAppointmentDtoRequest.getAppointment();
            Appointment appointment;
            if (app.getMethodType() == MethodTypeRequest.MEDICINE) {
                appointment = new Appointment(MethodType.MEDICINE, app.getTreatmentMethod(), app.getFrequency());
            } else if (app.getMethodType() == MethodTypeRequest.PROCEDURE) {
                appointment = new Appointment(MethodType.PROCEDURE, app.getTreatmentMethod(), app.getFrequency());
            } else {
                throw new ServerException(ErrorCode.WRONG_PATIENT_APPOINTMENTS_ERROR);
            }

            List<Patient> patientList = doctorDao.getPatientListByDoctorByAppointment(doctor, appointment);

            return getJsonFromClass(new PatientListDtoResponse(getPatientListResponse(patientList)));
        } catch (ServerException e) {
            return gson.toJson(new ErrorResponse(e));
        }
    }

    public String getPatientListByDisease(String json) {
        try {
            GetPatientListByDiseaseDtoRequest getPatientListByDiseaseDtoRequest = getClassFromJson(json, GetPatientListByDiseaseDtoRequest.class);
            String disease = getPatientListByDiseaseDtoRequest.getDisease();
            List<Patient> patientList = doctorDao.getPatientListByDisease(disease);

            return getJsonFromClass(new PatientListDtoResponse(getPatientListResponse(patientList)));
        } catch (ServerException e) {
            return gson.toJson(new ErrorResponse(e));
        }
    }

    public String getPatientListByAppointments(String json) {
        try {
            GetPatientListByDoctorByAppointmentDtoRequest getPatientListByAppointmentsDtoRequest = getClassFromJson(json, GetPatientListByDoctorByAppointmentDtoRequest.class);

            AppointmentDtoRequest app = getPatientListByAppointmentsDtoRequest.getAppointment();
            Appointment appointment;
            if (app.getMethodType() == MethodTypeRequest.MEDICINE) {
                appointment = new Appointment(MethodType.MEDICINE, app.getTreatmentMethod(), app.getFrequency());
            } else if (app.getMethodType() == MethodTypeRequest.PROCEDURE) {
                appointment = new Appointment(MethodType.PROCEDURE, app.getTreatmentMethod(), app.getFrequency());
            } else {
                throw new ServerException(ErrorCode.WRONG_PATIENT_APPOINTMENTS_ERROR);
            }

            List<Patient> patientList = doctorDao.getPatientListByAppointment(appointment);

            return getJsonFromClass(new PatientListDtoResponse(getPatientListResponse(patientList)));
        } catch (ServerException e) {
            return gson.toJson(new ErrorResponse(e));
        }
    }

    public String addAppointments(String json) {
        try {
            AddAppointmentDtoRequest addAppointmentsDtoRequest = getClassFromJson(json, AddAppointmentDtoRequest.class);
            Appointment appointmentFotAdd;
            if (addAppointmentsDtoRequest.getMethodType() == MethodTypeRequest.MEDICINE) {
                appointmentFotAdd = new Appointment(MethodType.MEDICINE, addAppointmentsDtoRequest.getMethodName(),
                        addAppointmentsDtoRequest.getFrequency());
            } else if (addAppointmentsDtoRequest.getMethodType() == MethodTypeRequest.PROCEDURE){
                appointmentFotAdd = new Appointment(MethodType.PROCEDURE, addAppointmentsDtoRequest.getMethodName(),
                        addAppointmentsDtoRequest.getFrequency());
            } else {
                throw new ServerException(ErrorCode.WRONG_PATIENT_APPOINTMENTS_ERROR);
            }

            doctorDao.addAppointment(addAppointmentsDtoRequest.getPatientID(), appointmentFotAdd);

            SuccessfulDtoResponse successfulDtoResponse = new SuccessfulDtoResponse();
            return gson.toJson(successfulDtoResponse);
        } catch (ServerException e) {
            return gson.toJson(new ErrorResponse(e));
        }
    }

    public String deletePatientByDoctor(String json) {
        try {
            DeletePatientByDoctorDtoRequest deletePatientByDoctorDtoRequest = getClassFromJson(json, DeletePatientByDoctorDtoRequest.class);

            Patient patient = patientDao.getPatientByID(deletePatientByDoctorDtoRequest.getPatientID());

            doctorDao.deletePatient(patient);
            if (doctorDao.getByToken(deletePatientByDoctorDtoRequest.getToken()) != null) {
                throw new ServerException(ErrorCode.DELETION_ERROR);
            }
            SuccessfulDtoResponse successfulDtoResponse = new SuccessfulDtoResponse();
            return gson.toJson(successfulDtoResponse);
        } catch (ServerException e) {
            return gson.toJson(new ErrorResponse(e));
        }
    }

    private List<PatientDtoResponse> getPatientListResponse (List<Patient> patientList) {
        List<PatientDtoResponse> patientDtoResponses = new LinkedList<>();

        for (Patient patient : patientList) {
            patientDtoResponses.add(new PatientDtoResponse(patient.getFirstName(), patient.getLastName(), patient.getId()));
        }
        return patientDtoResponses;
    }

    public String delete(String json) {
        try {
            DeleteUserDtoRequest deleteAccountUserRequest = getClassFromJson(json, DeleteUserDtoRequest.class);
            checkUserByToken(deleteAccountUserRequest.getToken());

            doctorDao.delete(doctorDao.getByToken(deleteAccountUserRequest.getToken()));

            return gson.toJson(null);
        } catch (ServerException e) {
            return gson.toJson(new ErrorResponse(e));
        }
    }

    private static void validateRegistrationDoctor(RegisterDoctorDtoRequest request) throws ServerException {
        if ((request.getFirstName().equals(""))) {
            throw new ServerException(ErrorCode.WRONG_FIRST_NAME_ERROR);
        } else if (request.getLastName().equals("")) {
            throw new ServerException(ErrorCode.WRONG_LAST_NAME_ERROR);
        } else if (request.getLogin().equals("")) {
            throw new ServerException(ErrorCode.WRONG_LOGIN_FORMAT_ERROR);
        } else if (request.getPassword().equals("") || request.getPassword().length() < 6) {
            throw new ServerException(ErrorCode.WRONG_PASSWORD_FORMAT_ERROR);
        } else if (request.getSpeciality().equals("")) {
            throw new ServerException(ErrorCode.WRONG_DOCTOR_SPECIALITY_ERROR);
        }
    }

    private static void validateRegistrationPatient(RegisterPatientDtoRequest request) throws ServerException {
        if ((request.getFirstName().equals(""))) {
            throw new ServerException(ErrorCode.WRONG_FIRST_NAME_ERROR);
        } else if (request.getLastName().equals("")) {
            throw new ServerException(ErrorCode.WRONG_LAST_NAME_ERROR);
        } else if (request.getLogin().equals("")) {
            throw new ServerException(ErrorCode.WRONG_LOGIN_FORMAT_ERROR);
        } else if (request.getPassword().equals("") || request.getPassword().length() < 6) {
            throw new ServerException(ErrorCode.WRONG_PASSWORD_FORMAT_ERROR);
        } else if (request.getDisease().equals("")) {
            throw new ServerException(ErrorCode.WRONG_DISEASE_FORMAT_ERROR);
        }
    }

    private boolean validateLogin(LoginUserDtoRequest request) throws ServerException {
        if (doctorDao.getByLoginAndPassword(request.getLogin(), request.getPassword()) == null) {
            throw new ServerException(ErrorCode.INVALID_LOGIN_OR_PASSWORD_ERROR);
        } else return true;
    }

    private void checkUserByToken(String token) throws ServerException {
        doctorDao.getByToken(token);
    }
}
