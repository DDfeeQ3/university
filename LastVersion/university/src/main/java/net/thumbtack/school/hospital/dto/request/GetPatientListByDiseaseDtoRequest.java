package net.thumbtack.school.hospital.dto.request;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class GetPatientListByDiseaseDtoRequest {
    private String disease;

    GetPatientListByDiseaseDtoRequest(String disease) {
        setDisease(disease);
    }

    public String getDisease() {
        return disease;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }
}
