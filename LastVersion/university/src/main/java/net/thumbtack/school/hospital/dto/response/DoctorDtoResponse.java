package net.thumbtack.school.hospital.dto.response;

import lombok.NoArgsConstructor;

import java.util.Objects;

@NoArgsConstructor
public class DoctorDtoResponse extends BaseResponse {

    private int id;
    private String fistName;
    private String lastName;
    private String speciality;
    private String login;
    private String password;

    public DoctorDtoResponse(String fistName, String lastName, String speciality) {
        setFistName(fistName);
        setLastName(lastName);
        setSpeciality(speciality);
    }

    public DoctorDtoResponse(int id, String fistName, String lastName, String speciality, String login, String password) {
        this(fistName, lastName, speciality);
        setId(id);
        setLogin(login);
        setPassword(password);
    }

    public String getFistName() {
        return fistName;
    }

    public void setFistName(String fistName) {
        this.fistName = fistName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DoctorDtoResponse)) return false;
        DoctorDtoResponse that = (DoctorDtoResponse) o;
        return getId() == that.getId() && Objects.equals(getFistName(), that.getFistName()) && Objects.equals(getLastName(), that.getLastName()) && Objects.equals(getSpeciality(), that.getSpeciality()) && Objects.equals(getLogin(), that.getLogin()) && Objects.equals(getPassword(), that.getPassword());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFistName(), getLastName(), getSpeciality(), getLogin(), getPassword());
    }
}
