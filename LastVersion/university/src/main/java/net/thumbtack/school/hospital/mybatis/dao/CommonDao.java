package net.thumbtack.school.hospital.mybatis.dao;

public interface CommonDao {
    // удаляет все записи из всех таблиц, иными словами, очищает базу данных
    void clear();
}
