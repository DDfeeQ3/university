package net.thumbtack.school.hospital.dto.response;

import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

@NoArgsConstructor
public class PatientListDtoResponse extends BaseResponse {
    private List<PatientDtoResponse> patientList;

    public PatientListDtoResponse(List<PatientDtoResponse> patientList) {
        setPatientList(patientList);
    }

    public List<PatientDtoResponse> getPatientList() {
        return patientList;
    }

    public void setPatientList(List<PatientDtoResponse> patientList) {
        this.patientList = patientList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatientListDtoResponse that = (PatientListDtoResponse) o;
        return patientList.equals(that.patientList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(patientList);
    }
}
