package net.thumbtack.school.hospital.mybatis.mappers;

import net.thumbtack.school.hospital.model.Appointment;
import net.thumbtack.school.hospital.model.Doctor;
import net.thumbtack.school.hospital.model.Patient;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;

public interface DoctorMapper {

    @Insert("INSERT INTO doctor (id_account, speciality) VALUES (#{doctor.id}, #{doctor.speciality})")
    Integer insert(@Param("doctor") Doctor doctor);

    @Insert("INSERT INTO account (firstName, lastName, login, password) VALUES "
            + "(#{doctor.firstName}, #{doctor.lastName}, #{doctor.login}, #{doctor.password})")
    @Options(useGeneratedKeys = true, keyProperty = "doctor.id")
    Integer createAccount(@Param("doctor") Doctor doctor);

    @Insert("INSERT INTO user_token (token, id_account) VALUES (#{token}, #{doctor.id})")
    Integer loginDoctor(@Param("doctor") Doctor doctor, @Param("token") String token);

    @Delete("DELETE FROM user_token WHERE token = #{token}")
    void logoutDoctor(@Param("token") String token);

    @Select("Select token from user_token where id_account = #{id}")
    String getTokenById(@Param("id") int id);

    @Select("Select id_account from user_token where token = #{token}")
    int getIdByToken(@Param("token") String token);

    @Select("select account.id, firstName, lastName, login, password, doctor.speciality from account join doctor on account.id = doctor.id_account " +
            "where doctor.id_account in (select id_doctor from patient where patient.id_account = #{patientId})")
    @Result(property = "patientList", column = "id", javaType = List.class,
            many = @Many(select = "net.thumbtack.school.hospital.mybatis.mappers.DoctorMapper.getPatientListByDoctorId", fetchType = FetchType.LAZY))
    Doctor getByPatientId(@Param("patientId") int patientId);

    @Select("select account.id, firstName, lastName, login, password, doctor.speciality from account " +
            "join doctor on doctor.id_account = account.id where doctor.speciality = #{doctor.speciality} order by account.id")
    @Result(property = "patientList", column = "id", javaType = List.class,
            many = @Many(select = "net.thumbtack.school.hospital.mybatis.mappers.DoctorMapper.getPatientListByDoctorId", fetchType = FetchType.LAZY))
    List<Doctor> getBySpeciality(@Param("doctor") Doctor doctor);

    @Select("select account.id, firstName, lastName, login, password, doctor.speciality from account join" +
            " doctor on doctor.id_account = account.id where doctor.id_account = #{id}")
    @Result(property = "patientList", column = "id", javaType = List.class,
            many = @Many(select = "net.thumbtack.school.hospital.mybatis.mappers.DoctorMapper.getPatientListByDoctorId", fetchType = FetchType.LAZY))
    Doctor getById(@Param("id") int id);

    @Select("select account.id, firstName, lastName, login, password, doctor.speciality from account join" +
            " doctor on doctor.id_account = account.id join user_token on token = #{token} where doctor.id_account = user_token.id_account")
    @Result(property = "patientList", column = "id", javaType = List.class,
            many = @Many(select = "net.thumbtack.school.hospital.mybatis.mappers.DoctorMapper.getPatientListByDoctorId", fetchType = FetchType.LAZY))
    Doctor getByToken(@Param("token") String token);

    @Select("select account.id, firstName, lastName, login, password, doctor.speciality from account join" +
            " doctor on doctor.id_account = account.id where login = #{login} and password = #{password}")
    @Result(property = "patientList", column = "id", javaType = List.class,
            many = @Many(select = "net.thumbtack.school.hospital.mybatis.mappers.DoctorMapper.getPatientListByDoctorId", fetchType = FetchType.LAZY))
    Doctor getByLoginAndPassword(@Param("login") String login, @Param("password") String password);

    @Select("select account.id, firstName, lastName, login, password, doctor.speciality from account join" +
            " doctor on doctor.id_account = account.id where login = #{login}")
    @Result(property = "patientList", column = "id", javaType = List.class,
            many = @Many(select = "net.thumbtack.school.hospital.mybatis.mappers.DoctorMapper.getPatientListByDoctorId", fetchType = FetchType.LAZY))
    Doctor getByLogin(@Param("login") String login);

    @Select("select account.id, firstName, lastName, login, password, doctor.speciality from account join doctor on doctor.id_account = account.id order by account.id")
    @Result(property = "patientList", column = "id", javaType = List.class,
            many = @Many(select = "net.thumbtack.school.hospital.mybatis.mappers.DoctorMapper.getPatientListByDoctorId", fetchType = FetchType.LAZY))
    List<Doctor> getAllDoctors();

    @Select("select account.id, firstName, lastName, login, password, patient.disease from account join patient on patient.id_account = account.id" +
            " where patient.id_doctor = #{id} order by account.id")
    @Results({
            @Result(property = "doctor", column = "id", javaType = Doctor.class,
                    one = @One(select = "net.thumbtack.school.hospital.mybatis.mappers.DoctorMapper.getByPatientId", fetchType = FetchType.LAZY)),
            @Result(property = "appointments", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hospital.mybatis.mappers.AppointmentMapper.getAppointmentsByPatientId", fetchType = FetchType.LAZY))})
    List<Patient> getPatientListByDoctorId(@Param("id") int id);

    @Select("select account.id, firstName, lastName, login, password, patient.disease from account join patient on patient.id_account = account.id " +
            "where patient.id_doctor = #{id} and patient.disease = #{disease} order by account.id")
    @Results({
            @Result(property = "doctor", column = "id", javaType = Doctor.class,
                    one = @One(select = "net.thumbtack.school.hospital.mybatis.mappers.DoctorMapper.getByPatientId", fetchType = FetchType.LAZY)),
            @Result(property = "appointments", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hospital.mybatis.mappers.AppointmentMapper.getAppointmentsByPatientId", fetchType = FetchType.LAZY))})
    List<Patient> getPatientListByDoctorIdAndByDisease(@Param("id") int id, @Param("disease") String disease);

    @Select("select account.id, firstName, lastName, login, password, patient.disease from account join" +
            " patient on patient.id_account = account.id inner join" +
            " appointment on appointment.id_patient = account.id where patient.id_doctor = #{id} and treatmentMethod = #{appointment.treatmentMethod}" +
            " and methodType = #{appointment.methodType} and frequency = #{appointment.frequency} order by account.id")
    @Results({
            @Result(property = "doctor", column = "id", javaType = Doctor.class,
                    one = @One(select = "net.thumbtack.school.hospital.mybatis.mappers.DoctorMapper.getByPatientId", fetchType = FetchType.LAZY)),
            @Result(property = "appointments", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hospital.mybatis.mappers.AppointmentMapper.getAppointmentsByPatientId", fetchType = FetchType.LAZY))})
    List<Patient> getPatientListByDoctorIdAndByAppointment(@Param("id") int id, @Param("appointment") Appointment appointment);

    @Select("select account.id, firstName, lastName, login, password, patient.disease from account join patient on patient.id_account = account.id where" +
            " patient.disease = #{disease} order by account.id")
    @Results({
            @Result(property = "doctor", column = "id", javaType = Doctor.class,
                    one = @One(select = "net.thumbtack.school.hospital.mybatis.mappers.DoctorMapper.getByPatientId", fetchType = FetchType.LAZY)),
            @Result(property = "appointments", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hospital.mybatis.mappers.AppointmentMapper.getAppointmentsByPatientId", fetchType = FetchType.LAZY))})
    List<Patient> getPatientListByDisease(@Param("disease") String disease);

    @Select("select account.id, firstName, lastName, login, password, patient.disease from account join" +
            " patient on patient.id_account = account.id inner join" +
            " appointment on appointment.id_patient = account.id where treatmentMethod = #{appointment.treatmentMethod}" +
            " and methodType = #{appointment.methodType} and frequency = #{appointment.frequency} order by account.id")
    @Results({
            @Result(property = "doctor", column = "id", javaType = Doctor.class,
                    one = @One(select = "net.thumbtack.school.hospital.mybatis.mappers.DoctorMapper.getByPatientId", fetchType = FetchType.LAZY)),
            @Result(property = "appointments", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hospital.mybatis.mappers.AppointmentMapper.getAppointmentsByPatientId", fetchType = FetchType.LAZY))})
    List<Patient> getPatientListByAppointment(@Param("appointment") Appointment appointment);

    @Select("select account.id, firstName, lastName, login, password, doctor.speciality from account " +
            "join doctor on doctor.id_account = account.id where doctor.speciality = #{doctor.speciality} and doctor.id_account != #{doctor.id} order by account.id")
    List<Doctor> getBySpecialityForChange(@Param("doctor") Doctor doctor);

    @Select("select account.id, firstName, lastName, login, password, doctor.speciality from account join doctor on doctor.id_account = account.id " +
            "where doctor.id_account != #{id} order by account.id")
    List<Doctor> getAllDoctorsForChange(@Param("id") int id);

    @Update("UPDATE patient SET id_doctor = #{doctor.id} WHERE id_account = #{patient.id}")
    void changeDoctor(@Param("patient") Patient patient, @Param("doctor") Doctor doctor);

    @Delete("DELETE FROM account WHERE id = #{doctor.id}")
    void delete(@Param("doctor") Doctor doctor);

    @Delete("delete from account")
    void deleteAll();

    @Update("ALTER TABLE account AUTO_INCREMENT = 1")
    void alterTableAccount();

    @Update("ALTER TABLE appointment AUTO_INCREMENT = 1")
    void alterTableAppointment();
}
