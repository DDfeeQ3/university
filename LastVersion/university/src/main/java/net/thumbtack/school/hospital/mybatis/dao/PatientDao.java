package net.thumbtack.school.hospital.mybatis.dao;

import net.thumbtack.school.hospital.model.Appointment;
import net.thumbtack.school.hospital.model.Doctor;
import net.thumbtack.school.hospital.model.Patient;

import java.util.List;

public interface PatientDao {
    Patient insert(Doctor doctor, Patient patient);

    Patient getById(int id);

    Patient changePassword(Patient patient, String password);

    List<Appointment> getAllAppointmentsByPatient(Patient patient);

    Doctor getDoctorByPatient(Patient patient);

    void delete(Patient patient);
}
