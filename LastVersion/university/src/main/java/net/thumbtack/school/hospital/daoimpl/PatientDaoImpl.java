package net.thumbtack.school.hospital.daoimpl;

import net.thumbtack.school.hospital.dao.PatientDao;
import net.thumbtack.school.hospital.database.Database;
import net.thumbtack.school.hospital.exceptions.ErrorCode;
import net.thumbtack.school.hospital.exceptions.ServerException;
import net.thumbtack.school.hospital.model.*;
import net.thumbtack.school.hospital.mybatis.daoimpl.DaoImplBase;
import net.thumbtack.school.hospital.utils.Settings;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Comparator;
import java.util.List;
import java.util.UUID;

public class PatientDaoImpl extends DaoImplBase implements PatientDao {
    private Database database = Database.getInstance();
    private static final String typeDatabase = new Settings().getTypeDatabase();
    private static final Logger LOGGER = LoggerFactory.getLogger(PatientDaoImpl.class);

    @Override
    public String insert(Doctor doctor, Patient patient) throws ServerException {
        if (typeDatabase.equals("ramDatabase")) {
            return database.registerPatient(patient, doctor);
        } else if (typeDatabase.equals("mySql")) {
            LOGGER.debug("DAO insert Patient {}", patient);
            String token = (UUID.randomUUID()).toString();
            try (SqlSession sqlSession = getSession()) {
                try {
                    getPatientMapper(sqlSession).createAccount(patient);
                    getPatientMapper(sqlSession).insert(patient, doctor.getId());
                    getDoctorMapper(sqlSession).changeDoctor(patient, doctor);
                    getPatientMapper(sqlSession).loginPatient(patient, token);
                    for (Appointment appointment : patient.getAppointments()) {
                        getAppointmentMapper(sqlSession).insert(patient.getId(), appointment);
                    }
                    for (Appointment appointment : patient.getAppointments()) {
                        appointment.setPatientId(patient.getId());
                    }
                } catch (RuntimeException e) {
                    LOGGER.info("Can't insert Patient {}, {}", patient, e);
                    sqlSession.rollback();
                    throw e;
                }
                sqlSession.commit();
            }
            return token;
        } else {
            throw new ServerException(ErrorCode.UNKNOWN_DATABASE_TYPE_ERROR);
        }
    }

    @Override
    public String login(String login, String password) throws ServerException {
        if (typeDatabase.equals("ramDatabase")) {
            User patient = database.getByLogin(login);
            return database.loginUser(patient);
        } else if (typeDatabase.equals("mySql")) {
            LOGGER.debug("DAO login Patient {}, {}", login, password);
            String token = (UUID.randomUUID()).toString();
            try (SqlSession sqlSession = getSession()) {
                try {
                    Patient patient = getPatientMapper(sqlSession).getByLoginAdnPassword(login, password);
                    if (getPatientMapper(sqlSession).getTokenById(patient.getId()) != null) {
                        throw new ServerException(ErrorCode.USER_ALREADY_AUTHORIZED_ERROR);
                    }
                    getPatientMapper(sqlSession).loginPatient(patient, token);
                    sqlSession.commit();
                    return token;
                } catch (RuntimeException e) {
                    LOGGER.info("Can't login Patient {}, {}, {}", login, password, e);
                    sqlSession.rollback();
                    throw e;
                }
            }
        } else {
            throw new ServerException(ErrorCode.UNKNOWN_DATABASE_TYPE_ERROR);
        }
    }

    @Override
    public void logout(String token) throws ServerException {
        if (typeDatabase.equals("ramDatabase")) {
            database.logoutUser(token);
        } else if (typeDatabase.equals("mySql")) {
            LOGGER.debug("DAO logout Patient {}", token);
            try (SqlSession sqlSession = getSession()) {
                try {
                    if (getByToken(token) == null) {
                        throw new ServerException(ErrorCode.NON_LOGGED_USER_ERROR);
                    }
                    getPatientMapper(sqlSession).logoutPatient(token);
                } catch (RuntimeException e) {
                    LOGGER.info("Can't logout Patient {}, {}", token, e);
                    sqlSession.rollback();
                    throw e;
                }
                sqlSession.commit();
            }
        } else {
            throw new ServerException(ErrorCode.UNKNOWN_DATABASE_TYPE_ERROR);
        }
    }

    @Override
    public Patient getByLoginAndPassword(String login, String password) throws ServerException {
        if (typeDatabase.equals("ramDatabase")) {
            User user = database.getByLoginAndPassword(login, password);
            if (!user.getClass().getName().equals(Patient.class.getName())) {
                throw new ServerException(ErrorCode.NOT_DOCTOR_ERROR);
            }
            return (Patient) user;
        } else if (typeDatabase.equals("mySql")) {
            LOGGER.debug("DAO get Patient by login and password {}, {}", login, password);
            try (SqlSession sqlSession = getSession()) {
                try {
                    return getPatientMapper(sqlSession).getByLoginAdnPassword(login, password);
                } catch (RuntimeException e) {
                    LOGGER.info("Can't get Patient by login and password {}, {}, {}", login, password, e);
                    throw e;
                }
            }
        } else {
            throw new ServerException(ErrorCode.UNKNOWN_DATABASE_TYPE_ERROR);
        }
    }

    @Override
    public Patient getByToken(String token) throws ServerException {
        if (typeDatabase.equals("ramDatabase")) {
            User user = database.getUserByToken(token);
            if (!user.getClass().equals(Patient.class)) {
                throw new ServerException(ErrorCode.NOT_PATIENT_ERROR);
            }
            return (Patient) user;
        } else if (typeDatabase.equals("mySql")) {
            LOGGER.debug("DAO get Doctor by token {}", token);
            try (SqlSession sqlSession = getSession()) {
                try {
                    return getPatientMapper(sqlSession).getByToken(token);
                } catch (RuntimeException e) {
                    LOGGER.info("Can't get Doctor by token {}, {}", token, e);
                    throw e;
                }
            }
        } else {
            throw new ServerException(ErrorCode.UNKNOWN_DATABASE_TYPE_ERROR);
        }
    }

    @Override
    public void changePatientPassword(Patient patient, String newPassword) throws ServerException {
        if (typeDatabase.equals("ramDatabase")) {
            database.changePatientPassword(patient, newPassword);
        } else if (typeDatabase.equals("mySql")) {
            LOGGER.info("DAO change password Patient {}, {}", patient, newPassword);
            try (SqlSession sqlSession = getSession()) {
                try {
                    getPatientMapper(sqlSession).changePassword(patient, newPassword);
                    patient.setPassword(newPassword);
                } catch (RuntimeException e) {
                    LOGGER.info("Can't update Patient {}, {}, {}", patient, newPassword, e);
                    sqlSession.rollback();
                    throw e;
                }
                sqlSession.commit();
            }
        } else {
            throw new ServerException(ErrorCode.UNKNOWN_DATABASE_TYPE_ERROR);
        }
    }

    @Override
    public List<Appointment> getAppointmentsByPatient(Patient patient) throws ServerException {
        if (typeDatabase.equals("ramDatabase")) {
            return database.getAppointmentsByPatient(patient);
        } else if (typeDatabase.equals("mySql")) {
            LOGGER.info("DAO get all Appointment by Patient {}", patient);
            try (SqlSession sqlSession = getSession()) {
                try {
                    List<Appointment> appointmentList = getAppointmentMapper(sqlSession).getAppointmentsByPatientId(patient.getId());
                    appointmentList.sort(Comparator.comparingInt(Appointment::getId));
                    return appointmentList;
                } catch (RuntimeException e) {
                    LOGGER.info("Can't get all Appointment by Patient {}, {}", patient, e);
                    throw e;
                }
            }
        } else {
            throw new ServerException(ErrorCode.UNKNOWN_DATABASE_TYPE_ERROR);
        }
    }

    @Override
    public Doctor getDoctorByPatient(Patient patient) throws ServerException {
        if (typeDatabase.equals("ramDatabase")) {
            return database.getDoctorByPatient(patient);
        } else if (typeDatabase.equals("mySql")) {
            LOGGER.info("DAO get Doctor by Patient {}", patient);
            try (SqlSession sqlSession = getSession()) {
                try {
                    return getDoctorMapper(sqlSession).getByPatientId(patient.getId());
                } catch (RuntimeException e) {
                    LOGGER.info("Can't get Doctor by Patient Id {}", patient, e);
                    throw e;
                }
            }
        } else {
            throw new ServerException(ErrorCode.UNKNOWN_DATABASE_TYPE_ERROR);
        }
    }

    @Override
    public void delete(Patient patient) throws ServerException {
        if (typeDatabase.equals("ramDatabase")) {
            database.deletePatient(patient);
        } else if (typeDatabase.equals("mySql")) {
            LOGGER.info("DAO delete Patient {}", patient);
            try (SqlSession sqlSession = getSession()) {
                try {
                    getPatientMapper(sqlSession).delete(patient);
                } catch (RuntimeException e) {
                    LOGGER.info("Can't delete Patient {}, {}", patient, e);
                    sqlSession.rollback();
                    throw e;
                }
                sqlSession.commit();
            }
        } else {
            throw new ServerException(ErrorCode.UNKNOWN_DATABASE_TYPE_ERROR);
        }
    }

    @Override
    public Patient getPatientByID(int patientID) throws ServerException {
        if (typeDatabase.equals("ramDatabase")) {
            if (!database.getUserByID(patientID).getClass().equals(Patient.class)) {
                throw new ServerException(ErrorCode.NOT_PATIENT_ERROR);
            }
            return (Patient) database.getUserByID(patientID);
        } else if (typeDatabase.equals("mySql")) {
            LOGGER.info("DAO get Patient by ID {}", patientID);
            try (SqlSession sqlSession = getSession()) {
                try {
                    return getPatientMapper(sqlSession).getById(patientID);
                } catch (RuntimeException e) {
                    LOGGER.info("Can't get Patient by ID {}, {}", patientID, e);
                    throw e;
                }
            }
        } else {
            throw new ServerException(ErrorCode.UNKNOWN_DATABASE_TYPE_ERROR);
        }
    }

    public String getPatientTokenById(int patientID) throws ServerException {
        if (typeDatabase.equals("ramDatabase")) {
            if (!database.getUserByID(patientID).getClass().equals(Patient.class)) {
                throw new ServerException(ErrorCode.NOT_PATIENT_ERROR);
            }
            return database.getTokenById(patientID);
        } else if (typeDatabase.equals("mySql")) {
            LOGGER.info("DAO get Patient token by ID {}", patientID);
            try (SqlSession sqlSession = getSession()) {
                try {
                    return getPatientMapper(sqlSession).getTokenById(patientID);
                } catch (RuntimeException e) {
                    LOGGER.info("Can't get Patient token by ID {}, {}", patientID, e);
                    throw e;
                }
            }
        } else {
            throw new ServerException(ErrorCode.UNKNOWN_DATABASE_TYPE_ERROR);
        }
    }
}
