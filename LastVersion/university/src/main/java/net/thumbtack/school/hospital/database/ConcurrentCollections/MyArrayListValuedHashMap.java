package net.thumbtack.school.hospital.database.ConcurrentCollections;

import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class MyArrayListValuedHashMap<K, V> {
    private final ArrayListValuedHashMap<K, V> map = new ArrayListValuedHashMap<>();
    private final ReadWriteLock lock = new ReentrantReadWriteLock();

    public void put(K key, V value) {
        lock.writeLock().lock();
        try {
            map.put(key, value);
        } finally {
            lock.writeLock().unlock();
        }
    }

    public List<V> get(K key) {
        lock.readLock().lock();
        try {
            return map.get(key);
        } finally {
            lock.readLock().unlock();
        }
    }

    public List<V> remove(K key) {
        lock.writeLock().lock();
        try {
            return map.remove(key);
        } finally {
            lock.writeLock().unlock();
        }
    }

    public int size() {
        lock.readLock().lock();
        try {
            return map.size();
        } finally {
            lock.readLock().unlock();
        }
    }

    public Collection<V> values() {
        lock.readLock().lock();
        try {
            return map.values();
        } finally {
            lock.readLock().unlock();
        }
    }

    public void clear() {
        lock.readLock().lock();
        try {
            map.clear();
        } finally {
            lock.readLock().unlock();
        }
    }

    public void removeMapping(Object key, Object value) {
        lock.readLock().lock();
        try {
            map.removeMapping(key, value);
        } finally {
            lock.readLock().unlock();
        }
    }

    public boolean containsByKey(K key) {
        return map.containsKey(key);
    }

    public boolean containsByValue(V value) {
        return map.containsValue(value);
    }
}
