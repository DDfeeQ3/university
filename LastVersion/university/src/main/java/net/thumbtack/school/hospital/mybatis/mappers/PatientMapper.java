package net.thumbtack.school.hospital.mybatis.mappers;

import net.thumbtack.school.hospital.model.Doctor;
import net.thumbtack.school.hospital.model.Patient;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;


public interface PatientMapper {

    @Insert("INSERT INTO patient (id_account, disease, id_doctor) VALUES (#{patient.id}, #{patient.disease}, #{doctorId})")
    Integer insert(@Param("patient") Patient patient, @Param("doctorId") int doctorId);

    @Insert("INSERT INTO account (firstName, lastName, login, password) VALUES "
            + "(#{patient.firstName}, #{patient.lastName}, #{patient.login}, #{patient.password})")
    @Options(useGeneratedKeys = true, keyProperty = "patient.id")
    Integer createAccount(@Param("patient") Patient patient);

    @Insert("INSERT INTO user_token (token, id_account) VALUES (#{token}, #{patient.id})")
    Integer loginPatient(@Param("patient") Patient patient, @Param("token") String token);

    @Delete("DELETE FROM user_token WHERE token = #{token}")
    void logoutPatient(@Param("token") String token);

    @Select("Select token from user_token where id_account = #{id}")
    String getTokenById(@Param("id") int id);

    @Select("select account.id, firstName, lastName, login, password, patient.disease from account join" +
            " patient on patient.id_account = account.id join user_token on token = #{token} where patient.id_account = user_token.id_account")
    @Results({
            @Result(property = "doctor", column = "id", javaType = Doctor.class,
                    one = @One(select = "net.thumbtack.school.hospital.mybatis.mappers.DoctorMapper.getByPatientId", fetchType = FetchType.LAZY)),
            @Result(property = "appointments", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hospital.mybatis.mappers.AppointmentMapper.getAppointmentsByPatientId", fetchType = FetchType.LAZY))})
    Patient getByToken(@Param("token") String token);

    @Select("select account.id, firstName, lastName, login, password, patient.disease from account join" +
            " patient on patient.id_account = account.id where login = #{login} and password = #{password}")
    @Results({
            @Result(property = "doctor", column = "id", javaType = Doctor.class,
                    one = @One(select = "net.thumbtack.school.hospital.mybatis.mappers.DoctorMapper.getByPatientId", fetchType = FetchType.LAZY)),
            @Result(property = "appointments", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hospital.mybatis.mappers.AppointmentMapper.getAppointmentsByPatientId", fetchType = FetchType.LAZY))})
    Patient getByLoginAdnPassword(@Param("login") String login, @Param("password") String password);

    @Select("select account.id, firstName, lastName, login, password, patient.disease from account join patient on patient.id_account = account.id where patient.id_account = #{id}")
    @Results({
            @Result(property = "doctor", column = "id", javaType = Doctor.class,
                    one = @One(select = "net.thumbtack.school.hospital.mybatis.mappers.DoctorMapper.getByPatientId", fetchType = FetchType.LAZY)),
            @Result(property = "appointments", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.hospital.mybatis.mappers.AppointmentMapper.getAppointmentsByPatientId", fetchType = FetchType.LAZY))})
    Patient getById(@Param("id") int id);

    @Update("UPDATE account SET password = #{newPassword} WHERE id = #{patient.id}")
    void changePassword(@Param("patient") Patient patient, @Param("newPassword") String password);

    @Select("Select password from account WHERE login = #{login}")
    String checkPassword(@Param("login") String login);

    @Delete("DELETE FROM account WHERE id = #{patient.id}")
    void delete(@Param("patient") Patient patient);
}
