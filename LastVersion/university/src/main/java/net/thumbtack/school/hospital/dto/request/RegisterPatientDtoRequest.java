package net.thumbtack.school.hospital.dto.request;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class RegisterPatientDtoRequest {
    private String firstName;
    private String lastName;
    private String disease;
    private String login;
    private String password;
    private String loginDoctor;//токен доктора

    public RegisterPatientDtoRequest(String firstName, String lastName, String disease,
                                     String login, String password, String loginDoctor) {
        setFirstName(firstName);
        setLastName(lastName);
        setLogin(login);
        setPassword(password);
        setLoginDoctor(loginDoctor);
        setDisease(disease);
    }

    public String getFirstName() {
        return firstName;
    }

    private void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    private void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDisease() {
        return disease;
    }

    private void setDisease(String disease) {
        this.disease = disease;
    }

    public String getLogin() {
        return login;
    }

    private void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    private void setPassword(String password) {
        this.password = password;
    }

    private void setLoginDoctor(String loginDoctor) {
        this.loginDoctor = loginDoctor;
    }

    public String getLoginDoctor() {
        return loginDoctor;
    }
}
