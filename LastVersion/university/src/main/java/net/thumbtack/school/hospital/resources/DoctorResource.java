package net.thumbtack.school.hospital.resources;

import net.thumbtack.school.hospital.dto.request.*;
import net.thumbtack.school.hospital.dto.response.BaseResponse;
import net.thumbtack.school.hospital.serviceTask18.DoctorService;

import javax.validation.Valid;
import javax.ws.rs.*;

@Path("/api")
public class DoctorResource {

    private DoctorService doctorService = new DoctorService();

    @POST
    @Path("/registerDoctor")
    @Consumes("application/json")
    @Produces("application/json")
    public BaseResponse registerDoctor(@Valid RegisterDoctorDtoRequest registerDoctorDtoRequest) {
        return doctorService.registerDoctor(registerDoctorDtoRequest);
    }

    @POST
    @Path("/registerPatient")
    @Consumes("application/json")
    @Produces("application/json")
    public BaseResponse registerPatient(@Valid RegisterPatientDtoRequest registerPatientDtoRequest) {
        return doctorService.registerPatient(registerPatientDtoRequest);
    }

    @DELETE
    @Path("/deleteDoctor")
    @Consumes("application/json")
    @Produces("application/json")
    public BaseResponse deleteDoctor(@Valid DeleteUserDtoRequest deleteUserDtoRequest) {
        return doctorService.delete(deleteUserDtoRequest);
    }

    @DELETE
    @Path("/deletePatientByDoctor")
    @Consumes("application/json")
    @Produces("application/json")
    public BaseResponse deletePatient(@Valid DeletePatientByDoctorDtoRequest deletePatientByDoctorDtoRequest) {
        return doctorService.deletePatientByDoctor(deletePatientByDoctorDtoRequest);
    }

    @POST
    @Path("/loginDoctor")
    @Consumes("application/json")
    @Produces("application/json")
    public BaseResponse loginDoctor(@Valid LoginUserDtoRequest loginUserDtoRequest) {
        return doctorService.login(loginUserDtoRequest);
    }

    @POST
    @Path("/logoutDoctor")
    @Consumes("application/json")
    @Produces("application/json")
    public BaseResponse logoutDoctor(@Valid LogoutUserDtoRequest logoutUserDtoRequest) {
        return doctorService.logout(logoutUserDtoRequest);
    }

    @POST
    @Path("/addAppointment")
    @Consumes("application/json")
    @Produces("application/json")
    public BaseResponse addAppointment(@Valid AddAppointmentDtoRequest addAppointmentDtoRequest) {
        return doctorService.addAppointment(addAppointmentDtoRequest);
    }

    @GET
    @Path("/getDoctorByToken")
    @Consumes("application/json")
    @Produces("application/json")
    public BaseResponse getDoctorByToken(@Valid GetDoctorByTokenDtoRequest getDoctorByTokenDtoRequest) {
        return doctorService.getDoctorByToken(getDoctorByTokenDtoRequest);
    }

    @GET
    @Path("/getPatientByID")
    @Consumes("application/json")
    @Produces("application/json")
    public BaseResponse getPatientByID(@Valid GetPatientByIDtoRequest getPatientByIDtoRequest) {
        return doctorService.getPatientByID(getPatientByIDtoRequest);
    }

    @GET
    @Path("/getPatientListByDoctor")
    @Consumes("application/json")
    @Produces("application/json")
    public BaseResponse getPatientListByDoctor(@Valid GetPatientListByDoctorDtoRequest getPatientListByDoctorDtoRequest) {
        return doctorService.getPatientListByDoctor(getPatientListByDoctorDtoRequest);
    }

    @GET
    @Path("/getPatientListByDisease")
    @Consumes("application/json")
    @Produces("application/json")
    public BaseResponse getPatientListByDisease(@Valid GetPatientListByDiseaseDtoRequest getPatientListByDiseaseDtoRequest) {
        return doctorService.getPatientListByDisease(getPatientListByDiseaseDtoRequest);
    }

    @GET
    @Path("/getPatientListByAppointments")
    @Consumes("application/json")
    @Produces("application/json")
    public BaseResponse getPatientListByAppointments(@Valid GetPatientListByAppointmentDtoRequest getPatientListByAppointmentDtoRequest) {
        return doctorService.getPatientListByAppointment(getPatientListByAppointmentDtoRequest);
    }

    @GET
    @Path("/getPatientListByDoctorByDisease")
    @Consumes("application/json")
    @Produces("application/json")
    public BaseResponse getPatientListByDoctorByDisease(@Valid GetPatientListByDoctorByDiseaseDtoRequest getPatientListByDoctorByDiseaseDtoRequest) {
        return doctorService.getPatientListByDoctorByDisease(getPatientListByDoctorByDiseaseDtoRequest);
    }

    @GET
    @Path("/getPatientListByDoctorByAppointments")
    @Consumes("application/json")
    @Produces("application/json")
    public BaseResponse getPatientListByDoctorByAppointments(@Valid GetPatientListByDoctorByAppointmentDtoRequest getPatientListByDoctorByAppointmentDtoRequest) {
        return doctorService.getPatientListByDoctorByAppointments(getPatientListByDoctorByAppointmentDtoRequest);
    }
}
