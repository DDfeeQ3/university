package net.thumbtack.school.hospital.dto.request;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public enum MethodTypeRequest {
    PROCEDURE,
    MEDICINE
}
