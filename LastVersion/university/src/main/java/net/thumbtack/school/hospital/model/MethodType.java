package net.thumbtack.school.hospital.model;

public enum MethodType {
    PROCEDURE,
    MEDICINE
}
