package net.thumbtack.school.hospital.dto.request;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class AppointmentDtoRequest {
    private String treatmentMethod;
    private int frequency;
    private MethodTypeRequest methodType;

    public AppointmentDtoRequest(MethodTypeRequest methodType , String treatmentMethod, int frequency) {
        this.treatmentMethod = treatmentMethod;
        this.frequency = frequency;
        this.methodType = methodType;
    }

    public MethodTypeRequest getMethodType() {
        return methodType;
    }

    public String getTreatmentMethod() {
        return treatmentMethod;
    }

    public int getFrequency() {
        return frequency;
    }
}
