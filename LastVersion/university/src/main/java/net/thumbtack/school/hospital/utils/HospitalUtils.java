package net.thumbtack.school.hospital.utils;


import com.google.gson.Gson;
import net.thumbtack.school.hospital.exceptions.ServerException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


public class HospitalUtils {
    private static final Gson GSON = new Gson();

    public static Response failureResponse(Response.Status status, ServerException serverException) {
        return Response.status(status).entity(GSON.toJson(new ServerException(serverException.getErrorCode(), serverException.getMessage())))
                .type(MediaType.APPLICATION_JSON_TYPE).build();
    }
}
