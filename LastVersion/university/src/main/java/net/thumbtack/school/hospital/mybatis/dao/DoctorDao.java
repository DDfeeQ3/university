package net.thumbtack.school.hospital.mybatis.dao;

import net.thumbtack.school.hospital.model.*;

import java.util.List;

public interface DoctorDao {
    Doctor insert(Doctor doctor);

    Doctor getById(int id);

    List<Doctor> getAllDoctors();

    List<Patient> getPatientListByDoctor(Doctor doctor);

    List<Patient> getPatientListByDoctorByDisease(Doctor doctor, String disease);

    List<Patient> getPatientListByDoctorByAppointment(Doctor doctor, Appointment appointment);

    List<Patient> getPatientListByDisease(String disease);

    List<Patient> getPatientListByAppointment(Appointment appointment);

    void addAppointment(int patientID, Appointment appointment);

    void deletePatient(Patient patient);

    void delete(Doctor doctor);

    void deleteAll();
}
