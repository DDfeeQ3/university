package net.thumbtack.school.hospital.mybatis.mappers;

import net.thumbtack.school.hospital.model.Appointment;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface AppointmentMapper {
    @Insert("INSERT INTO appointment (treatmentMethod, methodType, frequency, id_patient) VALUES (#{appointment.treatmentMethod}, " +
            "#{appointment.methodType}, #{appointment.frequency}, #{patientId})")
    @Options(useGeneratedKeys = true, keyProperty = "appointment.id")
    Integer insert(@Param("patientId") int patientId, @Param("appointment") Appointment appointment);

    @Select("select id, treatmentMethod, methodType, frequency, id_patient as patientId from appointment where id_patient = #{patientId} order by id")
    List<Appointment> getAppointmentsByPatientId(@Param("patientId")int patientId );

    @Delete("DELETE FROM appointment WHERE id = #{appointment.id}")
    void delete(@Param("appointment") Appointment appointment);
}
