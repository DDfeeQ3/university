package net.thumbtack.school.threads;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadLocalRandom;

interface Executable {
    Executable execut = () -> {
    };

    void execute();
}

class Task implements Executable {
    @Override
    public void execute() {
        System.out.println("Done");
    }
}

class TaskStream extends Thread {
    private final BlockingQueue<Executable> queue;

    public TaskStream(BlockingQueue<Executable> queue) {
        this.queue = queue;
    }

    public void run() {
        try {
            System.out.println("Start task putting");
            int n = ThreadLocalRandom.current().nextInt(10);
            for (int i = 0; i < n; i++) {
                queue.put(new Task());
                System.out.println("put new task" + i);
                Thread.sleep(200);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

class ExecutorStream extends Thread {
    private final BlockingQueue<Executable> queue;

    public ExecutorStream(BlockingQueue<Executable> queue) {
        this.queue = queue;
    }

    public void run() {
        System.out.println("Executor Started");
        while (true) {
            try {
                Executable curTask = queue.take();
                if (curTask == Executable.execut) {
                    System.out.println("Executor finished");
                    queue.put(Executable.execut);
                    break;
                }
                System.out.println("Executor execute " + curTask.toString());
                Thread.sleep(200);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        }
    }
}
