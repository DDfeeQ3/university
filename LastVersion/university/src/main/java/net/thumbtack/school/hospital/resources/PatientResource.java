package net.thumbtack.school.hospital.resources;

import net.thumbtack.school.hospital.dto.request.*;
import net.thumbtack.school.hospital.dto.response.BaseResponse;
import net.thumbtack.school.hospital.serviceTask18.PatientService;

import javax.validation.Valid;
import javax.ws.rs.*;;

@Path("/api")
public class PatientResource {
    private PatientService patientService = new PatientService();

    @DELETE
    @Path("/deletePatient")
    @Consumes("application/json")
    @Produces("application/json")
    public BaseResponse deletePatient(@Valid DeleteUserDtoRequest deleteUserDtoRequest) {
        return patientService.delete(deleteUserDtoRequest);
    }

    @POST
    @Path("/loginPatient")
    @Consumes("application/json")
    @Produces("application/json")
    public BaseResponse loginPatient(@Valid LoginUserDtoRequest loginUserDtoRequest) {
        return patientService.login(loginUserDtoRequest);
    }

    @POST
    @Path("/logoutPatient")
    @Consumes("application/json")
    @Produces("application/json")
    public BaseResponse logoutPatient(@Valid LogoutUserDtoRequest logoutUserDtoRequest) {
        return patientService.logout(logoutUserDtoRequest);
    }

    @POST
    @Path("/changePasswordPatient")
    @Consumes("application/json")
    @Produces("application/json")
    public BaseResponse changePasswordPatient(@Valid ChangePatientPasswordDtoRequest changePatientPasswordDtoRequest) {
        return patientService.changePasswordPatient(changePatientPasswordDtoRequest);
    }

    @GET
    @Path("/getPatientByToken")
    @Consumes("application/json")
    @Produces("application/json")
    public BaseResponse getPatientByID(@Valid GetPatientByTokenDtoRequest getPatientByTokenDtoRequest) {
        return patientService.getPatientByToken(getPatientByTokenDtoRequest);
    }

    @GET
    @Path("/getDoctorByPatient")
    @Consumes("application/json")
    @Produces("application/json")
    public BaseResponse getDoctorByPatient(@Valid GetDoctorByPatientDtoRequest getDoctorByPatientDtoRequest) {
        return patientService.getDoctorByPatient(getDoctorByPatientDtoRequest);
    }

    @GET
    @Path("/getAppointmentsByPatient")
    @Consumes("application/json")
    @Produces("application/json")
    public BaseResponse getAppointmentsByPatient(@Valid GetAppointmentsByPatientDtoRequest getAppointmentsByPatientDtoRequest) {
        return patientService.getAppointmentsByPatient(getAppointmentsByPatientDtoRequest);
    }
}
