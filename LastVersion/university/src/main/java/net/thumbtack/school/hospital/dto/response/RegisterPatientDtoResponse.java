package net.thumbtack.school.hospital.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class RegisterPatientDtoResponse extends BaseResponse {
    private String token;
    private String login;

    public RegisterPatientDtoResponse(String token, String login) {
        setToken(token);
        setLogin(login);
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
