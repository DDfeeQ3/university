package net.thumbtack.school.hospital.dto.request;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class GetInfoAboutDoctorByPatientDtoRequest {
    private String token;

    public GetInfoAboutDoctorByPatientDtoRequest(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
