package net.thumbtack.school.ModelsTask9;

import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

public class School {
    private Set<Group> groups;
    private String name;
    private int id;
    private int year;

    public School(int id, String name, int year, Set<Group> groups) {
        this.id = id;
        this.name = name;
        this.year = year;
        this.groups = groups;
    }

    public School(int id, String name, int year) {
        this(id, name, year, new CopyOnWriteArraySet<>());
    }

    public School(String name, int year) {
        this(0, name, year, new CopyOnWriteArraySet<>());
    }

    public synchronized String getName() {
        return name;
    }

    public synchronized void setName(String name) {
        this.name = name;
    }

    public synchronized int getYear() {
        return year;
    }

    public synchronized void setYear(int year) {
        this.year = year;
    }

    public synchronized int getId() {
        return id;
    }

    public synchronized void setId(int id) {
        this.id = id;
    }

    public synchronized Set<Group> getGroups() {
        return groups;
    }

    public synchronized void setGroups(Set<Group> groups) {
        this.groups = groups;
    }

    public synchronized void addGroup(Group group) {
        groups.add(group);
    }

    public synchronized void removeGroup(Group group) {
        groups.remove(group);
    }

    @Override
    public synchronized boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof School)) return false;
        School school = (School) o;
        return getYear() == school.getYear() &&
                getId() == school.getId() &&
                getName().equals(school.getName()) &&
                Objects.equals(getGroups(), school.getGroups());
    }

    @Override
    public synchronized int hashCode() {
        return Objects.hash(getName(), getYear(), getId(), getGroups());
    }
}
