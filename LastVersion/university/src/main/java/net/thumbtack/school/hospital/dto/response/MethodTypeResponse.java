package net.thumbtack.school.hospital.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public enum MethodTypeResponse {
    PROCEDURE,
    MEDICINE
}
