package net.thumbtack.school.hospital.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class RegisterDoctorDtoResponse extends BaseResponse {
    private String token;

    public RegisterDoctorDtoResponse(String token) {
        setToken(token);
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
