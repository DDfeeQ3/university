package net.thumbtack.school.hospital.daoimpl;

import net.thumbtack.school.hospital.dao.DatabaseDao;
import net.thumbtack.school.hospital.dao.DoctorDao;
import net.thumbtack.school.hospital.database.Database;
import net.thumbtack.school.hospital.exceptions.ServerException;
import net.thumbtack.school.hospital.mybatis.daoimpl.DaoImplBase;
import net.thumbtack.school.hospital.utils.Settings;

public class DatabaseDaoImpl extends DaoImplBase implements DatabaseDao {
    private final Database database = Database.getInstance();
    private static final String typeDatabase = new Settings().getTypeDatabase();
    private DoctorDao doctorDao = new DoctorDaoImpl();

    @Override
    public void clearDatabase() {
        try {
            if (typeDatabase.equals("ramDatabase")) {
                database.toClearDataBase();
            } else if (typeDatabase.equals("mySql")){
                doctorDao.deleteAll();
            }
        } catch (ServerException e) {

        }
    }
}
