package net.thumbtack.school.hospital.mybatis.daoimpl;

import net.thumbtack.school.hospital.mybatis.dao.CommonDao;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommonDaoImpl extends DaoImplBase implements CommonDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommonDaoImpl.class);

    @Override
    public void clear() throws RuntimeException {
        LOGGER.info("DAO clear DataBase");
        try (SqlSession sqlSession = getSession()) {
            try {
                getDoctorMapper(sqlSession).deleteAll();
                getDoctorMapper(sqlSession).alterTableAccount();
                getDoctorMapper(sqlSession).alterTableAppointment();
            } catch (RuntimeException e) {
                LOGGER.info("Can't clear DataBase {}", e);
                sqlSession.rollback();
                throw e;
            }
        }
    }
}
