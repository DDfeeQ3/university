package net.thumbtack.school.hospital.mybatis.daoimpl;

import net.thumbtack.school.hospital.model.Appointment;
import net.thumbtack.school.hospital.model.Doctor;
import net.thumbtack.school.hospital.model.Patient;
import net.thumbtack.school.hospital.mybatis.dao.DoctorDao;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Comparator;
import java.util.List;


public class DoctorDaoImpl extends DaoImplBase implements DoctorDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(DoctorDaoImpl.class);


    @Override
    public Doctor insert(Doctor doctor) throws RuntimeException {
        LOGGER.info("DAO insert Doctor {}", doctor);
        try (SqlSession sqlSession = getSession()) {
            try {
                getDoctorMapper(sqlSession).createAccount(doctor);
                getDoctorMapper(sqlSession).insert(doctor);
            } catch (RuntimeException e) {
                LOGGER.info("Can't insert Doctor {}, {}", doctor, e);
                sqlSession.rollback();
                throw e;
            }
            sqlSession.commit();
        }
        return doctor;
    }

    @Override
    public Doctor getById(int id) {
        LOGGER.info("DAO get Doctor by ID {}", id);
        try (SqlSession sqlSession = getSession()) {
            try {
                Doctor doctor = getDoctorMapper(sqlSession).getById(id);

                doctor.setPatientList(getDoctorMapper(sqlSession).getPatientListByDoctorId(id));
                return doctor;
            } catch (RuntimeException e) {
                LOGGER.info("Can't get Doctor by ID {}, {}", id, e);
                throw e;
            }
        }
    }

    @Override
    public List<Doctor> getAllDoctors() {
        LOGGER.info("DAO get All Doctors");
        try (SqlSession sqlSession = getSession()) {
            try {
                List<Doctor> doctorList = getDoctorMapper(sqlSession).getAllDoctors();
                doctorList.sort(Comparator.comparingInt(Doctor::getId));
                return doctorList;
            } catch (RuntimeException e) {
                LOGGER.info("Can't get Doctor by ID {}", e);
                throw e;
            }
        }
    }

    @Override
    public List<Patient> getPatientListByDoctor(Doctor doctor) {
        LOGGER.info("DAO get Patient List by doctor {}", doctor);
        try (SqlSession sqlSession = getSession()) {
            try {
                List<Patient> patientList = getDoctorMapper(sqlSession).getPatientListByDoctorId(doctor.getId());
                patientSort(patientList);
                return patientList;
            } catch (RuntimeException e) {
                LOGGER.info("Can't get Patient List by doctor {}, {}", doctor, e);
                throw e;
            }
        }
    }

    @Override
    public List<Patient> getPatientListByDoctorByDisease(Doctor doctor, String disease) {
        LOGGER.info("DAO get Patient List by doctor and by disease {}, {}", doctor, disease);
        try (SqlSession sqlSession = getSession()) {
            try {
                List<Patient> patientList = getDoctorMapper(sqlSession).getPatientListByDoctorIdAndByDisease(doctor.getId(), disease);
                patientSort(patientList);
                return patientList;
            } catch (RuntimeException e) {
                LOGGER.info("Can't get Patient List by doctor and by disease {}, {}, {}", doctor, disease, e);
                throw e;
            }
        }
    }

    @Override
    public List<Patient> getPatientListByDoctorByAppointment(Doctor doctor, Appointment appointment) {
        LOGGER.info("DAO get Patient List by doctor and by appointment {}, {}", doctor, appointment);
        try (SqlSession sqlSession = getSession()) {
            try {
                List<Patient> patientList = getDoctorMapper(sqlSession).getPatientListByDoctorIdAndByAppointment(doctor.getId(), appointment);
                patientSort(patientList);
                return patientList;
            } catch (RuntimeException e) {
                LOGGER.info("Can't get Patient List by doctor and by appointment {}, {}, {}", doctor, appointment, e);
                throw e;
            }
        }
    }

    @Override
    public List<Patient> getPatientListByDisease(String disease) {
        LOGGER.info("DAO get Patient List by disease {}", disease);
        try (SqlSession sqlSession = getSession()) {
            try {
                List<Patient> patientList = getDoctorMapper(sqlSession).getPatientListByDisease(disease);
                patientSort(patientList);
                return patientList;
            } catch (RuntimeException e) {
                LOGGER.info("Can't get Patient List by disease {}, {}", disease, e);
                throw e;
            }
        }
    }

    @Override
    public List<Patient> getPatientListByAppointment(Appointment appointment) {
        LOGGER.info("DAO get Patient List by appointment {}", appointment);
        try (SqlSession sqlSession = getSession()) {
            try {
                List<Patient> patientList = getDoctorMapper(sqlSession).getPatientListByAppointment(appointment);
                patientSort(patientList);
                return patientList;
            } catch (RuntimeException e) {
                LOGGER.info("Can't get Patient List by appointment {}, {}", appointment, e);
                throw e;
            }
        }
    }

    @Override
    public void addAppointment(int patientID, Appointment appointment) {
        LOGGER.info("DAO add  Appointment {}", appointment);
        try (SqlSession sqlSession = getSession()) {
            try {
                getAppointmentMapper(sqlSession).insert(patientID, appointment);
                appointment.setPatientId(patientID);
            } catch (RuntimeException e) {
                LOGGER.info("Can't add  Appointment {}, {}", appointment, e);
                sqlSession.rollback();
                throw e;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void deletePatient(Patient patient) {
        LOGGER.info("DAO delete Patient {}", patient);
        try (SqlSession sqlSession = getSession()) {
            try {
                getPatientMapper(sqlSession).delete(patient);
            } catch (RuntimeException e) {
                LOGGER.info("Can't delete Patient {}, {}", patient, e);
                sqlSession.rollback();
                throw e;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void delete(Doctor doctor) throws RuntimeException {
        LOGGER.info("DAO delete Doctor {}", doctor);
        try (SqlSession sqlSession = getSession()) {
            try {
                List<Doctor> doctorListFromDB = getDoctorMapper(sqlSession).getBySpecialityForChange(doctor);
                List<Patient> patientListFromDB = getPatientListByDoctor(doctor);
                if (doctorListFromDB != null && doctorListFromDB.size() != 0) {
                    int rand = (int) (Math.random() * doctorListFromDB.size());
                    if (rand == doctorListFromDB.size()) {
                        rand--;
                    }
                    for (Patient patient : patientListFromDB) {
                        getDoctorMapper(sqlSession).changeDoctor(patient, doctorListFromDB.get(rand));
                    }
                } else {
                    doctorListFromDB = getDoctorMapper(sqlSession).getAllDoctorsForChange(doctor.getId());
                    if (doctorListFromDB != null) {
                        int rand = (int) (Math.random() * doctorListFromDB.size());
                        if (rand == doctorListFromDB.size()) {
                            rand--;
                        }
                        for (Patient patient : patientListFromDB) {
                            getDoctorMapper(sqlSession).changeDoctor(patient, doctorListFromDB.get(rand));
                        }
                    }
                }
                getDoctorMapper(sqlSession).delete(doctor);
            } catch (RuntimeException e) {
                LOGGER.info("Can't delete Doctor {}, {}", doctor, e);
                sqlSession.rollback();
                throw e;
            }
            sqlSession.commit();
        }
    }

    @Override
    public void deleteAll() throws RuntimeException {
        LOGGER.info("DAO delete all");
        try (SqlSession sqlSession = getSession()) {
            try {
                getDoctorMapper(sqlSession).deleteAll();
                getDoctorMapper(sqlSession).alterTableAccount();
                getDoctorMapper(sqlSession).alterTableAppointment();
            } catch (RuntimeException e) {
                LOGGER.info("Can't delete all {}", e);
                sqlSession.rollback();
                throw e;
            }
            sqlSession.commit();
        }
    }

    private void patientSort(List<Patient> patientList) {
        patientList.sort(Comparator.comparingInt(Patient::getId));
    }
}
