package net.thumbtack.school.threads;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadLocalRandom;

enum Notice {
    DEVELOPER_FINISHED,
    DEVELOPER_GENERATED_TASK,
    DEVELOPER_CREATED_DEVELOPER,
    EXECUTOR_DONE_TASK
}

class MultistageTask implements Executable {
    private final List<Executable> stages;
    private int curTask;

    public MultistageTask(List<Executable> stages) {
        this.stages = Collections.synchronizedList(stages);
        this.curTask = 0;
    }

    public boolean isFinish() {
        return curTask == stages.size();
    }

    @Override
    public void execute() {
        stages.get(curTask).execute();
        curTask++;
    }
}

class CreateTasksStream extends Thread {
    private final BlockingQueue<Executable> queue;
    private final BlockingQueue<Notice> notices;

    public CreateTasksStream(BlockingQueue<Executable> queue, BlockingQueue<Notice> notices) {
        this.queue = queue;
        this.notices = notices;
    }

    public void run() {
        try {
            System.out.println("Start create task");
            int n = ThreadLocalRandom.current().nextInt(10);
            for (int i = 0; i < n; i++) {
                System.out.println("put task " + i);
                queue.put(new MultistageTask(Arrays.asList(
                        () -> System.out.println(1),
                        () -> System.out.println(2),
                        () -> System.out.println(3))));
                notices.put(Notice.DEVELOPER_GENERATED_TASK);
            }
            notices.put(Notice.DEVELOPER_FINISHED);
            System.out.println("End create task");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

class CompleteTasksStream extends Thread {
    private final BlockingQueue<Executable> queue;
    private final BlockingQueue<Notice> notices;

    CompleteTasksStream(BlockingQueue<Executable> queue, BlockingQueue<Notice> notices) {
        this.queue = queue;
        this.notices = notices;
    }

    public void run() {
        try {
            System.out.println("Start executor");
            Executable curTask = queue.take();
            while (curTask != Executable.execut) {
                curTask.execute();
                if (curTask instanceof MultistageTask &&
                        !((MultistageTask) curTask).isFinish())
                    queue.put(curTask);
                else {
                    System.out.println("Executor done task");
                    notices.put(Notice.EXECUTOR_DONE_TASK);
                }
                curTask = queue.take();
            }
            System.out.println("End executor");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
