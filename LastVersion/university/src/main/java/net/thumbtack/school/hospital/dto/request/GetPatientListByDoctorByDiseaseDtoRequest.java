package net.thumbtack.school.hospital.dto.request;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class GetPatientListByDoctorByDiseaseDtoRequest {
    private String token;
    private String disease;

    GetPatientListByDoctorByDiseaseDtoRequest(String disease, String token) {
        setDisease(disease);
        setToken(token);
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDisease() {
        return disease;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }
}
