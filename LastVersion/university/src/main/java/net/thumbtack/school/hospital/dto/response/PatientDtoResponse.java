package net.thumbtack.school.hospital.dto.response;

import lombok.NoArgsConstructor;
import java.util.Objects;

@NoArgsConstructor
public class PatientDtoResponse extends BaseResponse {
    private int patientID;
    private String firstName;
    private String lastName;
    private String disease;
    private String login;
    private String password;

    public PatientDtoResponse(String firstName, String lastName, int patientID) {
        setFirstName(firstName);
        setLastName(lastName);
        setPatientID(patientID);
    }

    public PatientDtoResponse(String firstName, String lastName, int patientID, String disease, String login, String password) {
        this(firstName, lastName, patientID);
        setLogin(login);
        setPassword(password);
        setDisease(disease);
    }

    public String getFirstName() {
        return firstName;
    }

    private void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    private void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getPatientID() {
        return patientID;
    }

    public void setPatientID(int patientID) {
        this.patientID = patientID;
    }

    public String getDisease() {
        return disease;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PatientDtoResponse)) return false;
        PatientDtoResponse that = (PatientDtoResponse) o;
        return getPatientID() == that.getPatientID() && Objects.equals(getFirstName(), that.getFirstName()) && Objects.equals(getLastName(), that.getLastName()) && Objects.equals(getDisease(), that.getDisease()) && Objects.equals(getLogin(), that.getLogin()) && Objects.equals(getPassword(), that.getPassword());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPatientID(), getFirstName(), getLastName(), getDisease(), getLogin(), getPassword());
    }
}
