package net.thumbtack.school.hospital.Configuration;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.configuration2.ex.ConfigurationException;

import java.io.File;

public class Settings {
    private static final Logger LOGGER = LoggerFactory.getLogger(Settings.class);

    public Settings() {
        Configurations configs = new Configurations();
        try {
            Configuration config = configs.properties(new File("config.properties"));

            String dbHost = config.getString("database.host");
            int dbPort = config.getInt("database.port");
            String dbUser = config.getString("database.user");
            String dbPassword = config.getString("database.password", "secret");
            long dbTimeout = config.getLong("database.timeout");

        } catch (ConfigurationException ex) {
            LOGGER.info(ex.getMessage());
        }
    }
}
