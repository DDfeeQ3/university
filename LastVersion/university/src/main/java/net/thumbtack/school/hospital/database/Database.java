package net.thumbtack.school.hospital.database;

import net.thumbtack.school.hospital.database.ConcurrentCollections.MyArrayListValuedHashMap;
import net.thumbtack.school.hospital.database.ConcurrentCollections.MyDualHashBidiMap;
import net.thumbtack.school.hospital.exceptions.ErrorCode;
import net.thumbtack.school.hospital.exceptions.ServerException;
import net.thumbtack.school.hospital.model.*;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Database {
    private static Database instance;

    private MyDualHashBidiMap<String, User> userByToken;
    private MyDualHashBidiMap<String, User> userByLogin;
    private MyDualHashBidiMap<Integer, User> userByID;
    private MyDualHashBidiMap<String, Doctor> doctorBySpec;
    private MyArrayListValuedHashMap<String, Patient> patientByDisease;
    private AtomicInteger nextID = new AtomicInteger(0);


    public Database(MyDualHashBidiMap<String, User> userByToken,
                    MyDualHashBidiMap<String, User> userByLogin,
                    MyDualHashBidiMap<Integer, User> userByID,
                    MyDualHashBidiMap<String, Doctor> doctorBySpec,
                    MyArrayListValuedHashMap<String, Patient> patientByDisease) {

        this.userByToken = userByToken;
        this.userByLogin = userByLogin;
        this.userByID = userByID;
        this.doctorBySpec = doctorBySpec;
        this.patientByDisease = patientByDisease;
    }

    public Database() {
        userByToken = new MyDualHashBidiMap<>();
        userByLogin = new MyDualHashBidiMap<>();
        userByID = new MyDualHashBidiMap<>();
        doctorBySpec = new MyDualHashBidiMap<>();
        patientByDisease = new MyArrayListValuedHashMap<>();
    }

    public static Database getInstance(MyDualHashBidiMap<String, User> userByToken,
                                       MyDualHashBidiMap<String, User> userByLogin,
                                       MyDualHashBidiMap<Integer, User> userByID,
                                       MyDualHashBidiMap<String, Doctor> doctorBySpec,
                                       MyArrayListValuedHashMap<String, Patient> patientByDisease) {
        if (instance == null) {
            instance = new Database(userByToken, userByLogin, userByID, doctorBySpec,
                    patientByDisease);
        }
        return instance;
    }

    public static Database getInstance() {
        if (instance == null) {
            instance = new Database();
        }
        return instance;
    }

    public User getUserByLogin(String login) {
        return userByLogin.getV(login);
    }

    public User getUserByToken(String token) {
        return userByToken.getV(token);
    }

    public String registerDoctor(Doctor doctor) throws ServerException {
        if (userByLogin.putIfAbsent(doctor.getLogin(), doctor) != null) {
            throw new ServerException(ErrorCode.LOGIN_ALREADY_EXISTS_ERROR);
        }
        doctor.setId(nextID.getAndIncrement());

        userByID.put(doctor.getId(), doctor);
        doctorBySpec.put(doctor.getSpeciality(), doctor);
        String token = (UUID.randomUUID()).toString();
        userByToken.put(token, doctor);
        return token;
    }

    public String registerPatient(Patient patient, Doctor doctor) throws ServerException {
        if (userByLogin.putIfAbsent(patient.getLogin(), patient) != null) {
            throw new ServerException(ErrorCode.LOGIN_ALREADY_EXISTS_ERROR);
        }
        patient.setId(nextID.getAndIncrement());
        userByID.put(patient.getId(), patient);
        String token = (UUID.randomUUID()).toString();

        userByToken.put(token, patient);
        patientByDisease.put(patient.getDisease(), patient);
        doctor.getPatientList().add(patient);

        return token;
    }

    public String loginUser(User user) throws ServerException {
        if (!userByLogin.containsByValue(user)) {
            throw new ServerException(ErrorCode.NON_LOGGED_USER_ERROR);
        }
        if (userByToken.containsByValue(user)) {
            return userByToken.getK(user);
        }
        String token = (UUID.randomUUID()).toString();
        userByToken.put(token, user);
        return token;
    }

    public String logoutUser(String token) {
        userByToken.remove(token);
        return null;
    }

    public User getByLoginAndPassword(String login, String password) throws ServerException {
        if (!userByLogin.containsByKey(login)) {
            throw new ServerException(ErrorCode.NON_LOGGED_USER_ERROR);
        }
        if (!userByLogin.getV(login).getPassword().equals(password)) {
            throw new ServerException(ErrorCode.WRONG_PASSWORD_ERROR);
        }
        return userByLogin.getV(login);
    }

    public User getByLogin(String login) throws ServerException {
        if (!userByLogin.containsByKey(login)) {
            throw new ServerException(ErrorCode.NON_LOGGED_USER_ERROR);
        }
        return userByLogin.getV(login);
    }

    public List<Doctor> getAllDoctors() throws ServerException {
        return new LinkedList<>(doctorBySpec.values());
    }


    //Список всех своих пациентов
    public List<Patient> getPatientListByDoctor(Doctor doctor) throws ServerException {
        return doctor.getPatientList();
    }

    //Список всех своих пациентов с указанным названием болезни
    public List<Patient> getPatientListByDoctorByDisease(Doctor doctor, String disease) throws ServerException {
        if (!patientByDisease.containsByKey(disease)) {
            throw new ServerException(ErrorCode.INVALID_DISEASE);
        }
        List<Patient> patientList = new ArrayList<>();
        for (Patient patient : doctor.getPatientList()) {
            if (Objects.equals(patient.getDisease(), disease)) {
                patientList.add(patient);
            }
        }
        return patientList;
    }

    //Список всех своих пациентов, получающие некоторое назначение
    public List<Patient> getPatientListByDoctorByAppointment(Doctor doctor, Appointment appointment) throws ServerException {
        List<Patient> patientList = new ArrayList<>();
        for (Patient patient : doctor.getPatientList()) {
            if (patient.getAppointments().contains(appointment)) {
                patientList.add(patient);
            }
        }
        return patientList;
    }

    //список всех пациентов, больных той или иной болезнью,
    //в том числе и не своих, с указанием, кто является врачом этого пациента
    public List<Patient> getPatientListByDisease(String disease) throws ServerException {
        if (!patientByDisease.containsByKey(disease)) {
            throw new ServerException(ErrorCode.INVALID_DISEASE);
        }
        return patientByDisease.get(disease);
    }

    //список всех пациентов, получающих то или иное назначение,
    //в том числе и не своих, с указанием, кто является врачом этого пациента
    public List<Patient> getPatientListByAppointment(Appointment appointment) throws ServerException {
        List<Patient> patientList = new LinkedList<>();
        List<Patient> tempPatientList = new LinkedList<>();
        for (Doctor doctor : doctorBySpec.values()) {
            tempPatientList.addAll(doctor.getPatientList());
        }
        for (Patient patient : tempPatientList) {
            if (patient.getAppointments() != null) {
                if (patient.getAppointments().contains(appointment)) {
                    patientList.add(patient);
                }
            }
        }
        return patientList;
    }

    public void deleteDoctor(Doctor doctor) throws ServerException {
        userByToken.remove(userByToken.getK(doctor));
        userByLogin.remove(doctor.getLogin());
        userByID.remove(doctor.getId());
        doctorBySpec.remove(doctor.getSpeciality());
        List<Patient> patients = doctor.getPatientList();
        Doctor doctorNext = doctorBySpec.getV(doctor.getSpeciality());
        if (doctorNext == null) {
            doctorNext = doctorBySpec.values().stream().findAny().orElse(null);
        }
        Doctor chooseDoctor = doctorNext;
        if (chooseDoctor != null) {
            patients.forEach(p -> p.setDoctor(chooseDoctor));
            chooseDoctor.getPatientList().addAll(patients);
        }
    }

    public void changePatientPassword(Patient patient, String newPassword) throws ServerException {
        userByLogin.getV(patient.getLogin()).setPassword(newPassword);
    }

    public List<Appointment> getAppointmentsByPatient(Patient patient) throws ServerException {
        return patient.getAppointments();
    }

    public Doctor getDoctorByPatient(Patient patient) throws ServerException {
        return patient.getDoctor();
    }

    public void deletePatient(Patient patient) {
        if (userByToken.containsByValue(patient)) {
            userByToken.remove(userByToken.getK(patient));
        }
        userByLogin.remove(patient.getLogin());
        userByID.remove(patient.getId());
        patientByDisease.remove(patient.getDisease());
        patient.getDoctor().getPatientList().remove(patient);
    }

    public void toClearDataBase() {
        userByToken.clear();
        userByLogin.clear();
        userByID.clear();
        doctorBySpec.clear();
        patientByDisease.clear();
    }

    public User getUserByID(int userID) throws ServerException {
        User user = userByID.getV(userID);
        if (user == null) {
            throw new ServerException(ErrorCode.NON_LOGGED_USER_ERROR);
        }
        return user;
    }

    public String getTokenById(int id) throws ServerException {
        User user = userByID.getV(id);
        if (user == null) {
            throw new ServerException(ErrorCode.NON_LOGGED_USER_ERROR);
        }
        return userByToken.getK(user);
    }
}
