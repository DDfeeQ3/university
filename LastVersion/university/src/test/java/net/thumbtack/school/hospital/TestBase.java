package net.thumbtack.school.hospital;

import net.thumbtack.school.hospital.model.*;
import net.thumbtack.school.hospital.mybatis.dao.DoctorDao;
import net.thumbtack.school.hospital.mybatis.dao.PatientDao;
import net.thumbtack.school.hospital.mybatis.daoimpl.DoctorDaoImpl;
import net.thumbtack.school.hospital.mybatis.daoimpl.PatientDaoImpl;
import net.thumbtack.school.hospital.mybatis.utils.HospitalUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class TestBase {

    protected DoctorDao doctorDao = new DoctorDaoImpl();
    protected PatientDao patientDao = new PatientDaoImpl();

    private static boolean setUpIsDone = false;

    @BeforeAll()
    public static void setUp() {
        if (!setUpIsDone) {
            boolean initSqlSessionFactory = HospitalUtils.initSqlSessionFactory();
            if (!initSqlSessionFactory) {
                throw new RuntimeException("Can't create connection, stop");
            }
            setUpIsDone = true;
        }
    }

    @BeforeEach()
    public void clearDatabase() {
        doctorDao.deleteAll();
    }

    protected Doctor insertDoctor(Doctor doctor) {
        doctorDao.insert(doctor);
        assertNotEquals(0, doctor.getId());
        return doctor;
    }

    protected Patient insertPatient(Doctor doctor, Patient patient) {
        patientDao.insert(doctor, patient);
        assertNotEquals(0, patient.getId());
        return patient;
    }

    protected Appointment insertAppointment(Patient patient, Appointment appointment) {
        doctorDao.addAppointment(patient.getId(), appointment);
        assertNotEquals(0, appointment.getId());
        patient.addAppointment(appointment);
        return appointment;
    }

    protected List<Appointment> insertAppointments(Patient patient, List<Appointment> appointmentList) {
        for (Appointment appointment : appointmentList) {
            doctorDao.addAppointment(patient.getId(), appointment);
        }
        appointmentList.sort(Comparator.comparingInt(Appointment::getId));
        patient.addAppointments(appointmentList);
        return appointmentList;
    }
}
