package net.thumbtack.school.threads;

import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class threadsTest {

    @Test
    public void testTasks1_3() {
        //Less 1
        Thread thread = Thread.currentThread();
        System.out.println(thread);
        System.out.println("_ _ _ _ _");
        System.out.println(" Id: " + thread.getId());
        System.out.println(" Name: " + thread.getName());
        System.out.println(" Priority: " + thread.getPriority());
        System.out.println(" State: " + thread.getState());
        System.out.println(" Thread group: " + thread.getThreadGroup());
        System.out.println("_ _ _ _ _\n");

        //Less 2
        ChildThread childThread = new ChildThread();
        childThread.start();

        //Less 3
        MyThread threadTheFirst = new MyThread("The First");
        MyThread threadTheSecond = new MyThread("The Second");
        MyThread threadTheThirdly = new MyThread("The Thirdly");
        threadTheFirst.start();
        threadTheSecond.start();
        threadTheThirdly.start();
    }

    @Test
    public void testTask4() {
        List<Integer> list = new ArrayList<>();
        AddIntThreadTask4 addIntThread = new AddIntThreadTask4(list);
        DeleteIntThreadTask4 deleteIntThread = new DeleteIntThreadTask4(list);
        addIntThread.start();
        deleteIntThread.start();
        try {
            addIntThread.join();
            deleteIntThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.print("List: " + list);
    }

    @Test
    public void testTask5() {
        List<Integer> list = new ArrayList<>();
        ArrayWrapper arrayWrapper = new ArrayWrapper(list);
        ArrayThreadTask5 addThread = new ArrayThreadTask5(arrayWrapper, OperationType.ADD);
        ArrayThreadTask5 deleteThread = new ArrayThreadTask5(arrayWrapper, OperationType.DELETE);
        addThread.start();
        deleteThread.start();
        try {
            addThread.join();
            deleteThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.print("List: " + list);
    }

    @Test
    public void testTask6() {
        List<Integer> list = Collections.synchronizedList(new ArrayList<>());
        AddIntThreadTask6 addIntThread = new AddIntThreadTask6(list);
        DeleteIntThreadTask6 deleteIntThread = new DeleteIntThreadTask6(list);
        addIntThread.start();
        deleteIntThread.start();
        try {
            addIntThread.join();
            deleteIntThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.print("List: " + list);
    }

    @Test
    public void testTask7() {
        PingPong pingPong = new PingPong();
        AtomicInteger limitationPing = new AtomicInteger(5);
        AtomicInteger limitationPong = new AtomicInteger(5);

        // Тут поставлено ограничение на пинг и понг, чтобы нормально коммитилось
        Thread threadPing = new Thread(() -> {
            while (limitationPing.get() > 0) {
                pingPong.printPing();
                limitationPing.getAndDecrement();
            }
        });

        Thread threadPong = new Thread(() -> {
            while (limitationPong.get() > 0) {
                pingPong.printPong();
                limitationPong.getAndDecrement();
            }
        });
        //Бесконечные пинг и понг
/*        Thread threadPing = new Thread(() -> {
            while (true) {
                pingPong.printPing();
            }
        });

        Thread threadPong = new Thread(() -> {
            while (true) {
                pingPong.printPong();
            }
        });*/

        threadPing.start();
        threadPong.start();

        try {
            threadPing.join();
            threadPong.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testTask8() {
        ReaderWriter readerWriter = new ReaderWriter();

        Runnable write = () -> {
            for (int i = 0; i < 5; i++) {
                readerWriter.write('A');
            }
        };

        Runnable read = () -> {
            for (int i = 0; i < 5; i++) {
                readerWriter.read();
            }
        };

        new Thread(write).start();
        new Thread(read).start();
    }

    @Test
    public void testTask10() {
        Lock lock = new ReentrantLock();
        List<Integer> list = new ArrayList<>();
        AddIntThreadTask10 addIntThread = new AddIntThreadTask10(list, lock);
        DeleteIntThreadTask10 deleteIntThread = new DeleteIntThreadTask10(list, lock);
        addIntThread.start();
        deleteIntThread.start();
        try {
            addIntThread.join();
            deleteIntThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.print("List: " + list);
    }

    @Test
    public void testTask11() {
        //в классе Task11, поставлены ограничения на пинг и понг, чтобы коммит проходил нормально
        ConditionPingPong conditionPingPong = new ConditionPingPong();
        Thread ping = new PingThread(conditionPingPong);
        Thread pong = new PongThread(conditionPingPong);
        ping.start();
        pong.start();
        try {
            ping.join();
            pong.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testTask12() {
        MyConcurrentHashMap<Integer, Integer> map = new MyConcurrentHashMap<>();

        Thread addThread = new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                map.put(i, i * i);
                System.out.println("Ключ: " + i + "; Элемент: " + i * i);
            }
        });

        Thread removeThread = new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                map.remove(i);
                System.out.println("Элемент с ключом " + i + " удален");
            }
        });

        addThread.start();
        removeThread.start();
        try {
            addThread.join();
            removeThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testTask13() {
        Formatter formatter = new Formatter();
        Date date = new Date();

        Thread t1 = new Thread(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Thread 1");
            formatter.format(date);
        });

        Thread t2 = new Thread(() -> {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Thread 2");
            formatter.format(date);
        });

        Thread t3 = new Thread(() -> {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Thread 3");
            formatter.format(date);
        });

        Thread t4 = new Thread(() -> {
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Thread 4");
            formatter.format(date);
        });

        Thread t5 = new Thread(() -> {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Thread 5");
            formatter.format(date);
        });

        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();

        try {
            t1.join();
            t2.join();
            t3.join();
            t4.join();
            t5.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testTask14() {
        List<String> emailAddresses = new LinkedList<>();
        try {
            File file = new File("emails.txt");
            FileReader fr = new FileReader(file);
            BufferedReader reader = new BufferedReader(fr);
            String line = reader.readLine();
            while (line != null) {
                emailAddresses.add(line);
                line = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Transport transport = new Transport();

        ExecutorService threadPool = Executors.newFixedThreadPool(emailAddresses.size());
        for (String email : emailAddresses) {
            Message message = new Message(email, "почтальон", "письмо", "текст сообщения");
            threadPool.execute(() -> transport.send(message));
        }
        threadPool.shutdown();
    }

    @Test
    public void testTask15() {
        BlockingQueue<Data> queue = new ArrayBlockingQueue<>(10);

        WriterStream producer1 = new WriterStream(queue);
        WriterStream producer2 = new WriterStream(queue);
        ReaderStream consumer1 = new ReaderStream(queue);
        ReaderStream consumer2 = new ReaderStream(queue);
        ReaderStream consumer3 = new ReaderStream(queue);

        producer1.start();
        producer2.start();
        consumer1.start();
        consumer2.start();
        consumer3.start();
        try {
            producer1.join();
            producer2.join();
            System.out.println("put null");
            queue.put(new Data((int[]) null));
            consumer1.join();
            consumer2.join();
            consumer3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testTask16() {
        BlockingQueue<Executable> queue = new ArrayBlockingQueue<>(5);

        TaskStream producer1 = new TaskStream(queue);
        TaskStream producer2 = new TaskStream(queue);
        ExecutorStream consumer1 = new ExecutorStream(queue);
        ExecutorStream consumer2 = new ExecutorStream(queue);
        ExecutorStream consumer3 = new ExecutorStream(queue);

        producer1.start();
        producer2.start();
        consumer1.start();
        consumer2.start();
        consumer3.start();
        try {
            producer1.join();
            producer2.join();
            System.out.println("put null");
            queue.put(Executable.execut);
            consumer1.join();
            consumer2.join();
            consumer3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testTask17() {
        BlockingQueue<Executable> queue = new LinkedBlockingQueue<>();
        BlockingQueue<Notice> notices = new LinkedBlockingQueue<>();

        CreateTasksStream producer1 = new CreateTasksStream(queue, notices);
        CreateTasksStream producer2 = new CreateTasksStream(queue, notices);
        CompleteTasksStream consumer1 = new CompleteTasksStream(queue, notices);
        CompleteTasksStream consumer2 = new CompleteTasksStream(queue, notices);
        CompleteTasksStream consumer3 = new CompleteTasksStream(queue, notices);

        producer1.start();
        producer2.start();
        consumer1.start();
        consumer2.start();
        consumer3.start();

        int generatedTasks = 0;
        int finishedDevelopers = 0;
        int doneTasks = 0;

        try {
            while (finishedDevelopers != 2 || generatedTasks != doneTasks) {
                Notice notice = notices.take();
                switch (notice) {
                    case DEVELOPER_FINISHED:
                        finishedDevelopers++;
                        break;
                    case DEVELOPER_GENERATED_TASK:
                        generatedTasks++;
                        break;
                    case EXECUTOR_DONE_TASK:
                        doneTasks++;
                        break;
                }
            }
            queue.put(Executable.execut);
            queue.put(Executable.execut);
            queue.put(Executable.execut);

            consumer1.join();
            consumer2.join();
            consumer3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testTask18() {
        BlockingQueue<Executable> queue = new LinkedBlockingQueue<>();
        BlockingQueue<Notice> notices = new LinkedBlockingQueue<>();

        int n = 3;

        CreateTasks2Stream producer1 = new CreateTasks2Stream(queue, n, notices);
        CreateTasks2Stream producer2 = new CreateTasks2Stream(queue, n, notices);
        CompleteTasksStream consumer1 = new CompleteTasksStream(queue, notices);
        CompleteTasksStream consumer2 = new CompleteTasksStream(queue, notices);
        CompleteTasksStream consumer3 = new CompleteTasksStream(queue, notices);

        producer1.start();
        producer2.start();
        consumer1.start();
        consumer2.start();
        consumer3.start();

        int generatedTasks = 0;
        int countDeveloper = 2;
        int finishedDevelopers = 0;
        int doneTasks = 0;

        try {
            while (finishedDevelopers != countDeveloper || generatedTasks != doneTasks) {
                Notice notice = notices.take();
                switch (notice) {
                    case DEVELOPER_FINISHED:
                        finishedDevelopers++;
                        break;
                    case DEVELOPER_GENERATED_TASK:
                        generatedTasks++;
                        break;
                    case DEVELOPER_CREATED_DEVELOPER:
                        countDeveloper++;
                        break;
                    case EXECUTOR_DONE_TASK:
                        doneTasks++;
                        break;
                }
            }
            queue.put(Executable.execut);
            queue.put(Executable.execut);
            queue.put(Executable.execut);
            consumer1.join();
            consumer2.join();
            consumer3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
