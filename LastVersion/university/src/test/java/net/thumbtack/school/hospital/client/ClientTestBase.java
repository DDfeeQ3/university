package net.thumbtack.school.hospital.client;

import net.thumbtack.school.hospital.dto.request.*;
import net.thumbtack.school.hospital.dto.response.*;
import net.thumbtack.school.hospital.exceptions.ErrorCode;
import net.thumbtack.school.hospital.server.ServerTask18;
import net.thumbtack.school.hospital.utils.Settings;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;

import static org.junit.jupiter.api.Assertions.*;

public class ClientTestBase {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClientTestBase.class);
    protected static HospitalClient client = new HospitalClient();
    private static String baseURL;

    @BeforeAll
    public static void startServer() {
        setBaseUrl();
        ServerTask18.createServer();
    }

    @AfterAll
    public static void stopServer() {
        ServerTask18.stopServer();
    }

    @BeforeEach
    public void clearDataBase() {
        ServerTask18.clearDatabase();
    }

    private static void setBaseUrl() {
        String hostName = null;
        try {
            hostName = InetAddress.getLocalHost().getCanonicalHostName();
        } catch (UnknownHostException e) {
            LOGGER.debug("Can't determine my own host name", e);
        }
        baseURL = "http://" + hostName + ":" + Settings.getRestHttpPort() + "/api";
    }

    public static String getBaseURL() {
        return baseURL;
    }

    protected void checkFailureResponse(Object response, ErrorCode expectedStatus) {
        assertTrue(response instanceof ErrorResponse);
        ErrorResponse failureResponseObject = (ErrorResponse) response;
        assertEquals(expectedStatus, failureResponseObject.getErrorCode());
    }

    protected RegisterDoctorDtoResponse registerDoctor(String firstName, String lastName, String speciality, String login,
                                                       String password, ErrorCode expectedStatus) {
        RegisterDoctorDtoRequest request = new RegisterDoctorDtoRequest(firstName, lastName, speciality, login, password);
        Object response = client.post(baseURL + "/registerDoctor", request, RegisterDoctorDtoResponse.class);
        if (response instanceof RegisterDoctorDtoResponse) {
            assertEquals(ErrorCode.OK, expectedStatus);
            RegisterDoctorDtoResponse response1 = (RegisterDoctorDtoResponse) response;
            assertNotNull(response1.getToken());
            return response1;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected RegisterPatientDtoResponse registerPatient(String firstName, String lastName, String disease, String login,
                                                         String password, String loginDoctor, ErrorCode expectedStatus) {
        RegisterPatientDtoRequest request = new RegisterPatientDtoRequest(lastName, firstName, disease, login, password, loginDoctor);
        Object response = client.post(baseURL + "/registerPatient", request, RegisterPatientDtoResponse.class);
        if (response instanceof RegisterPatientDtoResponse) {
            assertEquals(ErrorCode.OK, expectedStatus);
            RegisterPatientDtoResponse response1 = (RegisterPatientDtoResponse) response;
            assertNotNull(response1.getToken());
            return response1;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected AddAppointmentDtoResponse addAppointment(MethodTypeRequest methodTypeRequest, String methodName, int frequency, int patientID, ErrorCode expectedStatus) {
        AddAppointmentDtoRequest request = new AddAppointmentDtoRequest(methodTypeRequest, methodName, frequency, patientID);
        Object response = client.post(baseURL + "/addAppointment", request, AddAppointmentDtoResponse.class);
        if (response instanceof AddAppointmentDtoResponse) {
            assertEquals(ErrorCode.OK, expectedStatus);
            AddAppointmentDtoResponse response1 = (AddAppointmentDtoResponse) response;
            assertNotNull(response1.getTokenPatient());
            return response1;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }
}
