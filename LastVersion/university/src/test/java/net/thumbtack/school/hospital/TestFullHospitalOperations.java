package net.thumbtack.school.hospital;

import net.thumbtack.school.hospital.model.*;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class TestFullHospitalOperations extends TestBase {

    private <T extends User> void userListEquals(List<T> userList1, List<T> userList2) {
        assertEquals(userList1.size(), userList2.size());
        for (int i = 0; i < userList1.size(); i++) {
            assertEquals(userList1.get(i).getId(), userList2.get(i).getId());
            assertEquals(userList1.get(i).getFirstName(), userList2.get(i).getFirstName());
            assertEquals(userList1.get(i).getLastName(), userList2.get(i).getLastName());
            assertEquals(userList1.get(i).getPassword(), userList2.get(i).getPassword());
            assertEquals(userList1.get(i).getLogin(), userList2.get(i).getLogin());
        }
    }

    private <T extends User> void usersEquals(T user1, T user2) {
        assertEquals(user1.getId(), user2.getId());
        assertEquals(user1.getFirstName(), user2.getFirstName());
        assertEquals(user1.getLastName(), user2.getLastName());
        assertEquals(user1.getPassword(), user2.getPassword());
        assertEquals(user1.getLogin(), user2.getLogin());
    }

    private void appointmentListEquals(List<Appointment> appointmentList1, List<Appointment> appointmentList2) {
        assertEquals(appointmentList1.size(), appointmentList2.size());
        for (int i = 0; i < appointmentList1.size(); i++) {
            assertEquals(appointmentList1.get(i).getId(), appointmentList2.get(i).getId());
            assertEquals(appointmentList1.get(i).getTreatmentMethod(), appointmentList2.get(i).getTreatmentMethod());
            assertEquals(appointmentList1.get(i).getMethodType(), appointmentList2.get(i).getMethodType());
            assertEquals(appointmentList1.get(i).getFrequency(), appointmentList2.get(i).getFrequency());
            assertEquals(appointmentList1.get(i).getPatientId(), appointmentList2.get(i).getPatientId());
        }
    }

    @Test
    public void testInsertDoctorAndPatientWithAppointment() {
        Doctor doctor = insertDoctor(new Doctor("Николай", "Николаев", "nikola1", "kolya_1985", "Терапевт"));
        Patient patient = new Patient(doctor ,"Петр", "Петров", "Грипп", "petrov1", "petya_1999");
        Appointment appointment = new Appointment(MethodType.MEDICINE, "Таблетка", 3);
        patient.addAppointment(appointment);
        patient = insertPatient(doctor, patient);
        doctor.setPatient(patient);
        Patient patientFromDB = patientDao.getById(patient.getId());

        Doctor doctorFromDB = doctorDao.getById(doctor.getId());
        usersEquals(doctor, doctorFromDB);
        usersEquals(patient, patientFromDB);
    }

    @Test
    public void testChangePatientPasswordAndGetDoctorByPatient() {
        Doctor doctor = insertDoctor(new Doctor("Николай", "Николаев", "nikola1", "kolya_1985", "Терапевт"));
        Patient patient = new Patient(doctor,"Петр", "Петров", "Грипп", "petrov1", "petya_1999");
        patient = insertPatient(doctor, patient);

        patientDao.changePassword(patient, "newPassword");

        Patient patientFromDB = patientDao.getById(patient.getId());

        usersEquals(patient, patientFromDB);

        Doctor doctorFromDB = patientDao.getDoctorByPatient(patient);
        usersEquals(doctor, doctorFromDB);
    }

    @Test
    public void testGetAllAppointmentsPatient() {
        Doctor doctor = insertDoctor(new Doctor("Николай", "Николаев", "nikola1", "kolya_1985", "Терапевт"));
        Patient patient = new Patient(doctor,"Петр", "Петров", "Грипп", "petrov1", "petya_1999");

        patient = insertPatient(doctor, patient);

        Appointment appointment1 = new Appointment(MethodType.MEDICINE, "Таблетка", 3);
        Appointment appointment2 = new Appointment(MethodType.MEDICINE, "Сироп", 2);
        Appointment appointment3 = new Appointment(MethodType.PROCEDURE, "Капельница", 1);

        List<Appointment> appointmentList = new LinkedList<>();
        appointmentList.add(appointment1);
        appointmentList.add(appointment2);
        appointmentList.add(appointment3);
        insertAppointments(patient, appointmentList);

        List<Appointment> appointmentListFromDB = patientDao.getAllAppointmentsByPatient(patient);

        appointmentListEquals(appointmentList, appointmentListFromDB);

        patient.setAppointments(appointmentList);
        patientDao.changePassword(patient, "newPassword");
        Patient patientFromDB = patientDao.getById(patient.getId());

        usersEquals(patient, patientFromDB);
    }

    @Test
    public void testGetAllPatients() {
        List<Patient> patientList = new LinkedList<>();

        Doctor doctor1 = insertDoctor(new Doctor("Николай", "Николаев", "nikola1", "kolya_1985", "Терапевт"));
        Doctor doctor2 = insertDoctor(new Doctor("Роман", "Романов", "romka1", "roma_1991", "Хирург"));

        Patient patient1 = insertPatient(doctor1, new Patient(doctor1,"Петр", "Петров", "Грипп", "petrov1", "petya_1999"));
        Patient patient2 = insertPatient(doctor2, new Patient(doctor2,"Иван", "Иванов", "Грипп", "vanya98", "vanya_1998"));
        Patient patient3 = insertPatient(doctor1, new Patient(doctor1,"Николай", "Петров", "Сломанная нога", "nikola00", "nikola_00"));
        Patient patient4 = insertPatient(doctor2, new Patient(doctor2,"Инна", "Нарежко", "Простуда", "innochka1", "inna_2001"));

        Appointment appointment1 = new Appointment(MethodType.MEDICINE, "Таблетка", 3);
        Appointment appointment2 = new Appointment(MethodType.MEDICINE, "Таблетка", 2);
        Appointment appointment3 = new Appointment(MethodType.MEDICINE, "Таблетка", 3);
        insertAppointment(patient1, appointment1);
        insertAppointment(patient2, appointment2);
        insertAppointment(patient4, appointment3);

        //By Doctor
        patientList.add(patient1);
        patientList.add(patient3);

        List<Patient> patientListFromDB = doctorDao.getPatientListByDoctor(doctor1);


        userListEquals(patientList, patientListFromDB);


        //By Disease
        patientList.clear();
        patientList.add(patient1);
        patientList.add(patient2);

        patientListFromDB = doctorDao.getPatientListByDisease("Грипп");

        userListEquals(patientList, patientListFromDB);

        //By Doctor and Disease
        patientList.clear();
        patientList.add(patient1);

        patientListFromDB = doctorDao.getPatientListByDoctorByDisease(doctor1, "Грипп");

        userListEquals(patientList, patientListFromDB);

        //By Doctor and Appointment
        patientList.clear();

        patientList.add(patient1);

        patientListFromDB = doctorDao.getPatientListByDoctorByAppointment(doctor1, appointment1);

        userListEquals(patientList, patientListFromDB);

        //By Appointment
        patientList.add(patient4);

        patientListFromDB = doctorDao.getPatientListByAppointment(appointment1);

        userListEquals(patientList, patientListFromDB);

        //By Appointment and Delete patient4
        patientList.clear();
        patientList.add(patient1);
        doctorDao.deletePatient(patient4);

        patientListFromDB = doctorDao.getPatientListByAppointment(appointment1);

        userListEquals(patientList, patientListFromDB);

        //Delete doctor
        //У нас есть доктор - Терапевт(doctor1)[у него два пациента: patient1, patient3],
        //его удаляем и хотим перенаправить всех пациентов от doctor1 ко второму Терапевту(doctor3)
        patientList.clear();
        patientList.add(patient1);
        patientList.add(patient3);
        Doctor doctor3 = insertDoctor(new Doctor("Иван", "Иванов", "ivanov-terapevt", "terapevt_2", "Терапевт"));

        patient1.setDoctor(doctor3);
        patient3.setDoctor(doctor3);
        doctorDao.delete(doctor1);

        patientListFromDB = doctorDao.getPatientListByDoctor(doctor3);
        userListEquals(patientList, patientListFromDB);
        //Теперь удаляем последнего Терапевта и хотим отправить всех пациентов от doctor3 к doctor2
        patientList.clear();
        patientList.add(patient1);
        patientList.add(patient2);
        patientList.add(patient3);

        patient1.setDoctor(doctor2);
        patient2.setDoctor(doctor2);
        patient3.setDoctor(doctor2);
        doctorDao.delete(doctor3);
        doctor3 = null;

        patientListFromDB = doctorDao.getPatientListByDoctor(doctor2);
        userListEquals(patientList, patientListFromDB);
    }
}
