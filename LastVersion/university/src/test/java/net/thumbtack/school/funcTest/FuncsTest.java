package test.java.net.thumbtack.school.funcTest;

import net.thumbtack.school.func.Funcs;
import net.thumbtack.school.func.Person;
import net.thumbtack.school.func.PersonOpt;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.BinaryOperator;

import static org.junit.jupiter.api.Assertions.*;

public class FuncsTest {
    Funcs funcs = new Funcs();
    String str = "Три девицы под окном пряли поздно вечерком";

    //задания 1-3
    @Test
    public void split() {
        List<String> stringList = new LinkedList<>();
        stringList.add("Три");
        stringList.add("девицы");
        stringList.add("под");
        stringList.add("окном");
        stringList.add("пряли");
        stringList.add("поздно");
        stringList.add("вечерком");
        assertEquals(funcs.split(str), stringList);
    }

    @Test
    public void count() {
        assertEquals(funcs.count(funcs.split(str)), 7);
    }

    //Задание 4
    @Test
    public void splitAndCount() {
        assertEquals(funcs.splitAndCount(str), 7);
    }

    //Задание 5
    @Test
    public void createPerson() {
        assertEquals(funcs.create("Иван"), new Person("Иван"));
    }

    //Задание 6
    @Test
    public void max() {
        BinaryOperator<Integer> maxFunction = Math::max;
        assertEquals(maxFunction.apply(6, 3), 6);
    }

    //Задание 7
    @Test
    public void date() {
        assertEquals(funcs.getCurrentDate(), new Date());
    }

    //Задание 8
    @Test
    public void isEven() {
        //num % 3 == 0
        assertTrue(funcs.isEven(3));
        assertFalse(funcs.isEven(2));
    }

    //Задание 9
    @Test
    public void areEquals() {
        assertTrue(funcs.areEquals(3, 3));
        assertFalse(funcs.areEquals(3, 2));
    }

    //Задание 10
    @Test
    public void myFunction() {
        assertEquals(funcs.myFunction(funcs.split(str)), 7);
    }

    //Задание 12.a
    @Test
    public void getMothersMotherFather() {
        Person son = new Person("Son", new Person("Father"),
                new Person("Mother", new Person("Grandfather"),
                        new Person("Grandmother", new Person("Great-Grandfather"), new Person("Great-Grandmother"))));
        assertEquals(son.getMothersMotherFather(), new Person("Great-Grandfather"));
        son.getMother().setMother(null);
        assertNull(son.getMothersMotherFather());
    }

    //Задание 12.b
    @Test
    public void getMothersMotherFather2() {
        PersonOpt son = new PersonOpt("Son", new PersonOpt("Father"),
                new PersonOpt("Mother", new PersonOpt("Grandfather"),
                        new PersonOpt("Grandmother", new PersonOpt("Great-Grandfather"), new PersonOpt("Great-Grandmother"))));
        assertEquals(son.getMothersMotherFather(Optional.of(son)), son.getMother().flatMap(PersonOpt::getMother).flatMap(PersonOpt::getFather));
    }
}
