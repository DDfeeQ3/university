package test.java.net.thumbtack.school.funcTest;

import com.google.gson.Gson;
import net.thumbtack.school.hospital.dto.request.*;
import net.thumbtack.school.hospital.dto.response.*;
import net.thumbtack.school.hospital.exceptions.ErrorCode;
import net.thumbtack.school.hospital.server.Server;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ServerTest {
    Server server = new Server();
    Gson gson = new Gson();

    @BeforeEach()
    public void clear() {
        server.clearDatabase();
    }

    private String[] tokensForTest() {
        RegisterDoctorDtoRequest registerDoctorDtoRequest = new RegisterDoctorDtoRequest("Graves", "Willie", "surgeon", "admin", "123456");
        RegisterDoctorDtoRequest registerDoctorDtoRequest1 = new RegisterDoctorDtoRequest("King", "Bella", "surgeon", "not_admin", "1234561");
        RegisterDoctorDtoRequest registerDoctorDtoRequest2 = new RegisterDoctorDtoRequest("Johnson", "Phoebi", "psychiatrist", "maybe_admin", "not_at_all");
        String[] tokens = new String[8];
        tokens[0] = gson.fromJson(server.registerDoctor(gson.toJson(registerDoctorDtoRequest)), RegisterDoctorDtoResponse.class).getToken();
        tokens[1] = gson.fromJson(server.registerDoctor(gson.toJson(registerDoctorDtoRequest1)), RegisterDoctorDtoResponse.class).getToken();
        tokens[2] = gson.fromJson(server.registerDoctor(gson.toJson(registerDoctorDtoRequest2)), RegisterDoctorDtoResponse.class).getToken();

        RegisterPatientDtoRequest registerPatientDtoRequest = new RegisterPatientDtoRequest("Rufus", "French",
                "Unlucky", "patientGO", "maybenot", "admin");

        RegisterPatientDtoRequest registerPatientDtoRequest1 = new RegisterPatientDtoRequest("Jack", "Sparrow",
                "Black mark", "pirat", "barbossa", "not_admin");


        RegisterPatientDtoRequest registerPatientDtoRequest2 = new RegisterPatientDtoRequest("Alexunder", "Befus",
                "Lazy", "alwsangrywolf", "tessera", "maybe_admin");

        RegisterPatientDtoRequest registerPatientDtoRequest3 = new RegisterPatientDtoRequest("Tandziro", "Kamado",
                "Cancer", "slayer", "kootou", "not_admin");


        RegisterPatientDtoRequest registerPatientDtoRequest4 = new RegisterPatientDtoRequest("Last", "Guy",
                "Runny nose", "lebowski", "whereismoney", "maybe_admin");

        tokens[3] = gson.fromJson(server.registerPatient(gson.toJson(registerPatientDtoRequest)), LoginUserDtoResponse.class).getToken();
        tokens[4] = gson.fromJson(server.registerPatient(gson.toJson(registerPatientDtoRequest1)), LoginUserDtoResponse.class).getToken();
        tokens[5] = gson.fromJson(server.registerPatient(gson.toJson(registerPatientDtoRequest2)), LoginUserDtoResponse.class).getToken();
        tokens[6] = gson.fromJson(server.registerPatient(gson.toJson(registerPatientDtoRequest3)), LoginUserDtoResponse.class).getToken();
        tokens[7] = gson.fromJson(server.registerPatient(gson.toJson(registerPatientDtoRequest4)), LoginUserDtoResponse.class).getToken();
        return tokens;
    }

    @Test
    void loginAndLogoutTest() {
        RegisterDoctorDtoRequest registerDoctor = new RegisterDoctorDtoRequest("First", "Last", "Healer", "admin", "123456");
        String token = gson.fromJson(server.registerDoctor(gson.toJson(registerDoctor)), RegisterDoctorDtoResponse.class).getToken();

        LoginUserDtoRequest loginDoctor = new LoginUserDtoRequest("admin", "123456");
        assertEquals(ErrorCode.USER_ALREADY_AUTHORIZED_ERROR, gson.fromJson(
                server.loginDoctor(gson.toJson(loginDoctor)), ErrorResponse.class).getErrorCode()
        );

        LogoutUserDtoRequest logoutDoctor = new LogoutUserDtoRequest(token);
        assertEquals(gson.toJson(null), server.logoutDoctor(gson.toJson(logoutDoctor)));
        assertEquals(ErrorCode.NON_LOGGED_USER_ERROR, gson.fromJson(
                server.logoutDoctor(gson.toJson(logoutDoctor)), ErrorResponse.class).getErrorCode()
        );

        token = gson.fromJson(server.logoutDoctor(gson.toJson(loginDoctor)), RegisterDoctorDtoResponse.class).getToken();

        RegisterPatientDtoRequest registerPatientDtoRequest = new RegisterPatientDtoRequest("Pat", "ent",
                "Runny nose", "patient", "123123", "admin");

        server.registerPatient(gson.toJson(registerPatientDtoRequest));
    }

    @Test
    public void changePasswordTest() {

        String[] tokens = tokensForTest();

        //смена пароля
        ChangePatientPasswordDtoRequest changePatientPasswordDtoRequest = new ChangePatientPasswordDtoRequest("patientGO", "not_maybe", "maybenot");
        assertNull(gson.fromJson(server.changePasswordPatient(gson.toJson(changePatientPasswordDtoRequest)),
                String.class));

        LogoutUserDtoRequest logoutUserDtoRequest = new LogoutUserDtoRequest(tokens[3]);
        server.loginPatient(gson.toJson(logoutUserDtoRequest));
        //попытка захода по неактуальному паролю
        LoginUserDtoRequest loginUserDtoRequest = new LoginUserDtoRequest("patientGO", "or_maybe_not");
        ErrorResponse errorResponse = gson.fromJson(server.loginPatient(gson.toJson(loginUserDtoRequest)), ErrorResponse.class);
        assertEquals(ErrorCode.INVALID_LOGIN_OR_PASSWORD_ERROR, errorResponse.getErrorCode());
        //заход по актуальному паролю
        LoginUserDtoRequest loginUserDtoRequest1 = new LoginUserDtoRequest("patientGO", "not_maybe");
        tokens[3] = gson.fromJson(server.loginPatient(gson.toJson(loginUserDtoRequest1)), RegisterDoctorDtoResponse.class).getToken();
    }

    @Test
    public void appointmentTest() {
        String[] tokens = tokensForTest();
        GetPatientByTokenDtoRequest getPatientByTokenDtoRequest = new GetPatientByTokenDtoRequest(tokens[4]);
        int patientId = gson.fromJson(server.getPatientByToken(gson.toJson(getPatientByTokenDtoRequest)), PatientDtoResponse.class).getPatientID();

        GetAppointmentsByPatientDtoRequest getAppointmentsByPatientDtoRequest = new GetAppointmentsByPatientDtoRequest(patientId);
        String result = server.getAppointmentsByPatient(gson.toJson(getAppointmentsByPatientDtoRequest));
        AppointmentListDtoResponse response = gson.fromJson(result, AppointmentListDtoResponse.class);

        assertEquals(new AppointmentListDtoResponse(new LinkedList<>()).getAppointments(), response.getAppointments());
//      добавляем назначение
        AddAppointmentDtoRequest addAppointmentDtoRequest = new AddAppointmentDtoRequest(MethodTypeRequest.MEDICINE, "Lucky pills", 3, patientId);
        server.addAppointments(gson.toJson(addAppointmentDtoRequest));

        List<AppointmentDtoResponse> appList = new LinkedList<>();
        appList.add(new AppointmentDtoResponse(MethodTypeResponse.MEDICINE, "Lucky pills", 3));

        result = server.getAppointmentsByPatient(gson.toJson(getAppointmentsByPatientDtoRequest));
        response = gson.fromJson(result, AppointmentListDtoResponse.class);
        assertEquals(new AppointmentListDtoResponse(appList).getAppointments(), response.getAppointments());

//      получаем список пациентов доктора по одному назначению
        AppointmentDtoRequest appForRequest = new AppointmentDtoRequest(MethodTypeRequest.MEDICINE, "Lucky pills", 3);

        GetPatientListByDoctorByAppointmentDtoRequest getPatientListByDoctorByAppointmentsDtoRequest =
                new GetPatientListByDoctorByAppointmentDtoRequest(appForRequest, tokens[1]);

        List<PatientDtoResponse> patientDtoResponses = new LinkedList<>();
        patientDtoResponses.add(new PatientDtoResponse("Jack", "Sparrow", patientId));
        assertEquals(new PatientListDtoResponse(patientDtoResponses).getPatientList(),
                gson.fromJson(server.getPatientListByDoctorByAppointments(gson.toJson(getPatientListByDoctorByAppointmentsDtoRequest)),
                        PatientListDtoResponse.class).getPatientList());
    }

    @Test
    void registerTest() {
        RegisterDoctorDtoRequest registerDoctor = new RegisterDoctorDtoRequest("Firstname", "Lastname", "Healer", "admin", "1233456789");

        String token = gson.fromJson(server.registerDoctor(gson.toJson(registerDoctor)), RegisterDoctorDtoResponse.class).getToken();
        RegisterDoctorDtoRequest validateTest = new RegisterDoctorDtoRequest("", "Ly", "Healer", "admin2", "1122233344");
        ErrorResponse errorResponse = gson.fromJson(server.registerDoctor(gson.toJson(validateTest)), ErrorResponse.class);
        assertEquals(ErrorCode.WRONG_FIRST_NAME_ERROR, errorResponse.getErrorCode());
        RegisterDoctorDtoRequest alreadyRegisteredTest = new RegisterDoctorDtoRequest("Fee", "Loo", "Specialization", "admin", "123456784");
        errorResponse = gson.fromJson(server.registerDoctor(gson.toJson(alreadyRegisteredTest)), ErrorResponse.class);
        assertEquals(ErrorCode.LOGIN_ALREADY_EXISTS_ERROR, errorResponse.getErrorCode());

        RegisterPatientDtoRequest registerPatientDtoRequest = new RegisterPatientDtoRequest("Pat", "ent",
                "Runny nose", "patient", "123457", "admin");
        RegisterPatientDtoResponse response = gson.fromJson(server.registerPatient(gson.toJson(registerPatientDtoRequest)), RegisterPatientDtoResponse.class);
        assertEquals(registerPatientDtoRequest.getLogin(), response.getLogin());
    }

    @Test
    public void deleteDoctorAndPatient() {
        String[] tokens = tokensForTest();

        GetInfoAboutDoctorByPatientDtoRequest getInfoAboutDoctorByPatientDtoRequest = new GetInfoAboutDoctorByPatientDtoRequest(tokens[3]);
        DoctorDtoResponse getInfoAboutDoctorDtoResponse = new DoctorDtoResponse("Graves", "Willie", "surgeon");

        assertEquals(getInfoAboutDoctorDtoResponse,
                gson.fromJson(server.getDoctorInfoByPatient(gson.toJson(getInfoAboutDoctorByPatientDtoRequest)), DoctorDtoResponse.class));

        //удаление доктора
        DeleteUserDtoRequest deleteDoctorDtoRequest = new DeleteUserDtoRequest(tokens[0]);
        assertEquals(gson.toJson(null), server.deleteDoctor(gson.toJson(deleteDoctorDtoRequest)));

        //единственный оставшийся хирург это Белла

        GetInfoAboutDoctorByPatientDtoRequest getInfoAboutDoctorByPatientDtoRequest1 = new GetInfoAboutDoctorByPatientDtoRequest(tokens[3]);
        DoctorDtoResponse getInfoAboutDoctorDtoResponse1 = new DoctorDtoResponse("King", "Bella", "surgeon");
        assertEquals(getInfoAboutDoctorDtoResponse1,
                gson.fromJson(server.getDoctorInfoByPatient(gson.toJson(getInfoAboutDoctorByPatientDtoRequest1)), DoctorDtoResponse.class));

        //теперь у Беллы должно быть 3 пациента
        GetPatientListByDoctorDtoRequest getPatientListByDoctorDtoRequest = new GetPatientListByDoctorDtoRequest(tokens[1]);
        PatientListDtoResponse patientListDtoResponse =
                gson.fromJson(server.getPatientListByDoctor(gson.toJson(getPatientListByDoctorDtoRequest)), PatientListDtoResponse.class);
        assertEquals(3, patientListDtoResponse.getPatientList().size());
    }
}
