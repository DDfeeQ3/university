package net.thumbtack.school.hospital.client;

import net.thumbtack.school.hospital.dto.request.MethodTypeRequest;
import net.thumbtack.school.hospital.exceptions.ErrorCode;
import org.junit.jupiter.api.Test;

public class ClientTest extends ClientTestBase {



    @Test
    public void registerDoctor() {
        registerDoctor("Иван", "Иванов", "Терапевт", "ivan_ivanov",
                "vanya_1987", ErrorCode.OK);
    }

    @Test
    public void registerPatient() {
        registerDoctor("Иван", "Иванов", "Терапевт", "ivan_ivanov",
                "vanya_1987", ErrorCode.OK);
        registerPatient("Рома", "Ромамнов", "Кашель", "romka_1999",
                "romaroma", "ivan_ivanov", ErrorCode.OK);
    }

    @Test
    public void addAppointment() {
        registerDoctor("Иван", "Иванов", "Терапевт", "ivan_ivanov",
                "vanya_1987", ErrorCode.OK);
        registerPatient("Рома", "Ромамнов", "Кашель", "romka_1999",
                "romaroma", "ivan_ivanov", ErrorCode.OK);
        addAppointment(MethodTypeRequest.MEDICINE, "Сироп", 3, 2, ErrorCode.OK);
    }
}
