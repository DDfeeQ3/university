package net.thumbtack.school.hospital;

import com.google.gson.Gson;
import net.thumbtack.school.hospital.dto.request.*;
import net.thumbtack.school.hospital.dto.response.*;
import net.thumbtack.school.hospital.server.Server;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class TestThreads {
    private static final Server server = new Server();

    Gson gson = new Gson();

    @BeforeEach()
    public void clear() {
        server.clearDatabase();
    }

    @Test
    public void registerUsersMultiThreadTest() {
        Thread userDoctor1 = new Thread(() -> {
            try {
                RegisterDoctorDtoRequest registerDoctorDtoRequest = new RegisterDoctorDtoRequest("Иван", "Иванов", "Терапепвт",
                        "ivanov_top1", "myPassword1");
                String doctorToken = gson.fromJson(server.registerDoctor(gson.toJson(registerDoctorDtoRequest)), RegisterDoctorDtoResponse.class).getToken();

                assertNotNull(doctorToken);
                Thread.sleep(1000);

                GetDoctorByTokenDtoRequest getDoctorByTokenDtoRequest = new GetDoctorByTokenDtoRequest(doctorToken);
                DoctorDtoResponse doctorDtoResponse = gson.fromJson(server.getDoctorByToken(gson.toJson(getDoctorByTokenDtoRequest)), DoctorDtoResponse.class);

                assertEquals(doctorDtoResponse.getFistName(), registerDoctorDtoRequest.getFirstName());
                assertEquals(doctorDtoResponse.getLastName(), registerDoctorDtoRequest.getLastName());
                assertEquals(doctorDtoResponse.getSpeciality(), registerDoctorDtoRequest.getSpeciality());
                assertEquals(doctorDtoResponse.getLogin(), registerDoctorDtoRequest.getLogin());
                assertEquals(doctorDtoResponse.getPassword(), registerDoctorDtoRequest.getPassword());

                //Проверка выхода с сервера
                LogoutUserDtoRequest logoutDoctor = new LogoutUserDtoRequest(doctorToken);
                assertEquals(gson.toJson(null), server.logoutDoctor(gson.toJson(logoutDoctor)));

                //Проверка входа на сервер

                LoginUserDtoRequest loginDoctor = new LoginUserDtoRequest("ivanov_top1", "myPassword1");

                doctorToken = gson.fromJson(server.loginDoctor(gson.toJson(loginDoctor)), LoginUserDtoResponse.class).getToken();
                assertNotNull(doctorToken);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread userDoctor2 = new Thread(() -> {
            try {
                RegisterDoctorDtoRequest registerDoctorDtoRequest = new RegisterDoctorDtoRequest("Роман", "Романов", "Хирург",
                        "ya_romanov", "romkaTop1");
                String doctorToken = gson.fromJson(server.registerDoctor(gson.toJson(registerDoctorDtoRequest)), RegisterDoctorDtoResponse.class).getToken();

                assertNotNull(doctorToken);
                Thread.sleep(1000);

                GetDoctorByTokenDtoRequest getDoctorByTokenDtoRequest = new GetDoctorByTokenDtoRequest(doctorToken);
                DoctorDtoResponse doctorDtoResponse = gson.fromJson(server.getDoctorByToken(gson.toJson(getDoctorByTokenDtoRequest)), DoctorDtoResponse.class);

                assertEquals(doctorDtoResponse.getFistName(), registerDoctorDtoRequest.getFirstName());
                assertEquals(doctorDtoResponse.getLastName(), registerDoctorDtoRequest.getLastName());
                assertEquals(doctorDtoResponse.getSpeciality(), registerDoctorDtoRequest.getSpeciality());
                assertEquals(doctorDtoResponse.getLogin(), registerDoctorDtoRequest.getLogin());
                assertEquals(doctorDtoResponse.getPassword(), registerDoctorDtoRequest.getPassword());

                //Проверка выхода с сервера
                LogoutUserDtoRequest logoutDoctor = new LogoutUserDtoRequest(doctorToken);
                assertEquals(gson.toJson(null), server.logoutDoctor(gson.toJson(logoutDoctor)));

                //Проверка входа на сервер

                LoginUserDtoRequest loginDoctor = new LoginUserDtoRequest("ya_romanov", "romkaTop1");

                doctorToken = gson.fromJson(server.loginDoctor(gson.toJson(loginDoctor)), LoginUserDtoResponse.class).getToken();
                assertNotNull(doctorToken);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread userPatient1 = new Thread(() -> {
            try {
                Thread.sleep(1000);
                RegisterPatientDtoRequest registerPatientDtoRequest = new RegisterPatientDtoRequest("Николай", "Николя", "Кашель",
                        "kolyan1", "kolya_1998", "ivanov_top1");
                String patientToken = gson.fromJson(server.registerPatient(gson.toJson(registerPatientDtoRequest)), RegisterPatientDtoResponse.class).getToken();

                assertNotNull(patientToken);
                Thread.sleep(1000);

                GetPatientByTokenDtoRequest getPatientByTokenDtoRequest = new GetPatientByTokenDtoRequest(patientToken);
                PatientDtoResponse patientDtoResponse = gson.fromJson(server.getPatientByToken(gson.toJson(getPatientByTokenDtoRequest)), PatientDtoResponse.class);

                assertEquals(patientDtoResponse.getFirstName(), registerPatientDtoRequest.getFirstName());
                assertEquals(patientDtoResponse.getLastName(), registerPatientDtoRequest.getLastName());
                assertEquals(patientDtoResponse.getDisease(), registerPatientDtoRequest.getDisease());
                assertEquals(patientDtoResponse.getLogin(), registerPatientDtoRequest.getLogin());
                assertEquals(patientDtoResponse.getPassword(), registerPatientDtoRequest.getPassword());

                //Проверка выхода с сервера
                LogoutUserDtoRequest logoutPatient = new LogoutUserDtoRequest(patientToken);
                assertEquals(gson.toJson(null), server.logoutPatient(gson.toJson(logoutPatient)));

                //Проверка входа на сервер

                LoginUserDtoRequest loginPatient = new LoginUserDtoRequest("kolyan1", "kolya_1998");

                patientToken = gson.fromJson(server.loginPatient(gson.toJson(loginPatient)), LoginUserDtoResponse.class).getToken();
                assertNotNull(patientToken);

                //Смена пароля

                ChangePatientPasswordDtoRequest changePatientPasswordDtoRequest = new ChangePatientPasswordDtoRequest("kolyan1", "Kolyan1998", "kolya_1998");
                server.changePasswordPatient(gson.toJson(changePatientPasswordDtoRequest));
                assertNotNull(patientToken);

                getPatientByTokenDtoRequest.setToken(patientToken);

                patientDtoResponse = gson.fromJson(server.getPatientByToken(gson.toJson(getPatientByTokenDtoRequest)), PatientDtoResponse.class);

                assertEquals(patientDtoResponse.getPassword(), changePatientPasswordDtoRequest.getNewPassword());


            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread userPatient2 = new Thread(() -> {
            try {
                Thread.sleep(1000);
                RegisterPatientDtoRequest registerPatientDtoRequest = new RegisterPatientDtoRequest("Василий", "Васильев", "Травма руки",
                        "vasya322", "12vas56", "ya_romanov");
                String patientToken = gson.fromJson(server.registerPatient(gson.toJson(registerPatientDtoRequest)), RegisterPatientDtoResponse.class).getToken();

                assertNotNull(patientToken);
                Thread.sleep(1000);

                GetPatientByTokenDtoRequest getPatientByTokenDtoRequest = new GetPatientByTokenDtoRequest(patientToken);
                PatientDtoResponse patientDtoResponse = gson.fromJson(server.getPatientByToken(gson.toJson(getPatientByTokenDtoRequest)), PatientDtoResponse.class);

                assertEquals(patientDtoResponse.getFirstName(), registerPatientDtoRequest.getFirstName());
                assertEquals(patientDtoResponse.getLastName(), registerPatientDtoRequest.getLastName());
                assertEquals(patientDtoResponse.getDisease(), registerPatientDtoRequest.getDisease());
                assertEquals(patientDtoResponse.getLogin(), registerPatientDtoRequest.getLogin());
                assertEquals(patientDtoResponse.getPassword(), registerPatientDtoRequest.getPassword());

                //Проверка выхода с сервера
                LogoutUserDtoRequest logoutPatient = new LogoutUserDtoRequest(patientToken);
                assertEquals(gson.toJson(null), server.logoutPatient(gson.toJson(logoutPatient)));

                //Проверка входа на сервер

                LoginUserDtoRequest loginPatient = new LoginUserDtoRequest("vasya322", "12vas56");

                patientToken = gson.fromJson(server.loginPatient(gson.toJson(loginPatient)), LoginUserDtoResponse.class).getToken();
                assertNotNull(patientToken);

                AddAppointmentDtoRequest appointmentDtoRequest = new AddAppointmentDtoRequest(MethodTypeRequest.MEDICINE, "Сироп", 3, patientDtoResponse.getPatientID());

                server.addAppointments(gson.toJson(appointmentDtoRequest));

                AppointmentListDtoResponse appointmentsListDtoResponse = gson.fromJson(server.getAppointmentsByPatient(gson.toJson(patientDtoResponse)), AppointmentListDtoResponse.class);

                assertEquals(appointmentsListDtoResponse.getAppointments().get(0).getTreatmentMethod(), appointmentDtoRequest.getMethodName());
                assertEquals(appointmentsListDtoResponse.getAppointments().get(0).getFrequency(), appointmentDtoRequest.getFrequency());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        userDoctor1.start();
        userDoctor2.start();
        userPatient1.start();
        userPatient2.start();

        try {
            userDoctor1.join();
            userDoctor2.join();
            userPatient1.join();
            userPatient2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
