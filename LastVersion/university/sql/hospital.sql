DROP DATABASE IF EXISTS hospital;
CREATE DATABASE `hospital`;
USE `hospital`;

CREATE TABLE account(
	id int(11) not null auto_increment primary key,
    firstName varchar(50) not null,
    lastName varchar(50) not null,
    login varchar(50) not null,
    password varchar(50) not null,
    CONSTRAINT account UNIQUE (login),
    key firstName (firstName),
    key lastName (lastName),
    key login (login),
    key password (password)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

create table user_token (
    token varchar(100) not null primary key,
	id_account int(11) not null,
    foreign key (id_account) references account (id) on delete cascade,
    key token (token)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

create table doctor (
	id int(11) not null auto_increment primary key,
	id_account int(11) not null,
    speciality varchar(50) not null,
    foreign key (id_account) references account (id) on delete cascade,
    key speciality (speciality),
    key id_account (id_account)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

create table patient (
	id int(11) not null auto_increment primary key,
    id_account  int(11) not null,
    id_doctor int(11) not null,
    disease varchar(50) default null,
    foreign key (id_account) references account (id) on delete cascade,
    key disease (disease),
    key id_doctor (id_doctor),
    key id_account (id_account)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

create table appointment (
	id int(11) not null auto_increment primary key,
    treatmentMethod varchar(50) not null,
    methodType enum('PROCEDURE', 'MEDICINE') not null,
    frequency int(2) not null,
    id_patient int(11) not null,
    foreign key (id_patient) references patient (id_account) on delete cascade,
    key treatmentMethod (treatmentMethod),
    key methodType (methodType),
    key frequency (frequency)
) ENGINE=INNODB DEFAULT CHARSET=utf8;